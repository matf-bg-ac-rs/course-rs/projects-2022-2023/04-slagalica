QT -= gui
QT += network

TEMPLATE = lib
DEFINES += SERIALIZATION_LIBRARY

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    src/gameslistmodel.cpp \
    src/gamemodel.cpp \
    src/jsonserializer.cpp

HEADERS += \
    includes/gamemodel.h \
    includes/gameslistmodel.h \
    includes/iattributable.h \
    includes/jsonserializer.h \
    includes/serializable.h \
    serialization_global.h \
    includes/serializer.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
