#ifndef JSONSERIALIZER_H
#define JSONSERIALIZER_H


#include "serialization_global.h"
#include "serializer.h"
#include <QTcpSocket>

class SERIALIZATION_EXPORT JSONSerializer : public Serializer
{
public:
    JSONSerializer();

    void save(const Serializable &serializable, const QString &filepath, const QString &rootName = "") override;
    void load(Serializable &serializable, const QString &filepath) override;

    void loadFromString(Serializable &serializable, const QString &data) override;
    QString saveAsString(Serializable &serializable) override;

    void sendDataToSocket(Serializable &serializable, QTcpSocket* server) override;
};

#endif // JSONSERIALIZER_H
