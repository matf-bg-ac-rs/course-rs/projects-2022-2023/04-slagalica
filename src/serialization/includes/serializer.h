#ifndef SERIALIZER_H
#define SERIALIZER_H

#include <QTcpSocket>
#include "serialization_global.h"
#include "serializable.h"

#include <fstream>
#include <iostream>
#include <sstream>

class SERIALIZATION_EXPORT Serializer
{
public:
    virtual ~Serializer() = default;


    virtual void save(const Serializable& serializable, const QString& filepath, const QString& rootName = "") = 0;
    virtual void load(Serializable& serializable, const QString& filepath) = 0;
    virtual void loadFromString(Serializable& serializable, const QString& data) = 0;
    virtual QString saveAsString(Serializable &serializable) = 0;
    virtual void sendDataToSocket(Serializable &serializable, QTcpSocket* server) = 0;

    QString getRawData(const QString& filepath) const {
        std::ifstream t(filepath.toStdString());
        std::stringstream buffer;
        buffer << t.rdbuf();

        std::string data = buffer.str();
        return QString::fromStdString(data);
    }
};

#endif // SERIALIZER_H

