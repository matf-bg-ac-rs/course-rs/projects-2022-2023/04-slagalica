#ifndef GAMESLISTMODEL_H
#define GAMESLISTMODEL_H

#include "includes/gamemodel.h"
#include "serializable.h"
#include "serialization_global.h"
#include "iattributable.h"

class SERIALIZATION_EXPORT GamesListModel : public Serializable, public IAttributable
{
public:
    GamesListModel(
        const QVector<GameModel*> games = {}
    );
    ~GamesListModel();

    enum Attributes
    {
        Games,
        NumberOfAttributes
    };

    QVector<GameModel *> &getGames();

    int numberOfAttributes() const override;
    QString attributeName(int idx) const override;
    QVariant attribute(int idx) const override;
    void setAttribute(int idx, const QVariant & value) override;


    QVariant toVariant() const override;
    void fromVariant(const QVariant& variant) override;

private:
    QVector<GameModel*> m_games;

};

#endif // GAMESLISTMODEL_H
