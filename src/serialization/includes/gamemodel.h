#ifndef GAMEMODEL_H
#define GAMEMODEL_H

#include <QVariant>

#include "serialization_global.h"
#include "serializable.h"
#include "iattributable.h"



class SERIALIZATION_EXPORT GameModel : public Serializable, public IAttributable
{
private:
    static const int NUMBER_OF_ATTRIBUTES = 2;
public:
    enum Attributes
    {
        Username,
        GamesWon,
        NumberOfAttributes
    };

public:
    GameModel(
        QString username,
        unsigned gamesWon
    );
    GameModel();

    // Metodi za (de)serijalizaciju
    QVariant toVariant() const override;
    void fromVariant(const QVariant& variant) override;

    int numberOfAttributes() const override;
    QString attributeName(int idx) const override;
    QVariant attribute(int idx) const override;
    void setAttribute(int idx, const QVariant & value) override;

    void incrementWins();

public:
    QString getUsername() const;
    unsigned getGamesWon() const;

private:
    QString m_username;
    unsigned m_gamesWon;
};


#endif // GAMEMODEL_H


