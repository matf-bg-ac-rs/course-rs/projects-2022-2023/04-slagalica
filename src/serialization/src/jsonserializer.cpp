#include <QJsonDocument>
#include <QFile>

#include "includes/jsonserializer.h"

JSONSerializer::JSONSerializer()
{

}

void JSONSerializer::save(const Serializable &serializable, const QString &filepath, const QString &rootName)
{
    Q_UNUSED(rootName);

    QJsonDocument doc = QJsonDocument::fromVariant(serializable.toVariant());
    QFile file(filepath);
    file.open(QFile::WriteOnly);
    file.write(doc.toJson(QJsonDocument::JsonFormat::Indented)); // Neminimizovan zapis (moze i QJsonDocument::JsonFormat::Compact)
    file.close();
}

void JSONSerializer::load(Serializable &serializable, const QString &filepath)
{
    QFile file(filepath);
    file.open(QFile::ReadOnly);

    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
    file.close();
    serializable.fromVariant(doc.toVariant());
}

void JSONSerializer::loadFromString(Serializable &serializable, const QString &data)
{
    QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());
    serializable.fromVariant(doc.toVariant());
}

QString JSONSerializer::saveAsString(Serializable &serializable)
{
    QJsonDocument doc = QJsonDocument::fromVariant(serializable.toVariant());
    return QString(doc.toJson(QJsonDocument::Compact));
}

void JSONSerializer::sendDataToSocket(Serializable &serializable, QTcpSocket *server)
{
    const auto data = saveAsString(serializable).toUtf8();
    server->write(data);
    server->flush();
}

