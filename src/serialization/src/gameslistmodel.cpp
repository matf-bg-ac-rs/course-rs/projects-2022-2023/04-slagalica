#include "includes/gameslistmodel.h"

GamesListModel::GamesListModel(const QVector<GameModel *> games)
    :m_games(games)
{}

GamesListModel::~GamesListModel()
{
    qDeleteAll(m_games);
}

QVector<GameModel *> &GamesListModel::getGames()
{
    return m_games;
}

int GamesListModel::numberOfAttributes() const
{
    return Attributes::NumberOfAttributes;
}

QString GamesListModel::attributeName(int idx) const
{
    Q_UNUSED(idx);
    return {};
}

QVariant GamesListModel::attribute(int idx) const
{
    Q_UNUSED(idx);
    return {};
}

void GamesListModel::setAttribute(int idx, const QVariant &value)
{
    Q_UNUSED(idx);
    Q_UNUSED(value);
}


QVariant GamesListModel::toVariant() const
{
    QVariantMap map;
    QVariantList games;

    for (const auto & course : m_games) {
        games.append(course->toVariant());
    }

    map.insert("games", games);
    return map;

}

void GamesListModel::fromVariant(const QVariant &variant)
{
    const auto map = variant.toMap();

    qDeleteAll(m_games);
    m_games.clear();

    const auto games = map.value("games").toList();
    for(const auto& game : games) {
        auto gameItem = new GameModel();
        gameItem->fromVariant(game);
        m_games.push_back(gameItem);
    }
}
