#include "includes/gamemodel.h"

GameModel::GameModel(QString username, unsigned gamesWon)
    : m_username(username)
    , m_gamesWon(gamesWon)
{
}

GameModel::GameModel()
    : m_username("")
    , m_gamesWon(0u)
{
}

QVariant GameModel::toVariant() const
{
    QVariantMap map;
    map.insert("username", m_username);
    map.insert("gamesWon", m_gamesWon);
    return map;
}

void GameModel::fromVariant(const QVariant &variant)
{
    QVariantMap map = variant.toMap();
    m_username = map.value("username").toString();
    m_gamesWon = map.value("gamesWon").toUInt();
}

int GameModel::numberOfAttributes() const
{
    return Attributes::NumberOfAttributes;
}

QString GameModel::attributeName(int idx) const
{
    switch (idx) {
    case Attributes::Username:
        return "User name";
    case Attributes::GamesWon:
        return "Games won";
    default:
        return {};
    }
}

QVariant GameModel::attribute(int idx) const
{
    switch (idx) {
    case Attributes::Username:
        return QVariant(m_username);
    case Attributes::GamesWon:
        return QVariant(m_gamesWon);
    default:
        return {};
    }
}

void GameModel::setAttribute(int idx, const QVariant &value)
{
    switch (idx) {
    case Attributes::Username:
        m_username = value.toString();
        break;
    case Attributes::GamesWon: {
        bool parsed = false;
        auto parsedValue = value.toUInt(&parsed);
        if (parsed) {
            m_gamesWon = parsedValue;
        }
    }
        break;
    default:
        break;
    }
}

void GameModel::incrementWins()
{
    m_gamesWon++;
}

QString GameModel::getUsername() const
{
    return m_username;
}

unsigned GameModel::getGamesWon() const
{
    return m_gamesWon;
}
