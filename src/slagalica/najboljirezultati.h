#ifndef NAJBOLJIREZULTATI_H
#define NAJBOLJIREZULTATI_H

#include "qtcpsocket.h"
#include <QWidget>
#include <QAbstractItemModel>

namespace Ui {
class NajboljiRezultati;
}

class Serializer;
class GameModel;

class NajboljiRezultati : public QWidget
{
    Q_OBJECT

public:
    explicit NajboljiRezultati(QWidget *parent = nullptr, QTcpSocket *server = nullptr, int playerId = -1);
    ~NajboljiRezultati();

    Ui::NajboljiRezultati *getUi() const;

    QAbstractItemModel *gamesModel() const;

signals:
    void goBack();

private:
    Ui::NajboljiRezultati *ui;
    Serializer *m_jsonSerializer;

    QTcpSocket* m_server;
    QAbstractItemModel* m_gamesModel;

    int m_playerId;

private slots:
    void on_pbNazad_clicked();
    void readyRead();

private:
    void loadData(Serializer *serializer);
    void connectViewAndModel(const QVector<GameModel *> &games);

    void sendMessage(QTcpSocket* socket,QString msg);
};

#endif // NAJBOLJIREZULTATI_H
