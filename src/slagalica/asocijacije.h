#ifndef ASOCIJACIJE_H
#define ASOCIJACIJE_H

#include <QString>
#include <QPair>
#include <QVector>
#include <QMap>
#include <QTcpSocket>
#include "game.h"


enum Column{
  A,
  B,
  C,
  D,
  Konacno
};


class Asocijacije: public Game
{
public:
    bool m_turnCounter;
    QMap<char, QString> m_resultColumns;
    QMap<char, QVector<QString>> m_columnsField;


    void guess(int associationNumber, Column m_column, QString m_resultColumn, int playerId, QTcpSocket* server);
    virtual void init() override;
    virtual int calculateScore() override;

    void sendMessage(QTcpSocket* server, QString msg);
    QString columnToString(Column m_column);

};




#endif // ASOCIJACIJE_H
