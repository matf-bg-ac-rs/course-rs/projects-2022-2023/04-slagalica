#ifndef KOZNAZNAUI_H
#define KOZNAZNAUI_H

#include <QWidget>
#include <QTcpSocket>
#include <QTimer>
#include "koznazna.h"


namespace Ui {
class Koznaznaui;
}

class Koznaznaui : public QWidget
{
    Q_OBJECT

public:
    explicit Koznaznaui(QWidget *parent = nullptr, QTcpSocket* server = nullptr, int playerId = -1, QString clientName1= {}, QString clientName2={}, int questionCounter = -1,int playerPoint1=0,int playerPoints2=0);
    ~Koznaznaui();


    Ui::Koznaznaui *getUi();
    void getQuestion(int questionNumber);


    void paintButtons(int answer, int answerPlayer1, int answerPlayer2);
    void restartButtons();
    void nextQuestion();
    int id() const;
    void setId(int newId);

    int questionCounter() const;
    void setQuestionCounter(int newQuestionCounter);

    const QString &clientName1() const;
    void setClientName1(const QString &newClientName1);

    const QString &clientName2() const;
    void setClientName2(const QString &newClientName2);

    int time() const;
    void setTime(int newTime);

    int timeMs() const;
    void setTimeMs(int newTimeMs);

    void displayQuestion(QString question);

private slots:
    void on_pb_A_clicked();

    void on_pb_B_clicked();

    void on_pb_C_clicked();

    void on_pb_D_clicked();


    void on_pb_Next_clicked();

public slots:
    void updateTime();
    void updateTimeMs();
    void on_timesUp();

    void readyRead();

signals:
    void timesUp();
    void endGame();

private:
    Ui::Koznaznaui *ui;
    KoZnaZna* m_koznazna;
    QTcpSocket* m_server;
    int m_id;
    int m_questionCounter;

    QString m_clientName1;
    QString m_clientName2;

    void sendMessage(QTcpSocket* socket,QString msg);

    void displayAnswer(QString answer);
    void disableUi();
    void enableUi();

    void nextGame();

    int m_time;
    int m_timeMs;
    QTimer *m_timer;
    QTimer *m_timerMs;
};

#endif // KOZNAZNAUI_H
