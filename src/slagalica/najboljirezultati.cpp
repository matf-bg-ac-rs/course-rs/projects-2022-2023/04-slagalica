#include "najboljirezultati.h"
#include "ui_najboljirezultati.h"

#include "includes/jsonserializer.h"
#include "includes/gameslistmodel.h"
#include <iostream>
#include <fstream>
#include <sstream>

#include "model.h"
#include <QSortFilterProxyModel>

NajboljiRezultati::NajboljiRezultati(QWidget *parent, QTcpSocket *server, int playerId) :
    QWidget(parent),
    ui(new Ui::NajboljiRezultati),
    m_server(server),
    m_playerId(playerId)

{
    ui->setupUi(this);
    m_jsonSerializer = new JSONSerializer();

    connect(ui->pbNazad, &QPushButton::clicked, this, &NajboljiRezultati::on_pbNazad_clicked);

    connect(m_server,SIGNAL(readyRead()),this,SLOT(readyRead()));
    QString msg = QString::number(m_playerId) + "_w";

    // 1. posalji poruku serveru da vrati podatke o partijama
    sendMessage(m_server, msg);

}

NajboljiRezultati::~NajboljiRezultati()
{
    disconnect(m_server,SIGNAL(readyRead()),this,SLOT(readyRead()));
    delete ui;
}

Ui::NajboljiRezultati *NajboljiRezultati::getUi() const
{
    return ui;
}

QAbstractItemModel *NajboljiRezultati::gamesModel() const
{
    return m_gamesModel;
}

void NajboljiRezultati::on_pbNazad_clicked()
{
    emit goBack();
}
void NajboljiRezultati::readyRead()
{
    loadData(m_jsonSerializer);

}

void NajboljiRezultati::loadData(Serializer *serializer)
{
    GamesListModel* games = new GamesListModel();

    // 2. server vraca sirove podatke o partijama u obliku stringa
    QString data = m_server->readAll();

    qDebug() << data;
    // 3. deserijalizuj iz stringa podatke
    serializer->loadFromString(*games, data);

    // 4. formiraj pogled na osnovu modela podataka
    connectViewAndModel(games->getGames());
}

void NajboljiRezultati::connectViewAndModel(const QVector<GameModel *> &games)
{
       m_gamesModel = new TreeModel(games);

       QSortFilterProxyModel *filterModel = new QSortFilterProxyModel(this);
       filterModel->setSourceModel(m_gamesModel);

       ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
       ui->tableView->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

       // Postavljamo model za tabelarni prikaz
       ui->tableView->setModel(filterModel);
       ui->tableView->horizontalHeader()->setSectionsMovable(true);
       ui->tableView->verticalHeader()->setSectionsMovable(true);
       // Set StaticContents to enable minimal repaints on resizes.
       ui->tableView->viewport()->setAttribute(Qt::WA_StaticContents);
}

void NajboljiRezultati::sendMessage(QTcpSocket *socket, QString msg)
{
    socket->write(msg.toUtf8());
    socket->flush();
}


