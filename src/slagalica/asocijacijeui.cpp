#include "asocijacijeui.h"

#include "ui_asocijacijeui.h"

AsocijacijeUi::AsocijacijeUi(QWidget *parent, QTcpSocket *server, int playerId, QString clientName1,QString clientName2, int associationNumber,int playerPoints1, int playerPoints2) :
    QWidget(parent),
    ui(new Ui::AsocijacijeUi)
{
    ui->setupUi(this);

    m_server = server;
    connect(m_server, SIGNAL(readyRead()), this, SLOT(readyRead()));

    ui->lcdPlayer1->display(playerPoints1);
    ui->lcdPlayer2->display(playerPoints2);

    m_id = playerId;
    m_associationNumber = associationNumber;
    m_buttonsRemaining = 16;

    m_clientName1=clientName1;
    m_clientName2=clientName2;


    addButtonsToMap();

    disableAllTemp();
    if(m_id == associationNumber+1)
    {
        m_turn = true;
        enableAllRemainingButtons();
    }
    else
        m_turn = false;

    m_time = 20;
    ui->leTimer->setText(QString::number(m_time));
    m_brPoteza=0;


    ui->lineEdit->setText(m_clientName1);
    ui->lineEdit_4->setText(m_clientName2);

    m_timer = new QTimer(this);
    connect(m_timer,SIGNAL(timeout()),this,SLOT(updateTime()));
    connect(this,&AsocijacijeUi::switch_turn,this,&AsocijacijeUi::on_switchTurn);
    m_timer->start(1000);

    ui->le_A->setAlignment(Qt::AlignCenter);
    ui->le_B->setAlignment(Qt::AlignCenter);
    ui->le_C->setAlignment(Qt::AlignCenter);
    ui->le_D->setAlignment(Qt::AlignCenter);
    ui->le_KONACNO->setAlignment(Qt::AlignCenter);


}

AsocijacijeUi::~AsocijacijeUi()
{
    delete ui;
}

void AsocijacijeUi::on_pb_A1_clicked()
{



    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_A_1";

    sendMessage(m_server, msgString);







}

void AsocijacijeUi::on_pb_A2_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_A_2";

    sendMessage(m_server, msgString);



}


void AsocijacijeUi::on_pb_A3_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_A_3";

    sendMessage(m_server, msgString);

}


void AsocijacijeUi::on_pb_A4_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_A_4";

    sendMessage(m_server, msgString);

}

void AsocijacijeUi::on_pb_B1_clicked()
{


    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_B_1";

    sendMessage(m_server, msgString);

}


void AsocijacijeUi::on_pb_B2_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_B_2";

    sendMessage(m_server, msgString);

}


void AsocijacijeUi::on_pb_B3_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_B_3";

    sendMessage(m_server, msgString);
}


void AsocijacijeUi::on_pb_B4_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_B_4";

    sendMessage(m_server, msgString);
}


void AsocijacijeUi::on_pb_C1_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_C_1";

    sendMessage(m_server, msgString);
}


void AsocijacijeUi::on_pb_C2_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_C_2";

    sendMessage(m_server, msgString);
}


void AsocijacijeUi::on_pb_C3_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_C_3";

    sendMessage(m_server, msgString);
}


void AsocijacijeUi::on_pb_C4_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_C_4";

    sendMessage(m_server, msgString);
}



void AsocijacijeUi::on_pb_D1_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_D_1";

    sendMessage(m_server, msgString);
}


void AsocijacijeUi::on_pb_D2_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_D_2";

    sendMessage(m_server, msgString);
}


void AsocijacijeUi::on_pb_D3_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_D_3";

    sendMessage(m_server, msgString);
}


void AsocijacijeUi::on_pb_D4_clicked()
{
    disableOpeningFieldsTemp();

    //primer poruke poruka serveru -> id(idIgraca)_g(generate)_a(asocijacije)_1(redni broj asoc)_nazivKolone_brojKolone
    QString msgString = QString::number(m_id)
                        + "_g_a_"
                        + QString::number(m_associationNumber)
                        + "_D_4";

    sendMessage(m_server, msgString);
}
void AsocijacijeUi::on_le_A_returnPressed()
{
   //ui->le_A->setDisabled(true);
   disableAllTemp();
   m_asocijacije->guess(m_associationNumber, Column::A, ui->le_A->text(), m_id, m_server);

}

void AsocijacijeUi::on_le_B_returnPressed()
{
   //ui->le_B->setDisabled(true);
   disableAllTemp();
   m_asocijacije->guess(m_associationNumber, Column::B, ui->le_B->text(), m_id, m_server);

}

void AsocijacijeUi::on_le_C_returnPressed()
{
    //ui->le_C->setDisabled(true);
    disableAllTemp();
    m_asocijacije->guess(m_associationNumber, Column::C, ui->le_C->text(), m_id, m_server);
}

void AsocijacijeUi::on_le_D_returnPressed()
{
    //ui->le_D->setDisabled(true);
    disableAllTemp();
    m_asocijacije->guess(m_associationNumber, Column::D, ui->le_D->text(), m_id, m_server);
}

void AsocijacijeUi::on_le_KONACNO_returnPressed()
{
    //ui->le_KONACNO->setDisabled(true);
    disableAllTemp();
    m_asocijacije->guess(m_associationNumber, Column:: Konacno, ui->le_KONACNO->text(), m_id, m_server);
}

void AsocijacijeUi::sendMessage(QTcpSocket *server, QString msg)
{
    server->write(msg.toUtf8());
    server->flush();
}

const QString &AsocijacijeUi::clientName2() const
{
    return m_clientName2;
}

void AsocijacijeUi::setClientName2(const QString &newClientName2)
{
    m_clientName2 = newClientName2;
}

const QString &AsocijacijeUi::clientName1() const
{
    return m_clientName1;
}

void AsocijacijeUi::setClientName1(const QString &newClientName1)
{
    m_clientName1 = newClientName1;
}

int AsocijacijeUi::buttonsRemaining() const
{
    return m_buttonsRemaining;
}

void AsocijacijeUi::setButtonsRemaining(int newButtonsRemaining)
{
    m_buttonsRemaining = newButtonsRemaining;
}

int AsocijacijeUi::id() const
{
    return m_id;
}

void AsocijacijeUi::setId(int newId)
{
    m_id = newId;
}

bool AsocijacijeUi::turn() const
{
    return m_turn;
}

void AsocijacijeUi::setTurn(bool newTurn)
{
    m_turn = newTurn;
}

int AsocijacijeUi::brPoteza() const
{
    return m_brPoteza;
}

void AsocijacijeUi::setBrPoteza(int newBrPoteza)
{
    m_brPoteza = newBrPoteza;
}

int AsocijacijeUi::time() const
{
    return m_time;
}

void AsocijacijeUi::setTime(int newTime)
{
    m_time = newTime;
}

int AsocijacijeUi::associationNumber() const
{
    return m_associationNumber;
}

void AsocijacijeUi::readyRead()
{
    QString serverAnswer = m_server->readAll();
    QStringList serverAnswerSplit = serverAnswer.split('_');
    qDebug() << serverAnswer;

    if (serverAnswerSplit[0] == "g")
    {
        m_buttonsRemaining--;
        displayField(serverAnswerSplit);
        if (m_turn)
            enableAllRemainingLineEdits();
    }

    if (serverAnswerSplit[0] == "u")
    {
        m_timer->stop();
        showAttempt(serverAnswerSplit);
        updatePoints(serverAnswerSplit);
    }

    if (serverAnswerSplit[0] == "s")
    {
        QStringList dataSplit = serverAnswer.split('_');
        setAllFields(dataSplit, false);

        QTimer::singleShot(4000, this, &AsocijacijeUi::nextGame);
        return;
    }

}

void AsocijacijeUi::displayField(QStringList data)
{
    QString columnLetter = data[1];
    int columnNumber = data[2].toInt();
    QString fieldString = data[3];

    if (columnLetter == 'A')
    {
        m_enableLineEditVector[ui->le_A]++;

        switch (columnNumber) {
        case 1:
            ui->pb_A1->setText(fieldString);
            disableButtonForever(ui->pb_A1);

            break;
        case 2:
            ui->pb_A2->setText(fieldString);
            disableButtonForever(ui->pb_A2);

            break;
        case 3:
            ui->pb_A3->setText(fieldString);
            disableButtonForever(ui->pb_A3);

            break;
        case 4:
            ui->pb_A4->setText(fieldString);
            disableButtonForever(ui->pb_A4);

            break;
        default:
            break;
        }
    }

    if (columnLetter == 'B')
    {
        m_enableLineEditVector[ui->le_B]++;

        switch (columnNumber) {
        case 1:
            ui->pb_B1->setText(fieldString);
            disableButtonForever(ui->pb_B1);

            break;
        case 2:
            ui->pb_B2->setText(fieldString);
            disableButtonForever(ui->pb_B2);

            break;
        case 3:
            ui->pb_B3->setText(fieldString);
            disableButtonForever(ui->pb_B3);

            break;
        case 4:
            ui->pb_B4->setText(fieldString);
            disableButtonForever(ui->pb_B4);

            break;
        default:
            break;
        }
    }

    if (columnLetter == 'C')
    {
        m_enableLineEditVector[ui->le_C]++;

        switch (columnNumber) {
        case 1:
            ui->pb_C1->setText(fieldString);
            disableButtonForever(ui->pb_C1);

            break;
        case 2:
            ui->pb_C2->setText(fieldString);
            disableButtonForever(ui->pb_C2);

            break;
        case 3:
            ui->pb_C3->setText(fieldString);
            disableButtonForever(ui->pb_C3);

            break;
        case 4:
            ui->pb_C4->setText(fieldString);
            disableButtonForever(ui->pb_C4);

            break;
        default:
            break;
        }
    }

    if (columnLetter == 'D')
    {
        m_enableLineEditVector[ui->le_D]++;

        switch (columnNumber) {
        case 1:
            ui->pb_D1->setText(fieldString);
            disableButtonForever(ui->pb_D1);

            break;
        case 2:
            ui->pb_D2->setText(fieldString);
            disableButtonForever(ui->pb_D2);

            break;
        case 3:
            ui->pb_D3->setText(fieldString);
            disableButtonForever(ui->pb_D3);

            break;
        case 4:
            ui->pb_D4->setText(fieldString);
            disableButtonForever(ui->pb_D4);

            break;
        default:
            break;
        }
    }
}

void AsocijacijeUi::showAttempt(QStringList data)
{
    bool correctFlag = false;
    if (data[3] == 'c')
        correctFlag = true;

    QString columnLetter = data[1];
    QString attemptString = data[2];
    if (correctFlag)
    {
        m_enableLineEditVector[ui->le_KONACNO]++;

        if (columnLetter == 'A')
        {
            if(ui->pb_A1->text().compare("A1")==0){
                m_brPoteza++;
                m_buttonsRemaining--;
            }
            if(ui->pb_A2->text().compare("A2")==0){
                m_brPoteza++;
                m_buttonsRemaining--;
            }
            if(ui->pb_A3->text().compare("A3")==0){
                m_brPoteza++;
                m_buttonsRemaining--;
            }
            if(ui->pb_A4->text().compare("A4")==0){
                m_brPoteza++;
                m_buttonsRemaining--;
            }


            ui->le_A->setText(attemptString);
            ui->pb_A1->setText(data[6]);
            ui->pb_A2->setText(data[7]);
            ui->pb_A3->setText(data[8]);
            ui->pb_A4->setText(data[9]);

            paintWidget(ui->pb_A1,m_id,m_turn);
            paintWidget(ui->pb_A2,m_id,m_turn);
            paintWidget(ui->pb_A3,m_id,m_turn);
            paintWidget(ui->pb_A4,m_id,m_turn);
            paintWidget(ui->le_A,m_id,m_turn);
            setColorYellow(ui->le_A);

            m_timer->start(1000);
            m_time+=5;
            ui->leTimer->setText(QString::number(m_time));


            disableA();
        }

        if (columnLetter == 'B')
        {

            if(ui->pb_B1->text().compare("B1")==0){
                qDebug() << "iz b1 koji je ne otvoren ";
                m_brPoteza++;
                m_buttonsRemaining--;
            }
            if(ui->pb_B2->text().compare("B2")==0){
                qDebug() << "iz b2 koji je ne otvoren ";
                m_brPoteza++;
                m_buttonsRemaining--;
            }
            if(ui->pb_B3->text().compare("B3")==0){
                qDebug() << "iz b3 koji je ne otvoren ";
                m_brPoteza++;
                m_buttonsRemaining--;
            }
            if(ui->pb_B4->text().compare("B4")==0){
                qDebug() << "iz b4 koji je ne otvoren ";
                m_brPoteza++;
                m_buttonsRemaining--;
            }

            ui->le_B->setText(attemptString);
            ui->pb_B1->setText(data[6]);
            ui->pb_B2->setText(data[7]);
            ui->pb_B3->setText(data[8]);
            ui->pb_B4->setText(data[9]);

            paintWidget(ui->pb_B1,m_id,m_turn);
            paintWidget(ui->pb_B2,m_id,m_turn);
            paintWidget(ui->pb_B3,m_id,m_turn);
            paintWidget(ui->pb_B4,m_id,m_turn);
            paintWidget(ui->le_B,m_id,m_turn);
            setColorYellow(ui->le_B);

            m_timer->start(1000);
            m_time+=5;
            ui->leTimer->setText(QString::number(m_time));

            disableB();
        }

        if (columnLetter == 'C')
        {

            if(ui->pb_C1->text().compare("C1")==0){
                m_brPoteza++;
                m_buttonsRemaining--;
            }
            if(ui->pb_C2->text().compare("C2")==0){
                m_brPoteza++;
                m_buttonsRemaining--;
            }
            if(ui->pb_C3->text().compare("C3")==0){
                m_brPoteza++;
                m_buttonsRemaining--;
            }
            if(ui->pb_C4->text().compare("C4")==0){
                m_brPoteza++;
                m_buttonsRemaining--;
            }

            ui->le_C->setText(attemptString);
            ui->pb_C1->setText(data[6]);
            ui->pb_C2->setText(data[7]);
            ui->pb_C3->setText(data[8]);
            ui->pb_C4->setText(data[9]);

            paintWidget(ui->pb_C1,m_id,m_turn);
            paintWidget(ui->pb_C2,m_id,m_turn);
            paintWidget(ui->pb_C3,m_id,m_turn);
            paintWidget(ui->pb_C4,m_id,m_turn);
            paintWidget(ui->le_C,m_id,m_turn);
            setColorYellow(ui->le_C);

            m_timer->start(1000);
            m_time+=5;
            ui->leTimer->setText(QString::number(m_time));

            disableC();
        }

        if (columnLetter == 'D')
        {
            if(ui->pb_D1->text().compare("D1")==0){
                m_brPoteza++;
                m_buttonsRemaining--;
            }
            if(ui->pb_D2->text().compare("D2")==0){
                m_brPoteza++;
                m_buttonsRemaining--;
            }
            if(ui->pb_D3->text().compare("D3")==0){
                m_brPoteza++;
                m_buttonsRemaining--;
            }
            if(ui->pb_D4->text().compare("D4")==0){
                m_brPoteza++;
                m_buttonsRemaining--;
            }

            ui->le_D->setText(attemptString);
            ui->pb_D1->setText(data[6]);
            ui->pb_D2->setText(data[7]);
            ui->pb_D3->setText(data[8]);
            ui->pb_D4->setText(data[9]);


            paintWidget(ui->pb_D1,m_id,m_turn);
            paintWidget(ui->pb_D2,m_id,m_turn);
            paintWidget(ui->pb_D3,m_id,m_turn);
            paintWidget(ui->pb_D4,m_id,m_turn);
            paintWidget(ui->le_D,m_id,m_turn);
            setColorYellow(ui->le_D);

            m_timer->start(1000);
            m_time+=5;
            ui->leTimer->setText(QString::number(m_time));

            disableD();
        }

        if (columnLetter == 'K')
        {
            m_timer->stop();
            ui->le_KONACNO->setText(attemptString);
            disableLineEditForever(ui->le_KONACNO);

            setAllFields(data, true);

            QTimer::singleShot(4000, this, &AsocijacijeUi::nextGame);
            return;

        }

        if (!availableGuesses())
            changeTurn();
        else if (m_turn)
            enableAllRemainingLineEdits();


    }
    else
    {
        if (columnLetter == 'A')
        {
            ui->le_A->setText(attemptString);
            // should last a little bit
            QTimer::singleShot(2000, this, &AsocijacijeUi::changeTurn);
        }

        if (columnLetter == 'B')
        {
            ui->le_B->setText(attemptString);
            QTimer::singleShot(2000, this, &AsocijacijeUi::changeTurn);
        }

        if (columnLetter == 'C')
        {
            ui->le_C->setText(attemptString);
            QTimer::singleShot(2000, this, &AsocijacijeUi::changeTurn);
        }

        if (columnLetter == 'D')
        {
            ui->le_D->setText(attemptString);
            QTimer::singleShot(2000, this, &AsocijacijeUi::changeTurn);
        }

        if (columnLetter == 'K')
        {
            ui->le_KONACNO->setText(attemptString);
            QTimer::singleShot(2000, this, &AsocijacijeUi::changeTurn);
        }
    }

    qDebug() << "BROJ NEOTVORENIH DUGMICA: " << m_buttonsRemaining;
}

void AsocijacijeUi::setAllFields(QStringList data, bool paint)
{
    if (paint)
    {
        paintWidget(ui->le_A,m_id,m_turn);
        paintWidget(ui->le_B,m_id,m_turn);
        paintWidget(ui->le_C,m_id,m_turn);
        paintWidget(ui->le_D,m_id,m_turn);
        paintWidget(ui->le_KONACNO,m_id,m_turn);

        paintWidget(ui->pb_A1,m_id,m_turn);
        paintWidget(ui->pb_A2,m_id,m_turn);
        paintWidget(ui->pb_A3,m_id,m_turn);
        paintWidget(ui->pb_A4,m_id,m_turn);

        paintWidget(ui->pb_B1,m_id,m_turn);
        paintWidget(ui->pb_B2,m_id,m_turn);
        paintWidget(ui->pb_B3,m_id,m_turn);
        paintWidget(ui->pb_B4,m_id,m_turn);

        paintWidget(ui->pb_C1,m_id,m_turn);
        paintWidget(ui->pb_C2,m_id,m_turn);
        paintWidget(ui->pb_C3,m_id,m_turn);
        paintWidget(ui->pb_C4,m_id,m_turn);

        paintWidget(ui->pb_D1,m_id,m_turn);
        paintWidget(ui->pb_D2,m_id,m_turn);
        paintWidget(ui->pb_D3,m_id,m_turn);
        paintWidget(ui->pb_D4,m_id,m_turn);
    }

    setColorYellow(ui->le_A);
    setColorYellow(ui->le_B);
    setColorYellow(ui->le_C);
    setColorYellow(ui->le_D);
    setColorYellow(ui->le_KONACNO);

    ui->le_A->setText(data[6]);
    ui->le_B->setText(data[7]);
    ui->le_C->setText(data[8]);
    ui->le_D->setText(data[9]);
    ui->le_KONACNO->setText(data[10]);

    ui->pb_A1->setText(data[11]);
    ui->pb_A2->setText(data[12]);
    ui->pb_A3->setText(data[13]);
    ui->pb_A4->setText(data[14]);

    ui->pb_B1->setText(data[15]);
    ui->pb_B2->setText(data[16]);
    ui->pb_B3->setText(data[17]);
    ui->pb_B4->setText(data[18]);

    ui->pb_C1->setText(data[19]);
    ui->pb_C2->setText(data[20]);
    ui->pb_C3->setText(data[21]);
    ui->pb_C4->setText(data[22]);

    ui->pb_D1->setText(data[23]);
    ui->pb_D2->setText(data[24]);
    ui->pb_D3->setText(data[25]);
    ui->pb_D4->setText(data[26]);

    disableKonacno();
}

void AsocijacijeUi::updatePoints(QStringList data)
{
    ui->lcdPlayer1->display(ui->lcdPlayer1->intValue() + data[4].toInt());
    ui->lcdPlayer2->display(ui->lcdPlayer2->intValue() + data[5].toInt());
}

void AsocijacijeUi::disableAllTemp()
{
    ui->le_KONACNO->setDisabled(true);
    ui->le_A->setDisabled(true);
    ui->le_B->setDisabled(true);
    ui->le_C->setDisabled(true);
    ui->le_D->setDisabled(true);

    disableOpeningFieldsTemp();
}

void AsocijacijeUi::disableOpeningFieldsTemp()
{
    ui->pb_A1->setDisabled(true);
    ui->pb_A2->setDisabled(true);
    ui->pb_A3->setDisabled(true);
    ui->pb_A4->setDisabled(true);

    ui->pb_B1->setDisabled(true);
    ui->pb_B2->setDisabled(true);
    ui->pb_B3->setDisabled(true);
    ui->pb_B4->setDisabled(true);

    ui->pb_C1->setDisabled(true);
    ui->pb_C2->setDisabled(true);
    ui->pb_C3->setDisabled(true);
    ui->pb_C4->setDisabled(true);

    ui->pb_D1->setDisabled(true);
    ui->pb_D2->setDisabled(true);
    ui->pb_D3->setDisabled(true);
    ui->pb_D4->setDisabled(true);

    return;
}

void AsocijacijeUi::disableKonacno()
{
    ui->le_KONACNO->setDisabled(true);

    disableA();
    disableB();
    disableC();
    disableD();
}

void AsocijacijeUi::disableA()
{
    disableButtonForever(ui->pb_A1);
    disableButtonForever(ui->pb_A2);
    disableButtonForever(ui->pb_A3);
    disableButtonForever(ui->pb_A4);
    disableLineEditForever(ui->le_A);
}

void AsocijacijeUi::disableB()
{
    disableButtonForever(ui->pb_B1);
    disableButtonForever(ui->pb_B2);
    disableButtonForever(ui->pb_B3);
    disableButtonForever(ui->pb_B4);
    disableLineEditForever(ui->le_B);
}

void AsocijacijeUi::disableC()
{
    disableButtonForever(ui->pb_C1);
    disableButtonForever(ui->pb_C2);
    disableButtonForever(ui->pb_C3);
    disableButtonForever(ui->pb_C4);
    disableLineEditForever(ui->le_C);
}

void AsocijacijeUi::disableD()
{
    disableButtonForever(ui->pb_D1);
    disableButtonForever(ui->pb_D2);
    disableButtonForever(ui->pb_D3);
    disableButtonForever(ui->pb_D4);
    disableLineEditForever(ui->le_D);
}

void AsocijacijeUi::addButtonsToMap()
{
    m_enableButtonsVector[ui->pb_A1]++;
    m_enableButtonsVector[ui->pb_A2]++;
    m_enableButtonsVector[ui->pb_A3]++;
    m_enableButtonsVector[ui->pb_A4]++;

    m_enableButtonsVector[ui->pb_B1]++;
    m_enableButtonsVector[ui->pb_B2]++;
    m_enableButtonsVector[ui->pb_B3]++;
    m_enableButtonsVector[ui->pb_B4]++;

    m_enableButtonsVector[ui->pb_C1]++;
    m_enableButtonsVector[ui->pb_C2]++;
    m_enableButtonsVector[ui->pb_C3]++;
    m_enableButtonsVector[ui->pb_C4]++;

    m_enableButtonsVector[ui->pb_D1]++;
    m_enableButtonsVector[ui->pb_D2]++;
    m_enableButtonsVector[ui->pb_D3]++;
    m_enableButtonsVector[ui->pb_D4]++;

    m_enableLineEditVector[ui->le_A] = 0;
    m_enableLineEditVector[ui->le_B] = 0;
    m_enableLineEditVector[ui->le_C] = 0;
    m_enableLineEditVector[ui->le_D] = 0;
    m_enableLineEditVector[ui->le_KONACNO] = 0;
}

void AsocijacijeUi::disableButtonForever(QPushButton* btn)
{
    btn->setDisabled(true);
    m_enableButtonsVector[btn] = 0;
}

void AsocijacijeUi::disableLineEditForever(QLineEdit* line)
{
    line->setDisabled(true);
    m_enableLineEditVector[line] = 0;
}

void AsocijacijeUi::changeTurn()
{
    m_brPoteza++;
    if (m_brPoteza == 20)
    {
        m_timer->stop();
        requestSolution();
        return;
    }

    clearRemainingLineEdits();

    if (m_turn)
        disableAllTemp();
    else
    {
        if (m_buttonsRemaining == 0)
            enableAllRemainingLineEdits();
        else
            enableAllRemainingButtons();
    }

    m_time=10;
    ui->leTimer->setText(QString::number(m_time));
    m_timer->start(1000);
    m_turn = !m_turn;

}

void AsocijacijeUi::enableAllRemainingButtons()
{
    for (QPushButton* btn: m_enableButtonsVector.keys())
        if (m_enableButtonsVector[btn])
            btn->setEnabled(true);
}

void AsocijacijeUi::enableAllRemainingLineEdits()
{
    for (QLineEdit* line: m_enableLineEditVector.keys())
        if (m_enableLineEditVector[line])
        {
            line->clear();
            line->setEnabled(true);

        }
}

bool AsocijacijeUi::availableGuesses()
{
    for (QLineEdit* line: m_enableLineEditVector.keys())
        if (m_enableLineEditVector[line])
            return true;

    return false;
}

void AsocijacijeUi::clearRemainingLineEdits()
{
    for (QLineEdit* line: m_enableLineEditVector.keys())
        if (m_enableLineEditVector[line])
            line->clear();
}

void AsocijacijeUi::nextGame()
{
    qDebug() << "Disconnecti";
    disconnect(m_server, SIGNAL(readyRead()), this, SLOT(readyRead()));
    disconnect(m_timer,SIGNAL(timeout()),this,SLOT(updateTime()));
    disconnect(this,&AsocijacijeUi::switch_turn,this,&AsocijacijeUi::on_switchTurn);
    qDebug() << "Emit end game";
    emit endGame();
}

void AsocijacijeUi::updateTime()
{

    m_time-=1;
    ui->leTimer->setText(QString::number(m_time));

    if(m_time==0)
    {
        m_timer->stop();
        if(m_turn)
            emit switch_turn();
    }

}

void AsocijacijeUi::on_switchTurn()
{

    QMapIterator<QPushButton*, int> pbMap(m_enableButtonsVector);



    while(pbMap.hasNext()){
        pbMap.next();
        if(pbMap.value()>0){
            pbMap.key()->click();
            //m_enableButtonsVector[pbMap.key()]=0;

            break;
        }
    }
    QTimer::singleShot(1000, this, &AsocijacijeUi::posalji_Prazno);

}

QMap<QLineEdit *, int> &AsocijacijeUi::enableLineEditVector()
{
    return m_enableLineEditVector;
}

void AsocijacijeUi::setEnableLineEditVector(const QMap<QLineEdit *, int> &newEnableLineEditVector)
{
    m_enableLineEditVector = newEnableLineEditVector;
}

 QMap<QPushButton *, int> &AsocijacijeUi::enableButtonsVector()
{
    return m_enableButtonsVector;
}

void AsocijacijeUi::setEnableButtonsVector(const QMap<QPushButton *, int> &newEnableButtonsVector)
{
    m_enableButtonsVector = newEnableButtonsVector;
}

Ui::AsocijacijeUi *AsocijacijeUi::getUi() const
{
    return ui;
}

void AsocijacijeUi::posalji_Prazno(){


    QMapIterator<QLineEdit*, int> leMap(m_enableLineEditVector);
    while(leMap.hasNext()){
        leMap.next();
        if(leMap.value()>0){

            leMap.key()->setText("a");
            disableAllTemp();

            leMap.key()->returnPressed();
            break;
        }
    }
}

void AsocijacijeUi::paintWidget(QWidget *w,int id,bool turn){




    // Get the QColor object for the background color
    QColor backgroundColor = w->palette().color(w->backgroundRole());

    // You can now use the backgroundColor object to access the
    // red, green, blue, and alpha values of the color
    int red = backgroundColor.red();
    int green = backgroundColor.green();
    int blue = backgroundColor.blue();



    if((red == 169  and green == 216 and blue == 255) or (red == 76  and green == 130 and blue == 196)){
        if((turn and id==1) or (!turn and id==2)){


        w->setStyleSheet(w->styleSheet().append(":disabled{background-color: rgb(14, 65, 157);}"));
         }
        else {
        w->setStyleSheet(w->styleSheet().append(":disabled{background-color: rgb(193, 31, 53);}"));
        }
    }

}


void AsocijacijeUi::setColorYellow(QWidget *w){
    w->setStyleSheet(w->styleSheet().append(":disabled{color: yellow;}"));

}

void AsocijacijeUi::requestSolution()
{
    QString serverMsg = QString::number(m_id) + "_s_a_" + QString::number(m_associationNumber) + "_s";

    sendMessage(m_server, serverMsg);
}
