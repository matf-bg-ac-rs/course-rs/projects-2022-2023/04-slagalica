#ifndef WINNERUI_H
#define WINNERUI_H

#include <QWidget>
#include <QTcpServer>

namespace Ui {
class WinnerUi;
}

class WinnerUi : public QWidget
{
    Q_OBJECT

public:
    enum class Outcomes {
        Win,
        Lose,
        Tie
    };
    ~WinnerUi();

    WinnerUi(QWidget *parent = nullptr,
             QTcpSocket *server = nullptr,
             int playerId = -1,
             QString clientName1 = {},
             QString clientName2 = {},
             int playerPoints1 = 0,
             int playerPoints2 = 0);
    void sendMessage(QTcpSocket* server, QString msg);
    Outcomes checkWinner();
private:
    Ui::WinnerUi *ui;
    QTcpSocket* m_server;
    int m_playerId;
    QString m_clientName1;
    QString m_clientName2;

    int m_playerPoints1;
    int m_playerPoints2;
private:
    signals:
        void goToMainMenu();
private slots:
    void on_pbMainMenuClicked();
};

#endif // WINNERUI_H
