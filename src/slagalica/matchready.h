#ifndef MATCHREADY_H
#define MATCHREADY_H

#include <QDialog>

namespace Ui {
class MatchReady;
}

class MatchReady : public QDialog
{
    Q_OBJECT

public:
    explicit MatchReady(QWidget *parent = nullptr);
    ~MatchReady();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MatchReady *ui;
};

#endif // MATCHREADY_H
