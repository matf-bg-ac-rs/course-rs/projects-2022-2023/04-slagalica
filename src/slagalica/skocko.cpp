#include "skocko.h"
#include <QDebug>

Skocko::Skocko(){
    m_currentCombination={Symbols::Prazno, Symbols::Prazno, Symbols::Prazno, Symbols::Prazno};
    m_randomCombination=createRandomCombination();
    m_status={Status::Transparentno, Status::Transparentno, Status::Transparentno, Status::Transparentno};
   //init();

}
Skocko::~Skocko(){

}

void Skocko::guess(QVector<Symbols> symbols, QTcpSocket* server, int playerId,int skockoNum,int attemptNum){

    // provera attempt preko servera

    QString attemptString = combinationToAttemptString(symbols);
    QString msgString = QString::number(playerId) + "_a_s_"+ QString::number(skockoNum)+"_"+ QString::number(attemptNum)+"_" + attemptString;
    // poruka serveru -> playerId_a(attempt)_s(skocko)_redniBrojSkocka_redniBrojPokusaja_attemptString
    sendMessage(server, msgString);
}



void Skocko::init()
{



}

int Skocko::calculateScore()
{
    return 0;
}


QVector<Symbols> Skocko::createRandomCombination() {
    QVector<Symbols> combination = QVector<Symbols>{};
    double rnd;
    srand(time(nullptr));
    for (int i = 0; i < 50; i++)
            rnd = rand();


    for (int i = 0; i < 4; i++) {
        rnd = rand() / (double)RAND_MAX;

        if (rnd < 1. / 6)
             combination.push_back(Symbols::Skocko);
        else if (rnd < 2. / 6)
            combination.push_back(Symbols::Tref);
        else if (rnd < 3. / 6)
            combination.push_back(Symbols::Pik);
        else if (rnd < 4. / 6)
            combination.push_back(Symbols::Srce);
        else if (rnd < 5. / 6)
            combination.push_back(Symbols::Karo);
        else
            combination.push_back(Symbols::Zvezda);
    }

    return combination;
}


int Skocko::findFirstEmpty(){

  auto it=std::find(m_currentCombination.cbegin(), m_currentCombination.cend(), Symbols::Prazno);

   if(it!=m_currentCombination.cend()){
       return it - m_currentCombination.cbegin();
   }

   return -1;

}

void Skocko::resetSymbolCombination(int column)
{
    m_currentCombination[column-1] = Symbols::Prazno;
}

bool Skocko::isCombinationCompleted() const
{
    auto it = std::find(m_currentCombination.cbegin(), m_currentCombination.cend(), Symbols::Prazno);
    return it == m_currentCombination.cend();
}

int Skocko::getCurrentRow() const
{
    return m_currentRow;
}
void Skocko::setCurrentRow(int currentRow){

    m_currentRow=currentRow;
}
QString Skocko::combinationToAttemptString(QList<Symbols> combination)
{
    //prebacuje listu simbola u string koji se salje posle serveru

    QString attemptString = "";
    for (Symbols symbol: combination)
    {
        if (symbol == Symbols::Karo)
            attemptString += "k";
        if (symbol == Symbols::Pik)
            attemptString += "p";
        if (symbol == Symbols::Skocko)
            attemptString += "S";
        if (symbol == Symbols::Srce)
            attemptString += "s";
        if (symbol == Symbols::Tref)
            attemptString += "t";
        if (symbol == Symbols::Zvezda)
            attemptString += "z";
        if (symbol == Symbols::Prazno)
            attemptString += "q";
    }

    return attemptString;
}

QVector<Status> Skocko::attemptStringToStatus(QString attemptString)
{
    //prebacuje string odgovor servera u vektor statusa

    QVector<Status> attemptStatus;

    int redStatusCount = attemptString[0].digitValue();
    int yellowStatusCount = attemptString[1].digitValue();

    qDebug() << attemptString << redStatusCount << yellowStatusCount;

    for (int i = 0; i < redStatusCount; i++)
        attemptStatus.append(Status::Crveno);
    for (int i = 0; i < yellowStatusCount; i++)
        attemptStatus.append(Status::Zuto);
    for (int i = 0; i < 4 - (redStatusCount + yellowStatusCount); i++)
        attemptStatus.append(Status::Transparentno);

    return attemptStatus;

}

void Skocko::sendMessage(QTcpSocket* server, QString msg)
{
    server->write(msg.toUtf8());
    server->flush();
}















