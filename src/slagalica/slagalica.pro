QT       += core gui \
            multimedia
QT += multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17 Wall Wextra

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    asocijacije.cpp \
    asocijacijeui.cpp \
    clickablelabel.cpp \
    game.cpp \
    koznazna.cpp \
    koznaznaui.cpp \
    main.cpp \
    mainwindow.cpp \
    matchready.cpp \
    model.cpp \
    modelitem.cpp \
    najboljirezultati.cpp \
    player.cpp \
    skocko.cpp \
    skockoui.cpp \
    slagalica.cpp \
    spojnice.cpp \
    spojniceui.cpp \
    winnerui.cpp

HEADERS += \
    asocijacije.h \
    asocijacijeui.h \
    clickablelabel.h \
    game.h \
    koznazna.h \
    koznaznaui.h \
    mainwindow.h \
    matchready.h \
    model.h \
    modelitem.h \
    najboljirezultati.h \
    player.h \
    skocko.h \
    skockoui.h \
    slagalica.h \
    spojnice.h \
    spojniceui.h \
    winnerui.h

FORMS += \
    asocijacijeui.ui \
    koznaznaui.ui \
    mainwindow.ui \
    matchready.ui \
    najboljirezultati.ui \
    skockoui.ui \
    spojniceui.ui \
    winnerui.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target



RESOURCES += \
    resources.qrc

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../serialization/release/ -lserialization
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../serialization/debug/ -lserialization
else:unix: LIBS += -L$$OUT_PWD/../serialization/ -lserialization

INCLUDEPATH += $$PWD/../serialization
DEPENDPATH += $$PWD/../serialization

DISTFILES +=
