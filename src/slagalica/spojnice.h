#ifndef SPOJNICE_H
#define SPOJNICE_H

#include <iostream>
#include <QString>
#include <QPair>
#include <QVector>
#include <QMap>

#include "game.h"
#include "qtcpsocket.h"


class Spojnice: public Game
{
public:
    Spojnice();
    virtual ~Spojnice();
    bool m_turnCounter;
    QMap<QString, QString> m_result;

    void guess(int leftColumnNumber, int rightColumnNumber, int playerId, int spojniceNumber, QTcpSocket *server);
    virtual void init() override;
    virtual int calculateScore() override;
    int calcluateScore(bool correct) const;

    int getCurrentRow() const;
    void setCurrentRow(int row);
    void incrementCurrentRow();

    void sendMessage(QTcpSocket *server, QString msg);

private:
    int m_currentRow = 1;

};




#endif // SPOJNICE_H
