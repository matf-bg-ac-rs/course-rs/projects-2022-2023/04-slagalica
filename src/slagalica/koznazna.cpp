#include "koznazna.h"


void KoZnaZna::guess(int questionNumber, int guessNumber, QTcpSocket* server, int playerId, int msTime)
{
    QString msgString =  QString::number(playerId)
                       + "_a_k_"
                       + QString::number(questionNumber)
                       + "_" + QString::number(guessNumber)
                       + "_" + QString::number(msTime);
    // poruka serveru -> playerId_a(attempt)_k(koZnazna)_i(brojPitanja)_answer(brojOdgovora - A->0, B->1, C->2, D->3)_timeToAns
    sendMessage(server, msgString);
    //server->waitForReadyRead(-1);
    //QString attemptResult = server->readAll();
    // poruka od servera -> brOdgIgrac1_brOdgIgrac2_brTacnogOdg_poeniIgraca1(0 znaci 0, 5 znaci -5, 1 znaci +10)_poeniIgraca2
    //qDebug() << attemptResult;

    //return attemptResult;

}

void KoZnaZna::init()
{

}

int KoZnaZna::calculateScore()
{
    return 0;
}

int KoZnaZna::calculateScore(bool guessed, double myTime, double opponentTime)
{
    if(guessed && (myTime < opponentTime))
        return 10;
    else if(!guessed){
        return -5;
    }

    return 0;
}

void KoZnaZna::sendMessage(QTcpSocket* server, QString msg)
{
    server->write(msg.toUtf8());
    server->flush();
}
