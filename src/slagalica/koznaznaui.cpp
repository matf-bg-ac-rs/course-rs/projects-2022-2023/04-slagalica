#include "koznaznaui.h"
#include "ui_koznaznaui.h"
#include "koznazna.h"
#include <QDebug>
#include <cstring>
#include <iostream>
#include <QTimer>

Koznaznaui::Koznaznaui(QWidget *parent, QTcpSocket *server, int playerId, QString clientName1, QString clientName2, int questionCounter,int playerPoints1,int playerPoints2) :
    QWidget(parent),
    ui(new Ui::Koznaznaui)
{
    m_id = playerId;
    m_server = server;
    m_clientName1=clientName1;
    m_clientName2= clientName2;
    connect(m_server, SIGNAL(readyRead()), this, SLOT(readyRead()));
    ui->setupUi(this);

    ui->lcd_score1->display(playerPoints1);
    ui->lcd_score2->display(playerPoints2);

    ui->lineEdit_2->setText(m_clientName1);
    ui->lineEdit_5->setText(m_clientName2);

    m_koznazna = new KoZnaZna();
    m_questionCounter = questionCounter;

    m_time = 10;
    ui->leTimer_2->setText(QString::number(m_time));
    m_timeMs = 0;

    m_timer = new QTimer(this);
    connect(m_timer,SIGNAL(timeout()),this,SLOT(updateTime()));
    connect(this,&Koznaznaui::timesUp,this,&Koznaznaui::on_timesUp);
    m_timer->start(1000);


    m_timerMs = new QTimer(this);
    connect(m_timerMs,SIGNAL(timeout()),this,SLOT(updateTimeMs()));
    m_timerMs->start(1);

}

Koznaznaui::~Koznaznaui()
{
    delete m_koznazna;
    delete ui;
}

void Koznaznaui::on_pb_A_clicked()
{
    disableUi();
    m_koznazna->guess(m_questionCounter, 0, m_server, m_id, m_timeMs);
}

void Koznaznaui::on_pb_B_clicked()
{
   disableUi();
   m_koznazna->guess(m_questionCounter, 1, m_server, m_id, m_timeMs);
}

void Koznaznaui::on_pb_C_clicked()
{
    disableUi();
    m_koznazna->guess(m_questionCounter, 2, m_server, m_id, m_timeMs);
}

void Koznaznaui::on_pb_D_clicked()
{
    disableUi();
    m_koznazna->guess(m_questionCounter, 3, m_server, m_id, m_timeMs);
}

void Koznaznaui::on_timesUp()
{
    disableUi();
    m_koznazna->guess(m_questionCounter, 4, m_server, m_id, m_timeMs);
}

Ui::Koznaznaui *Koznaznaui::getUi()
{
    return ui;
}

void Koznaznaui::sendMessage(QTcpSocket* socket,QString msg)
{
    socket->write(msg.toUtf8());
    socket->flush();

}

void Koznaznaui::updateTime()
{

    m_time-=1;
    ui->leTimer_2->setText(QString::number(m_time));

    if(m_time==0){
        emit timesUp();
    }
}
void Koznaznaui::updateTimeMs()
{
    m_timeMs+=1;
}

void Koznaznaui::getQuestion(int questionNumber)
{
    QString msg = QString::number(m_id) + "_g_" + "k_" + QString::number(questionNumber);
    sendMessage(m_server, msg);
}

void Koznaznaui::readyRead()
{
    QString serverMsg = m_server->readAll();
    qDebug() << serverMsg;
    if (serverMsg[0] == 'g')
        displayQuestion(serverMsg.remove(0, 2));
    else
        displayAnswer(serverMsg.remove(0, 2));
}

const QString &Koznaznaui::clientName2() const
{
    return m_clientName2;
}

void Koznaznaui::setClientName2(const QString &newClientName2)
{
    m_clientName2 = newClientName2;
}

const QString &Koznaznaui::clientName1() const
{
    return m_clientName1;
}

void Koznaznaui::setClientName1(const QString &newClientName1)
{
    m_clientName1 = newClientName1;
}

int Koznaznaui::questionCounter() const
{
    return m_questionCounter;
}

void Koznaznaui::setQuestionCounter(int newQuestionCounter)
{
    m_questionCounter = newQuestionCounter;
}

int Koznaznaui::id() const
{
    return m_id;
}

void Koznaznaui::setId(int newId)
{
    m_id = newId;
}

void Koznaznaui::displayQuestion(QString question)
{

    enableUi();
    QStringList serverAnswerSplit = question.split("_");
    m_time = 10;
    ui->leTimer_2->setText(QString::number(m_time));
    getUi()->le_question->setText(serverAnswerSplit[0]);
    getUi()->pb_A->setText(serverAnswerSplit[1]);
    getUi()->pb_B->setText(serverAnswerSplit[2]);
    getUi()->pb_C->setText(serverAnswerSplit[3]);
    getUi()->pb_D->setText(serverAnswerSplit[4]);

    qDebug() << question;


    m_timeMs = 0;

    m_timer->start(1000);

    m_timerMs->start(1);

}

void Koznaznaui::displayAnswer(QString answer)
{
    qDebug() << answer;
    int current_score1 = getUi()->lcd_score1->intValue();
    int current_score2 = getUi()->lcd_score2->intValue();
    QStringList resultSplit = answer.split("_");

    int answerPlayer1 = resultSplit[0].toInt();
    int answerPlayer2 = resultSplit[1].toInt();
    int answer1 = resultSplit[2].toInt();


    int  scorePlayer1 = resultSplit[3].toInt();
    int  scorePlayer2 = resultSplit[4].toInt();
    if(scorePlayer1==5){
        current_score1-=5;
    }
    else if(scorePlayer1==1){
        current_score1+=10;
    }

    if(scorePlayer2==5){
        current_score2-=5;
    }
    else if(scorePlayer2==1){
        current_score2+=10;
    }

    getUi()->lcd_score1->display(current_score1);
    getUi()->lcd_score2->display(current_score2);

    paintButtons(answer1,answerPlayer1,answerPlayer2);

}

void Koznaznaui::disableUi()
{
    getUi()->pb_A->setDisabled(true);
    getUi()->pb_B->setDisabled(true);
    getUi()->pb_C->setDisabled(true);
    getUi()->pb_D->setDisabled(true);
    getUi()->pb_Next->setDisabled(true);
    m_timer->stop();
}

void Koznaznaui::enableUi()
{
    m_timer->start(1000);
    updateTime();
    getUi()->pb_A->setEnabled(true);
    getUi()->pb_B->setEnabled(true);
    getUi()->pb_C->setEnabled(true);
    getUi()->pb_D->setEnabled(true);
    getUi()->pb_Next->setEnabled(true);
}

void Koznaznaui::nextGame()
{
    disconnect(m_server, SIGNAL(readyRead()), this, SLOT(readyRead()));
    emit endGame();
}

int Koznaznaui::timeMs() const
{
    return m_timeMs;
}

void Koznaznaui::setTimeMs(int newTimeMs)
{
    m_timeMs = newTimeMs;
}

int Koznaznaui::time() const
{
    return m_time;
}

void Koznaznaui::setTime(int newTime)
{
    m_time = newTime;
}



void Koznaznaui::paintButtons(int answer,int answerPlayer1,int answerPlayer2){

    if (answer == 0){
        ui->pb_A->setStyleSheet(":disabled{background-color: rgb(169, 216, 255);border-style: solid;border-color: rgb(153, 255, 51);border-width: 5px;color:black}");
    }
    else if (answer == 1){
        ui->pb_B->setStyleSheet(":disabled{background-color: rgb(169, 216, 255);border-style: solid;border-color: rgb(153, 255, 51);border-width: 5px;color:black}");
    }
    else if (answer == 2){
        ui->pb_C->setStyleSheet(":disabled{background-color: rgb(169, 216, 255);border-style: solid;border-color: rgb(153, 255, 51);border-width: 5px;color:black}");
    }
    else if (answer == 3){
        ui->pb_D->setStyleSheet(":disabled{background-color: rgb(169, 216, 255);border-style: solid;border-color: rgb(153, 255, 51);border-width: 5px;color:black}");
    }



    if (answerPlayer1 == answerPlayer2){

            if (answerPlayer1 == 0){
                ui->pb_A->setStyleSheet(ui->pb_A->styleSheet().append(":disabled{background-color: qlineargradient(spread:pad, x1:0, y1:0.5, x2:1, y2:0.5, stop:0.45 rgba(14, 65, 157, 255), stop:0.55 rgba(193, 31, 53, 255));color:black}"));
            }
            else if (answerPlayer1 == 1){
                ui->pb_B->setStyleSheet(ui->pb_B->styleSheet().append(":disabled{background-color: qlineargradient(spread:pad, x1:0, y1:0.5, x2:1, y2:0.5, stop:0.45 rgba(14, 65, 157,255), stop:0.55 rgba(193, 31, 53, 255));color:black}"));

            }
            else if (answerPlayer1 == 2){
                ui->pb_C->setStyleSheet(ui->pb_C->styleSheet().append(":disabled{background-color: qlineargradient(spread:pad, x1:0, y1:0.5, x2:1, y2:0.5, stop:0.45 rgba(14, 65, 157,255), stop:0.55 rgba(193, 31, 53, 255));color:black}"));
            }
            else if (answerPlayer1 == 3){
                ui->pb_D->setStyleSheet(ui->pb_D->styleSheet().append(":disabled{background-color: qlineargradient(spread:pad, x1:0, y1:0.5, x2:1, y2:0.5, stop:0.45 rgba(14, 65, 157, 255), stop:0.55 rgba(193, 31, 53, 255));color:black}"));
            }
    }
    else {

            if (answerPlayer1 == 0){
                ui->pb_A->setStyleSheet(ui->pb_A->styleSheet().append(":disabled{background-color: rgb(14, 65, 157);color:black}"));
            }
            else if (answerPlayer1 == 1){
                ui->pb_B->setStyleSheet(ui->pb_B->styleSheet().append(":disabled{background-color: rgb(14, 65, 157);color:black}"));
            }
            else if (answerPlayer1 == 2){
                ui->pb_C->setStyleSheet(ui->pb_C->styleSheet().append(":disabled{background-color: rgb(14, 65, 157);color:black}"));
            }
            else if (answerPlayer1 == 3){
                ui->pb_D->setStyleSheet(ui->pb_D->styleSheet().append(":disabled{background-color: rgb(14, 65, 157);color:black}"));
            }

            if (answerPlayer2 == 0){
                ui->pb_A->setStyleSheet(ui->pb_A->styleSheet().append(":disabled{background-color: rgb(193, 31, 53);color:black}"));
            }
            else if (answerPlayer2 == 1){
                ui->pb_B->setStyleSheet(ui->pb_B->styleSheet().append(":disabled{background-color: rgb(193, 31, 53);color:black}"));
            }
            else if (answerPlayer2 == 2){
                ui->pb_C->setStyleSheet(ui->pb_C->styleSheet().append(":disabled{background-color: rgb(193, 31, 53);color:black}"));
            }
            else if (answerPlayer2 == 3){
                ui->pb_D->setStyleSheet(ui->pb_D->styleSheet().append(":disabled{background-color: rgb(193, 31, 53);color:black}"));
            }


      }



     QTimer::singleShot(2000, this, &Koznaznaui::restartButtons);

}

void Koznaznaui::restartButtons(){
    ui->pb_A->setStyleSheet(":enabled{background-color: rgb(169, 216, 255);}"
                            ":disabled { color: black;}");
    ui->pb_B->setStyleSheet(":enabled{background-color: rgb(169, 216, 255);}"
                            ":disabled { color: black;}");
    ui->pb_C->setStyleSheet(":enabled{background-color: rgb(169, 216, 255);}"
                            ":disabled { color: black;}");
    ui->pb_D->setStyleSheet(":enabled{background-color: rgb(169, 216, 255);}"
                            ":disabled { color: black;}");



    nextQuestion();
}

void Koznaznaui::nextQuestion(){


    m_questionCounter++;
    if (m_questionCounter == 10)
    {
        m_timer->stop();
        QTimer::singleShot(4000, this, &Koznaznaui::nextGame);
    }
    else
        getQuestion(m_questionCounter);
}


void Koznaznaui::on_pb_Next_clicked()
{
    on_timesUp();
}

