#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QtMultimedia/QMediaPlayer>
#include <QtMultimedia/QAudioOutput>
#include <QTcpSocket>
#include "najboljirezultati.h"
#include "skockoui.h"
#include "koznaznaui.h"
#include "asocijacijeui.h"
#include "spojniceui.h"
#include "winnerui.h"




QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    Ui::MainWindow *getUi() const;

private slots:
    void on_pb_LogIn_clicked();

    void on_pb_StartGame_clicked();

    void on_pb_Settings_clicked();

    void on_pb_Return_clicked();

    void on_sl_Volume_sliderMoved(int position);

    void on_pb_OnOff_toggled(bool checked);

    void on_EndSkocko();
    void on_EndKoZnaZna();
    void on_EndSpojnice();
    void on_EndAsocijacije();

    void connected();
    void disconnected();
    void showMessageBox(QString content) const;
private:
    Ui::MainWindow *ui;


    QTcpSocket *m_socket;
    bool connectToServer();
    void sendMessage(QTcpSocket *socket, QString msg);

    QString m_username;
    QString m_username2;
    int m_playerId;


    QMediaPlayer * mp;
    QAudioOutput * audioOutput;

    SkockoUi* skockoUi;
    Koznaznaui* m_koznaznaUi;
    AsocijacijeUi* asocijacijeUi;
    SpojniceUi* spojniceUi;
    NajboljiRezultati* najboljiRezultatiUi;
    WinnerUi* winnerUi;

};
#endif // MAINWINDOW_H
