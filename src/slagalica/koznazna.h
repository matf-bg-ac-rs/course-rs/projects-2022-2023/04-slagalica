#ifndef KOZNAZNA_H
#define KOZNAZNA_H


#include <QString>
#include <QPair>
#include <QVector>
#include <QMap>
#include <QTcpSocket>

#include "game.h"

class KoZnaZna: public Game
{
public:

    KoZnaZna() = default;
    virtual ~KoZnaZna() = default;

    bool m_turnCounter;

    QVector<QMap<QString, QVector<QPair<QString, bool>>>> m_result;

    void guess(int questionNumber, int guessNumber, QTcpSocket* server, int clientId, int msTime);
    void sendMessage(QTcpSocket* server, QString msg);

    virtual void init() override;
    virtual int calculateScore() override;
    int calculateScore(bool guessed,double time , double opponentTime);

};

#endif // KOZNAZNA_H
