#include "skockoui.h"
#include "ui_skockoui.h"
#include "skocko.h"
#include <QDebug>
#include <cstring>
#include <iostream>
#include "clickablelabel.h"

SkockoUi::SkockoUi(QWidget *parent, QTcpSocket *server, int playerId, QString clientName1, QString clientName2, bool turn,int skockoNum, int playerPoints1, int playerPoints2) :
    QWidget(parent),
    m_turn(turn),
    ui(new Ui::SkockoUi)

{
    m_server = server;
    m_playerId=playerId;
    m_skockoNum=skockoNum;
    m_clientName1=clientName1;
    m_clientName2= clientName2;

    connect(m_server,SIGNAL(readyRead()),this,SLOT(readyRead()));

    ui->setupUi(this);

    // show points to lcd widgets
    ui->lcdPlayer1->display(playerPoints1);
    ui->lcdPlayer2->display(playerPoints2);



    ui->lineEdit->setText(m_clientName1);
    ui->lineEdit_4->setText(m_clientName2);

    m_time = 60;
    ui->leTimer->setText(QString::number(m_time));

    m_timer = new QTimer(this);
    connect(m_timer,SIGNAL(timeout()),this,SLOT(updateTime()));
    connect(this,&SkockoUi::timesUp,this,&SkockoUi::on_timesUp);

    m_extraTimer = new QTimer(this);
    connect(m_extraTimer,SIGNAL(timeout()),this,SLOT(updateExtraTime()));
    connect(this,&SkockoUi::extraTimesUp,this,&SkockoUi::on_extraTimesUp);

    m_timer->start(1000);

    connect(ui->pbAddOwls, &QPushButton::clicked, this, &SkockoUi::on_btSkocko);
    connect(ui->pbAddAceOfSpades, &QPushButton::clicked, this, &SkockoUi::on_btPik);
    connect(ui->pbAddClubs, &QPushButton::clicked, this, &SkockoUi::on_btTref);
    connect(ui->pbAddDiamonds, &QPushButton::clicked, this, &SkockoUi::on_btKaro);
    connect(ui->pbAddHearts, &QPushButton::clicked, this, &SkockoUi::on_btSrce);
    connect(ui->pbAddStars, &QPushButton::clicked, this, &SkockoUi::on_btZvezda);

    connect(ui->pbConfirmRow1, &QPushButton::clicked, this, &SkockoUi::on_btPotvrdi_1);
    connect(ui->pbConfirmRow2, &QPushButton::clicked, this, &SkockoUi::on_btPotvrdi_2);
    connect(ui->pbConfirmRow3, &QPushButton::clicked, this, &SkockoUi::on_btPotvrdi_3);
    connect(ui->pbConfirmRow4, &QPushButton::clicked, this, &SkockoUi::on_btPotvrdi_4);
    connect(ui->pbConfirmRow5, &QPushButton::clicked, this, &SkockoUi::on_btPotvrdi_5);
    connect(ui->pbConfirmRow6, &QPushButton::clicked, this, &SkockoUi::on_btPotvrdi_6);
    connect(ui->pbConfirmExtra, &QPushButton::clicked, this, &SkockoUi::on_btPotvrdiExtra);

    if(!turn){
        ui->pbConfirmRow1->setEnabled(false);
        ui->pbConfirmRow2->setEnabled(false);
        ui->pbConfirmRow3->setEnabled(false);
        ui->pbConfirmRow4->setEnabled(false);
        ui->pbConfirmRow5->setEnabled(false);
        ui->pbConfirmRow6->setEnabled(false);

        ui->pbAddAceOfSpades->setEnabled(false);
        ui->pbAddClubs->setEnabled(false);
        ui->pbAddDiamonds->setEnabled(false);
        ui->pbAddHearts->setEnabled(false);
        ui->pbAddOwls->setEnabled(false);
        ui->pbAddStars->setEnabled(false);

    }

    connect(ui->lbRow1Column1, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow1Column1Clicked);
    connect(ui->lbRow1Column2, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow1Column2Clicked);
    connect(ui->lbRow1Column3, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow1Column3Clicked);
    connect(ui->lbRow1Column4, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow1Column4Clicked);

    connect(ui->lbRow2Column1, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow2Column1Clicked);
    connect(ui->lbRow2Column2, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow2Column2Clicked);
    connect(ui->lbRow2Column3, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow2Column3Clicked);
    connect(ui->lbRow2Column4, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow2Column4Clicked);


    connect(ui->lbRow3Column1, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow3Column1Clicked);
    connect(ui->lbRow3Column2, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow3Column2Clicked);
    connect(ui->lbRow3Column3, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow3Column3Clicked);
    connect(ui->lbRow3Column4, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow3Column4Clicked);


    connect(ui->lbRow4Column1, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow4Column1Clicked);
    connect(ui->lbRow4Column2, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow4Column2Clicked);
    connect(ui->lbRow4Column3, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow4Column3Clicked);
    connect(ui->lbRow4Column4, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow4Column4Clicked);

    connect(ui->lbRow5Column1, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow5Column1Clicked);
    connect(ui->lbRow5Column2, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow5Column2Clicked);
    connect(ui->lbRow5Column3, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow5Column3Clicked);
    connect(ui->lbRow5Column4, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow5Column4Clicked);

    connect(ui->lbRow6Column1, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow6Column1Clicked);
    connect(ui->lbRow6Column2, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow6Column2Clicked);
    connect(ui->lbRow6Column3, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow6Column3Clicked);
    connect(ui->lbRow6Column4, &ClickableLabel::clicked, this, &SkockoUi::on_lbRow6Column4Clicked);

    connect(ui->lbExtraColumn1, &ClickableLabel::clicked, this, &SkockoUi::on_lbRowExtraColumn1Clicked);
    connect(ui->lbExtraColumn2, &ClickableLabel::clicked, this, &SkockoUi::on_lbRowExtraColumn2Clicked);
    connect(ui->lbExtraColumn3, &ClickableLabel::clicked, this, &SkockoUi::on_lbRowExtraColumn3Clicked);
    connect(ui->lbExtraColumn4, &ClickableLabel::clicked, this, &SkockoUi::on_lbRowExtraColumn4Clicked);


    initController();
}

void SkockoUi::initController(){
    m_skockoController= new Skocko();
}


SkockoUi::~SkockoUi()
{
    delete m_skockoController;
    delete ui;
}

Ui::SkockoUi *SkockoUi::getUi() const
{
    return ui;
}

int SkockoUi::playerId() const
{
    return m_playerId;
}

void SkockoUi::setPlayerId(int newPlayerId)
{
    m_playerId = newPlayerId;
}

int SkockoUi::time() const
{
    return m_time;
}

void SkockoUi::setTime(int newTime)
{
    m_time = newTime;
}

void SkockoUi::readyRead()
{
    QString attemptResult = m_server->readAll();
    qDebug() << "line 116 read"<< attemptResult;
    // poruka od servera -> pogodjena kombinacije ( intint -> broj crvenih i zutih)_(sama kombinacija)_(broj poena)_turn(nema na 7om pokusaju!)_tacnaKomb(samo na 7om pokusaju!)
    QList<QString> data = attemptResult.trimmed().split("_");
    auto status = attemptStringToStatus(data[0]);


    int counter = 0;
    for (auto status: status){
        if (status == Status::Crveno){
            counter++;
        }
    }

    QVector<QLabel*> labelRow;

    int row = m_skockoController->getCurrentRow();

    switch (row) {
        case 1:

             labelRow= {ui->lbResultRow1Colum1, ui->lbResultRow1Colum2, ui->lbResultRow1Colum3, ui->lbResultRow1Colum4,};
             ui->pbConfirmRow1->setEnabled(false);

        break;
        case 2:
             labelRow= {ui->lbResultRow2Colum1, ui->lbResultRow2Colum2, ui->lbResultRow2Colum3, ui->lbResultRow2Colum4,};
             ui->pbConfirmRow2->setEnabled(false);

        break;
        case 3:
             labelRow= {ui->lbResultRow3Colum1, ui->lbResultRow3Colum2, ui->lbResultRow3Colum3, ui->lbResultRow3Colum4,};
             ui->pbConfirmRow3->setEnabled(false);

        break;
        case 4:
             labelRow= {ui->lbResultRow4Colum1, ui->lbResultRow4Colum2, ui->lbResultRow4Colum3, ui->lbResultRow4Colum4,};
             ui->pbConfirmRow4->setEnabled(false);

        break;
        case 5:
             labelRow= {ui->lbResultRow5Colum1, ui->lbResultRow5Colum2, ui->lbResultRow5Colum3, ui->lbResultRow5Colum4,};
             ui->pbConfirmRow5->setEnabled(false);

        break;
        case 6:
             labelRow= {ui->lbResultRow6Colum1, ui->lbResultRow6Colum2, ui->lbResultRow6Colum3, ui->lbResultRow6Colum4,};
             ui->pbConfirmRow6->setEnabled(false);
        break;
        case 7:
            labelRow = {ui->lbResultRowExtraColum1, ui->lbResultRowExtraColum2, ui->lbResultRowExtraColum3, ui->lbResultRowExtraColum4};
            break;
        default:

        break;
    }


    bool correctGuessExit = counter == 4;
    bool extraTurnFinishedExit = m_skockoController->getCurrentRow() == 7;

    bool exitCondition = (correctGuessExit || extraTurnFinishedExit);

    if (exitCondition){

        if (m_timer != nullptr){
            m_timer->stop();
        }
        if (m_extraTimer != nullptr){
            m_extraTimer->stop();
        }

        ui->leTimer->setText(0);

        m_skockoController->setCurrentRow(-1);
        QString poeni = data[2];

        auto currentPointsPlayer1 = static_cast<int>(ui->lcdPlayer1->value());
        auto currentPointsPlayer2 = static_cast<int>(ui->lcdPlayer2->value());

        if (m_turn){
            if (m_playerId == 1){
                ui->lcdPlayer1->display(currentPointsPlayer1 += poeni.toInt());
            } else if (m_playerId == 2){
                ui->lcdPlayer2->display(currentPointsPlayer2 += poeni.toInt());
            }
        } else{
            if (m_playerId == 2){
                ui->lcdPlayer1->display(currentPointsPlayer1 += poeni.toInt());
            } else if (m_playerId == 1){
                ui->lcdPlayer2->display(currentPointsPlayer2 += poeni.toInt());
            }
        }

        // show correct symbols
        if (extraTurnFinishedExit)
        {
            // swap to original m_turn
            m_turn = !m_turn;

            if (currentPointsPlayer1 + currentPointsPlayer2 == 0)
            {
                QVector<QLabel*> symbolsResultLabels = {ui->lbResultSymbolsColumn1, ui->lbResultSymbolsColumn2, ui->lbResultSymbolsColumn3, ui->lbResultSymbolsColumn4};
                for (int i = 0; i < 4; i++)
                {
                    Symbols symbol;
                    if (data[3][i] == QString("s")[0]){
                        symbol = Symbols::Srce;
                    }
                    else if (data[3][i] == QString("S")[0]){
                        symbol = Symbols::Skocko;
                    }
                    else if (data[3][i] == QString("t")[0]){
                        symbol = Symbols::Tref;
                    }
                    else if (data[3][i] == QString("k")[0]){
                        symbol = Symbols::Karo;
                    }
                    else if (data[3][i] == QString("z")[0]){
                        symbol = Symbols::Zvezda;
                    }
                    else if (data[3][i] == QString("p")[0]){
                        symbol = Symbols::Pik;
                    }
                    else
                    {
                        qDebug() << "Wrong symbol on result label!";
                        return;
                    }

                    setIconOnLabel(symbol, 8, i);
                }
            }
        }

        qDebug() << "Saljem end game";
        QTimer::singleShot(4000, this, &SkockoUi::nextGame);
    } else {
        m_skockoController->setCurrentRow(m_skockoController->getCurrentRow()+1);

        if (m_skockoController->getCurrentRow() == 7){
            m_turn = !m_turn;

            ui->pbAddAceOfSpades->setEnabled(m_turn);
            ui->pbAddClubs->setEnabled(m_turn);
            ui->pbAddDiamonds->setEnabled(m_turn);
            ui->pbAddHearts->setEnabled(m_turn);
            ui->pbAddOwls->setEnabled(m_turn);
            ui->pbAddStars->setEnabled(m_turn);


            m_timer->stop();
            m_extraTimer->start(1000);

            m_time = 10;
            ui->leTimer->setText(QString::number(m_time));
        }

    }

    for(int i=0; i<4;i++){
        m_skockoController->m_currentCombination[i] = Symbols::Prazno;
    }

    int i = 0;
    for (auto lb : labelRow)
    {
        if (status[i] == Status::Crveno){
            lb->setStyleSheet("background-color: red");
        } else if (status[i] == Status::Zuto){
            lb->setStyleSheet("background-color: yellow");
        } else {
            lb->setStyleSheet("background-color: rgb(169, 216, 255)");
        }
        i++;
    }

    for (i=0;i<4;i++)
    {
        if (data[1][i] == QString("s")[0]){
            setIconOnLabel(Symbols::Srce, row, i);
        }
        else if (data[1][i] == QString("S")[0]){
            setIconOnLabel(Symbols::Skocko, row, i);

        }
        else if (data[1][i] == QString("t")[0]){
            setIconOnLabel(Symbols::Tref,row, i);
        }
        else if (data[1][i] == QString("k")[0]){
            setIconOnLabel(Symbols::Karo, row, i);
        }
        else if (data[1][i] == QString("z")[0]){
            setIconOnLabel(Symbols::Zvezda, row, i);
        }
        else if (data[1][i] == QString("p")[0]){
            setIconOnLabel(Symbols::Pik, row, i);
        }
        else {
            qDebug()<<"WROND SYMBOL";
        }


    }


}

void SkockoUi::on_timesUp()
{
    m_skockoController->setCurrentRow(6);
    if(m_turn){
        std::fill(m_skockoController->m_currentCombination.begin(), m_skockoController->m_currentCombination.end(), Symbols::Prazno);
        m_skockoController->guess(m_skockoController->m_currentCombination, m_server, m_playerId,m_skockoNum, 6);
    }
}

void SkockoUi::updateTime()
{
    if (m_time >= 0){
        ui->leTimer->setText(QString::number(m_time));
    }

    if(m_time--==0){
        for (int i = 1; i <= 7; i++){
            customSetButtonEnabled(i, false);
        }

        ui->pbAddAceOfSpades->setEnabled(false);
        ui->pbAddClubs->setEnabled(false);
        ui->pbAddDiamonds->setEnabled(false);
        ui->pbAddHearts->setEnabled(false);
        ui->pbAddOwls->setEnabled(false);
        ui->pbAddStars->setEnabled(false);
        emit timesUp();
    }


}

void SkockoUi::updateExtraTime()
{
    if (m_time >= 0){
        ui->leTimer->setText(QString::number(m_time));
    }

    if(m_time--==0){

        for (int i = 1; i <= 7; i++){
            customSetButtonEnabled(i, false);
        }

        ui->pbAddAceOfSpades->setEnabled(false);
        ui->pbAddClubs->setEnabled(false);
        ui->pbAddDiamonds->setEnabled(false);
        ui->pbAddHearts->setEnabled(false);
        ui->pbAddOwls->setEnabled(false);
        ui->pbAddStars->setEnabled(false);
        emit extraTimesUp();
    }
}

void SkockoUi::on_extraTimesUp()
{
    m_skockoController->setCurrentRow(7);
    if(m_turn){
        std::fill(m_skockoController->m_currentCombination.begin(), m_skockoController->m_currentCombination.end(), Symbols::Prazno);
        m_skockoController->guess(m_skockoController->m_currentCombination, m_server, m_playerId,m_skockoNum, 7);
    }

}

QVector<Status> SkockoUi::attemptStringToStatus(QString attemptString)
{
    //prebacuje string odgovor servera u vektor statusa

    QVector<Status> attemptStatus;

    int redStatusCount = attemptString[0].digitValue();
    int yellowStatusCount = attemptString[1].digitValue();


    for (int i = 0; i < redStatusCount; i++)
        attemptStatus.append(Status::Crveno);
    for (int i = 0; i < yellowStatusCount; i++)
        attemptStatus.append(Status::Zuto);
    for (int i = 0; i < 4 - (redStatusCount + yellowStatusCount); i++)
        attemptStatus.append(Status::Transparentno);


    return attemptStatus;

}


void SkockoUi::on_btPotvrdi_1()
{
    m_skockoController->guess(m_skockoController->m_currentCombination, m_server, m_playerId,m_skockoNum,1);
}

void SkockoUi::on_btPotvrdi_2()
{
    m_skockoController->guess(m_skockoController->m_currentCombination, m_server, m_playerId,m_skockoNum,2);

}

void SkockoUi::on_btPotvrdi_3()
{

   m_skockoController->guess(m_skockoController->m_currentCombination, m_server, m_playerId,m_skockoNum,3);

}

void SkockoUi::on_btPotvrdi_4()
{

    m_skockoController->guess(m_skockoController->m_currentCombination, m_server, m_playerId,m_skockoNum,4);

}

void SkockoUi::on_btPotvrdi_5()
{

    m_skockoController->guess(m_skockoController->m_currentCombination, m_server, m_playerId,m_skockoNum,5);

}

void SkockoUi::on_btPotvrdi_6()
{

    m_skockoController->guess(m_skockoController->m_currentCombination, m_server, m_playerId,m_skockoNum,6);

}

void SkockoUi::on_btPotvrdiExtra()
{
   m_skockoController->guess(m_skockoController->m_currentCombination, m_server, m_playerId, m_skockoNum, 7);
}

void SkockoUi::customSetButtonEnabled(int index, bool enabled)
{
    const auto backgroundColor = enabled ? "background-color: #90EE90" : "background-color: #cccccc";
    switch (index){
        case 1:{
            ui->pbConfirmRow1->setStyleSheet(backgroundColor);
            ui->pbConfirmRow1->setEnabled(enabled);
            break;
        }
        case 2:{
            ui->pbConfirmRow2->setStyleSheet(backgroundColor);
            ui->pbConfirmRow2->setEnabled(enabled);
            break;
        }
        case 3:{
            ui->pbConfirmRow3->setStyleSheet(backgroundColor);
            ui->pbConfirmRow3->setEnabled(enabled);
            break;
        }
        case 4:{
            ui->pbConfirmRow4->setStyleSheet(backgroundColor);
            ui->pbConfirmRow4->setEnabled(enabled);
            break;
        }
        case 5:{
            ui->pbConfirmRow5->setStyleSheet(backgroundColor);
            ui->pbConfirmRow5->setEnabled(enabled);
            break;
        }
        case 6:{
            ui->pbConfirmRow6->setStyleSheet(backgroundColor);
            ui->pbConfirmRow6->setEnabled(enabled);
            break;
        }
        case 7:{
            ui->pbConfirmExtra->setStyleSheet(backgroundColor);
            ui->pbConfirmExtra->setEnabled(enabled);
            break;
        }
    }
}

void SkockoUi::on_btSkocko()
{

    int index= m_skockoController->findFirstEmpty();
    if(index>=0){
      setIconOnLabel(Symbols::Skocko, m_skockoController->getCurrentRow(), index);
      m_skockoController->m_currentCombination[index]=Symbols::Skocko;
    }

    if(m_skockoController->isCombinationCompleted()){
        customSetButtonEnabled(m_skockoController->getCurrentRow(), true);
    }

}

void SkockoUi::on_btTref()
{
    int index= m_skockoController->findFirstEmpty();
    if(index>=0){
      setIconOnLabel(Symbols::Tref, m_skockoController->getCurrentRow(), index);
      m_skockoController->m_currentCombination[index]=Symbols::Tref;
    }

    if(m_skockoController->isCombinationCompleted()){
        customSetButtonEnabled(m_skockoController->getCurrentRow(), true);
    }

}

void SkockoUi::on_btPik()
{
    int index= m_skockoController->findFirstEmpty();
    if(index>=0){
      setIconOnLabel(Symbols::Pik, m_skockoController->getCurrentRow(), index);
      m_skockoController->m_currentCombination[index]=Symbols::Pik;
    }

    if(m_skockoController->isCombinationCompleted()){
        customSetButtonEnabled(m_skockoController->getCurrentRow(), true);
    }

}

void SkockoUi::on_btSrce()
{
    int index= m_skockoController->findFirstEmpty();
    if(index>=0){
      setIconOnLabel(Symbols::Srce, m_skockoController->getCurrentRow(), index);
      m_skockoController->m_currentCombination[index]=Symbols::Srce;
    }

    if(m_skockoController->isCombinationCompleted()){
        customSetButtonEnabled(m_skockoController->getCurrentRow(), true);
    }

}

void SkockoUi::on_btKaro()
{

    int index= m_skockoController->findFirstEmpty();
    if(index>=0){
      setIconOnLabel(Symbols::Karo, m_skockoController->getCurrentRow(), index);
      m_skockoController->m_currentCombination[index]=Symbols::Karo;
    }

    if(m_skockoController->isCombinationCompleted()){
        customSetButtonEnabled(m_skockoController->getCurrentRow(), true);
    }
}

void SkockoUi::on_btZvezda()
{
    int index= m_skockoController->findFirstEmpty();
    if(index>=0){
      setIconOnLabel(Symbols::Zvezda, m_skockoController->getCurrentRow(), index);
      m_skockoController->m_currentCombination[index]=Symbols::Zvezda;
    }

    if(m_skockoController->isCombinationCompleted()){
        customSetButtonEnabled(m_skockoController->getCurrentRow(), true);
    }
}


// label clicked actions

// row 1
void SkockoUi::on_lbRow1Column1Clicked()
{
    qDebug() << "clicked!";
    m_skockoController->resetSymbolCombination(1);
    customSetButtonEnabled(m_skockoController->getCurrentRow(), false);

    if (m_skockoController->getCurrentRow() == 1){
        ui->lbRow1Column1->clear();
    }

}

void SkockoUi::on_lbRow1Column2Clicked()
{
    if (m_skockoController->getCurrentRow() == 1){
        m_skockoController->resetSymbolCombination(2);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow1Column2->clear();
    }

}

void SkockoUi::on_lbRow1Column3Clicked()
{

    if (m_skockoController->getCurrentRow() == 1){
        m_skockoController->resetSymbolCombination(3);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow1Column3->clear();
    }

}

void SkockoUi::on_lbRow1Column4Clicked()
{

    if (m_skockoController->getCurrentRow() == 1){
        m_skockoController->resetSymbolCombination(4);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow1Column4->clear();
    }

}

// row 2
void SkockoUi::on_lbRow2Column1Clicked()
{

    if (m_skockoController->getCurrentRow() == 2){
        m_skockoController->resetSymbolCombination(1);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow2Column1->clear();
    }

}
void SkockoUi::on_lbRow2Column2Clicked()
{

    if (m_skockoController->getCurrentRow() == 2){
       m_skockoController->resetSymbolCombination(2);
       customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
       ui->lbRow2Column2->clear();
    }

}
void SkockoUi::on_lbRow2Column3Clicked()
{

    if (m_skockoController->getCurrentRow() == 2){
      m_skockoController->resetSymbolCombination(3);
      customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
      ui->lbRow2Column3->clear();
    }

}
void SkockoUi::on_lbRow2Column4Clicked()
{


    if (m_skockoController->getCurrentRow() == 2){
      m_skockoController->resetSymbolCombination(4);
      customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
      ui->lbRow2Column4->clear();
    }

}

// row 3
void SkockoUi::on_lbRow3Column1Clicked()
{

    if (m_skockoController->getCurrentRow() == 3){
        m_skockoController->resetSymbolCombination(1);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow3Column1->clear();
    }

}
void SkockoUi::on_lbRow3Column2Clicked()
{

    if (m_skockoController->getCurrentRow() == 3){
        m_skockoController->resetSymbolCombination(2);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow3Column2->clear();
    }

}
void SkockoUi::on_lbRow3Column3Clicked()
{

    if (m_skockoController->getCurrentRow() == 3){
        m_skockoController->resetSymbolCombination(3);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow3Column3->clear();
    }
}
void SkockoUi::on_lbRow3Column4Clicked()
{

    if (m_skockoController->getCurrentRow() == 3){
        m_skockoController->resetSymbolCombination(4);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow3Column4->clear();
    }
}
// row 4

void SkockoUi::on_lbRow4Column1Clicked()
{

    if (m_skockoController->getCurrentRow() == 4){
        m_skockoController->resetSymbolCombination(1);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow4Column1->clear();
    }

}
void SkockoUi::on_lbRow4Column2Clicked()
{

    if (m_skockoController->getCurrentRow() == 4){
       m_skockoController->resetSymbolCombination(2);
       customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
       ui->lbRow4Column2->clear();
    }

}
void SkockoUi::on_lbRow4Column3Clicked()
{

    if (m_skockoController->getCurrentRow() == 4){
        m_skockoController->resetSymbolCombination(3);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow4Column3->clear();
    }
}
void SkockoUi::on_lbRow4Column4Clicked()
{

    if (m_skockoController->getCurrentRow() == 4){
        m_skockoController->resetSymbolCombination(4);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow4Column4->clear();
    }
}

// row 5

void SkockoUi::on_lbRow5Column1Clicked()
{

    if (m_skockoController->getCurrentRow() == 5){
        m_skockoController->resetSymbolCombination(1);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow5Column1->clear();
    }

}
void SkockoUi::on_lbRow5Column2Clicked()
{

    if (m_skockoController->getCurrentRow() == 5){
       m_skockoController->resetSymbolCombination(2);
       customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
       ui->lbRow5Column2->clear();
    }

}
void SkockoUi::on_lbRow5Column3Clicked()
{

    if (m_skockoController->getCurrentRow() == 5){
        m_skockoController->resetSymbolCombination(3);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow5Column3->clear();
    }
}
void SkockoUi::on_lbRow5Column4Clicked()
{

    if (m_skockoController->getCurrentRow() == 5){
        m_skockoController->resetSymbolCombination(4);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow5Column4->clear();
    }
}

// row 6

void SkockoUi::on_lbRow6Column1Clicked()
{

    if (m_skockoController->getCurrentRow() == 6){
        m_skockoController->resetSymbolCombination(1);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow6Column1->clear();
    }

}
void SkockoUi::on_lbRow6Column2Clicked()
{

    if (m_skockoController->getCurrentRow() == 6){
        m_skockoController->resetSymbolCombination(2);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow6Column2->clear();
    }

}
void SkockoUi::on_lbRow6Column3Clicked()
{

    if (m_skockoController->getCurrentRow() == 6){
        m_skockoController->resetSymbolCombination(3);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow6Column3->clear();
    }
}
void SkockoUi::on_lbRow6Column4Clicked()
{

    if (m_skockoController->getCurrentRow() == 6){
        m_skockoController->resetSymbolCombination(4);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbRow6Column4->clear();
    }
}

// extra
void SkockoUi::on_lbRowExtraColumn1Clicked()
{
    if (m_skockoController->getCurrentRow() == 7){
        m_skockoController->resetSymbolCombination(1);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbExtraColumn1->clear();
    }
}

void SkockoUi::on_lbRowExtraColumn2Clicked()
{
    if (m_skockoController->getCurrentRow() == 7){
        m_skockoController->resetSymbolCombination(2);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbExtraColumn2->clear();
    }
}

void SkockoUi::on_lbRowExtraColumn3Clicked()
{
    if (m_skockoController->getCurrentRow() == 7){
        m_skockoController->resetSymbolCombination(3);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbExtraColumn3->clear();
    }
}

void SkockoUi::on_lbRowExtraColumn4Clicked()
{
    if (m_skockoController->getCurrentRow() == 7){
        m_skockoController->resetSymbolCombination(4);
        customSetButtonEnabled(m_skockoController->getCurrentRow(), false);
        ui->lbExtraColumn4->clear();
    }
}

// end label actions

void SkockoUi::setIconOnLabel(Symbols symbol, int row, int column)
{

    std::string path = getIconResourcePath(symbol);

    const char* str = path.c_str();
    QPixmap icon(str);

    int width = ui->lbRow1Column1->width() - 5;
    int heigth = ui->lbRow1Column1->height() - 5;

    if (row == 1){
        switch (column) {
        case 0:
            ui->lbRow1Column1->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 1:
            ui->lbRow1Column2->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 2:
            ui->lbRow1Column3->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 3:
            ui->lbRow1Column4->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        default:
            break;
        }
    }
    if (row == 2){
        switch (column) {
        case 0:
            ui->lbRow2Column1->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 1:
            ui->lbRow2Column2->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 2:
            ui->lbRow2Column3->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 3:
            ui->lbRow2Column4->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        default:
            break;
        }
    }
    if (row == 3){
        switch (column) {
        case 0:
            ui->lbRow3Column1->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 1:
            ui->lbRow3Column2->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 2:
            ui->lbRow3Column3->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 3:
            ui->lbRow3Column4->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        default:
            break;
        }
    }
    if (row == 4){
        switch (column) {
        case 0:
            ui->lbRow4Column1->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 1:
            ui->lbRow4Column2->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 2:
            ui->lbRow4Column3->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 3:
            ui->lbRow4Column4->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        default:
            break;
        }
    }
    if (row == 5){
        switch (column) {
        case 0:
            ui->lbRow5Column1->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 1:
            ui->lbRow5Column2->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 2:
            ui->lbRow5Column3->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 3:
            ui->lbRow5Column4->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        default:
            break;
        }
    }
    if (row == 6){
        switch (column) {
        case 0:
            ui->lbRow6Column1->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 1:
            ui->lbRow6Column2->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 2:
            ui->lbRow6Column3->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        case 3:
            ui->lbRow6Column4->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
            break;
        default:
            break;
        }
    }
    if (row == 7){
        switch (column) {
            case 0:
                ui->lbExtraColumn1->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
                break;
            case 1:
                ui->lbExtraColumn2->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
                break;
            case 2:
                ui->lbExtraColumn3->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
                break;
            case 3:
                ui->lbExtraColumn4->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
                break;
            default:
                break;
            }
    }
    if (row == 8){
        switch (column) {
            case 0:
                ui->lbResultSymbolsColumn1->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
                break;
            case 1:
                ui->lbResultSymbolsColumn2->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
                break;
            case 2:
                ui->lbResultSymbolsColumn3->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
                break;
            case 3:
                ui->lbResultSymbolsColumn4->setPixmap(icon.scaled(width, heigth, Qt::KeepAspectRatio));
                break;
            default:
                break;
            }
    }
}

std::string SkockoUi::getIconResourcePath(Symbols symbol)
{
    std::string base = ":/icons/resources/";
    switch (symbol) {
    case Symbols::Karo:
        return base+="diamonds.png";
        break;
    case Symbols::Pik:
        return base+="ace-of-spades.png";
        break;
    case Symbols::Skocko:
        return base+="owl.png";
        break;
    case Symbols::Srce:
        return base+="hearts.png";
        break;
    case Symbols::Tref:
        return base+="clubs.png";
        break;
    case Symbols::Zvezda:
        return base+="star.png";
        break;
    default:
        qDebug() << "skockoui.cpp 1075 bad resource";
        exit(EXIT_FAILURE);
        break;
    }
}

void SkockoUi::nextGame()
{
    disconnect(m_server, SIGNAL(readyRead()), this, SLOT(readyRead()));
    emit endGame();
}
