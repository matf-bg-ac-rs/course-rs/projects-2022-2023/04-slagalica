#ifndef ASOCIJACIJEUI_H
#define ASOCIJACIJEUI_H


#include <QWidget>
#include <QTcpSocket>
#include <QPushButton>
#include <QLineEdit>
#include <QTimer>

#include "asocijacije.h"

namespace Ui {
class AsocijacijeUi;
}

class AsocijacijeUi : public QWidget
{
    Q_OBJECT

public:
    explicit AsocijacijeUi(QWidget *parent = nullptr,QTcpSocket* server = nullptr, int playerId = -1, QString clientName1={} ,QString clientName2={}, int associationNumber = -1,int playerPoints1 = 0, int playerPoints2 = 0);
    ~AsocijacijeUi();

    int associationNumber() const;

    void posalji_Prazno();

    Ui::AsocijacijeUi *getUi() const;

    void paintWidget(QWidget *btn, int id,bool turn);
    int time() const;
    void setTime(int newTime);



     int brPoteza() const;
     void setBrPoteza(int newBrPoteza);

     bool turn() const;
     void setTurn(bool newTurn);

     void changeTurn();
     void setColorYellow(QWidget *w);

     int id() const;
     void setId(int newId);

     int buttonsRemaining() const;
     void setButtonsRemaining(int newButtonsRemaining);

     const QString &clientName1() const;
     void setClientName1(const QString &newClientName1);

     const QString &clientName2() const;
     void setClientName2(const QString &newClientName2);

     void showAttempt(QStringList data);

     void sendMessage(QTcpSocket* socket,QString msg);

     void updatePoints(QStringList data);

     void disableButtonForever(QPushButton* btn);

     void disableLineEditForever(QLineEdit* line);

     bool availableGuesses();

     void enableAllRemainingLineEdits();

     void setAllFields(QStringList data);

     void setAllFields(QStringList data, bool paint);





     QMap<QPushButton *, int> &enableButtonsVector();
     void setEnableButtonsVector(const QMap<QPushButton *, int> &newEnableButtonsVector);

     QMap<QLineEdit *, int> &enableLineEditVector();
     void setEnableLineEditVector(const QMap<QLineEdit *, int> &newEnableLineEditVector);

signals:

    void timesUp();
    void endGame();
    void switch_turn();

public slots:
void updateTime();
void on_pb_A1_clicked();

private slots:


    void on_pb_A2_clicked();

    void on_pb_A3_clicked();

    void on_pb_A4_clicked();

    void on_pb_C1_clicked();

    void on_pb_C2_clicked();

    void on_pb_C3_clicked();

    void on_pb_C4_clicked();

    void on_pb_B1_clicked();

    void on_pb_B2_clicked();

    void on_pb_B3_clicked();

    void on_pb_B4_clicked();

    void on_pb_D1_clicked();

    void on_pb_D2_clicked();

    void on_pb_D3_clicked();

    void on_pb_D4_clicked();

    void on_le_A_returnPressed();

    void on_le_B_returnPressed();

    void on_le_C_returnPressed();

    void on_le_D_returnPressed();

    void on_le_KONACNO_returnPressed();

    void readyRead();



    void on_switchTurn();


private:
    Ui::AsocijacijeUi *ui;

  
    Asocijacije* m_asocijacije;


    QTcpSocket* m_server;
    int m_id;
    int m_associationNumber;
    bool m_turn;

    int m_buttonsRemaining;

    int m_time;
    QTimer *m_timer;

    int m_brPoteza;

    QString m_clientName1;
    QString m_clientName2;


    QMap<QPushButton*, int> m_enableButtonsVector;
    QMap<QLineEdit*, int> m_enableLineEditVector;

    void nextGame();

    void displayField(QStringList data);


    void requestSolution();

    void clearRemainingLineEdits();
    void enableAllRemainingButtons();

    void disableOpeningFieldsTemp();
    void disableAllTemp();
    void disableKonacno();
    void disableA();
    void disableB();
    void disableC();
    void disableD();

    void addButtonsToMap();


};

#endif // ASOCIJACIJEUI_H
