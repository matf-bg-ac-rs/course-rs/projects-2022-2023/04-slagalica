#ifndef PLAYER_H
#define PLAYER_H

#include <QString>

class Player
{
public:
    Player() = default;
    ~Player() = default;

    int m_id;
    QString m_name;
    int m_score;

    int getScore();

};

#endif // PLAYER_H
