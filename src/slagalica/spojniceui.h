#ifndef SPOJNICEUI_H
#define SPOJNICEUI_H

#include "spojnice.h"

#include <QWidget>
#include <QTimer>
#include <QMap>
#include <QPushButton>
#include <QLabel>


namespace Ui {
class SpojniceUi;
}

class SpojniceUi : public QWidget
{
    Q_OBJECT

public:

    explicit SpojniceUi(QWidget *parent = nullptr, QTcpSocket* server = nullptr, int playerId = -1,QString clientName1={}, QString clientName2={}, int spojniceNum=-1, int playerPoints1 = 0, int playerPoints2 = 0);
    ~SpojniceUi();

    Spojnice* m_spojniceController;

    Ui::SpojniceUi* getUi();
    int spojniceNumber();

    bool turn() const;
    void setTurn(bool newTurn);

    int time() const;
    void setTime(int newTime);

    int id() const;
    void setId(int newId);

    int playerPoints1() const;
    void setPlayerPoints1(int newPlayerPoints1);

    int playerPoints2() const;
    void setPlayerPoints2(int newPlayerPoints2);
    void showFields(QStringList data);
    void addButtonsAndLabelsToMaps();
    void enableAllRemainingButtons();

signals:
    void timesUp();

public slots:
    void on_timesUp();

    void updateTime();
    void changeTurn();

private:
    Ui::SpojniceUi *ui;

    void on_btRhs1();
    void on_btRhs2();
    void on_btRhs3();
    void on_btRhs4();
    void on_btRhs5();
    void on_btRhs6();
    void on_btRhs7();
    void on_btRhs8();

    void setUiEnabled(bool enabled);

    bool m_turn;
    int m_id;
    bool m_changedTurn;
    int m_spojniceNumber;
    int m_correctCounter;
    QVector<int> m_missedLabels;
    int m_missedLabelsCounter = 0;

    int m_playerPoints1;
    int m_playerPoints2;

    QString m_clientName1;
    QString m_clientName2;

    QVector<int> m_fieldColored;
    QStringList m_spojniceResults;


    QMap<QPushButton *, int> m_rightButtonsDisableFlags;
    QMap<QLabel*, int> m_leftLabelsDisableFlags;


    void initController();
    QString getCurrentRowValue(int currentRow) const;

    void markLabelAsFinished(int currentRow, bool markNextLabel, int playerGuessed, int rightFieldNumber);
    void markLabelAsMissed (int currentRow, bool markNextLabel);
    void markLabelAsNext(int currentRow);
    void removeButtonColor(int buttonIndex);

    QTcpSocket* m_server;


    int m_time;
    QTimer* m_timer;


    void updatePoints(QStringList data);
    void disableButton(int btnIndex);
    void disableLabel(int lblIndex);
    void disableAll();
    void nextGame();

    void getSpojnicaFields();
    int getNextNonGuessedLabel();
    void getResults();
    void showResults(QStringList data);

    void sendMessage(QTcpSocket* server, QString msg);

private slots:
    void readyRead();



signals:
    void endGame();
};

#endif // SPOJNICEUI_H
