#include "spojnice.h"
#include <QMap>

Spojnice::Spojnice()
{


}

Spojnice::~Spojnice()
{

}

void Spojnice::guess(int leftColumnNumber, int rightColumnNumber, int playerId, int spojniceNumber, QTcpSocket *server)
{
   QString msgString = QString::number(playerId)
             + "_a_S_"
             + QString::number(spojniceNumber)
             + "_"
             + QString::number(leftColumnNumber-1)
             + "_"
             + QString::number(rightColumnNumber-1);


    sendMessage(server, msgString);
}

void Spojnice::init()
{
    return;
}

int Spojnice::calculateScore()
{
    return 0;
}

int Spojnice::calcluateScore(bool correct) const
{
    return correct ? 2 : 0;
}

int Spojnice::getCurrentRow() const
{
    return m_currentRow;
}

void Spojnice::setCurrentRow(int row)
{
    m_currentRow = row;
}

void Spojnice::incrementCurrentRow()
{
    m_currentRow++;
}

void Spojnice::sendMessage(QTcpSocket *server, QString msg)
{
    qDebug() << msg;
    server->write(msg.toUtf8());
    server->flush();
}
