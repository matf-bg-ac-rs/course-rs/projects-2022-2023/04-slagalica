#include "matchready.h"
#include "ui_matchready.h"

MatchReady::MatchReady(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MatchReady)
{
    ui->setupUi(this);
}

MatchReady::~MatchReady()
{
    delete ui;
}

void MatchReady::on_pushButton_clicked()
{
    this->close();
}

