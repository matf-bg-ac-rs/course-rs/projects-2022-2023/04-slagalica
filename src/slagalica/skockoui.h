#ifndef SKOCKOUI_H
#define SKOCKOUI_H

#include <QWidget>
#include <QTcpSocket>
#include <QTimer>
#include "skocko.h"
namespace Ui {
class SkockoUi;
}

class SkockoUi : public QWidget
{
    Q_OBJECT

public:
    explicit SkockoUi(QWidget *parent = nullptr, QTcpSocket* server = nullptr, int playerId = -1,QString clientName1="",QString clientName2="", bool turn=false,int skockoNum=-1, int playerPoints1 = 0, int playerPoints2 = 0);
    ~SkockoUi();

    Ui::SkockoUi *getUi() const;
    bool m_turn;

    int m_skockoNum;
    int m_playerId;
    int m_time;
    QString m_clientName1;
    QString m_clientName2;
    QTimer* m_timer;
    QTimer* m_extraTimer;

    int time() const;
    void setTime(int newTime);

    int playerId() const;
    void setPlayerId(int newPlayerId);

signals:
    void endGame();
    void switchTurn();
    void timesUp();

    void extraTimesUp();

public slots :
    void readyRead();
    void on_timesUp();
    void updateTime();

    void updateExtraTime();
    void on_extraTimesUp();

private:
    Ui::SkockoUi *ui;
    Skocko* m_skockoController;
    QTcpSocket* m_server;

    QVector<Status> attemptStringToStatus(QString attemptString);

    void nextGame();

    void on_btPotvrdi_1();
    void on_btPotvrdi_2();
    void on_btPotvrdi_3();
    void on_btPotvrdi_4();
    void on_btPotvrdi_5();
    void on_btPotvrdi_6();
    void on_btPotvrdiExtra();

    void on_lbRow1Column1Clicked();
    void on_lbRow1Column2Clicked();
    void on_lbRow1Column3Clicked();
    void on_lbRow1Column4Clicked();

    void on_lbRow2Column1Clicked();
    void on_lbRow2Column2Clicked();
    void on_lbRow2Column3Clicked();
    void on_lbRow2Column4Clicked();


    void on_lbRow3Column1Clicked();
    void on_lbRow3Column2Clicked();
    void on_lbRow3Column3Clicked();
    void on_lbRow3Column4Clicked();


    void on_lbRow4Column1Clicked();
    void on_lbRow4Column2Clicked();
    void on_lbRow4Column3Clicked();
    void on_lbRow4Column4Clicked();


    void on_lbRow5Column1Clicked();
    void on_lbRow5Column2Clicked();
    void on_lbRow5Column3Clicked();
    void on_lbRow5Column4Clicked();


    void on_lbRow6Column1Clicked();
    void on_lbRow6Column2Clicked();
    void on_lbRow6Column3Clicked();
    void on_lbRow6Column4Clicked();

    void on_lbRowExtraColumn1Clicked();
    void on_lbRowExtraColumn2Clicked();
    void on_lbRowExtraColumn3Clicked();
    void on_lbRowExtraColumn4Clicked();

    void on_btSkocko();
    void on_btTref();
    void on_btPik();
    void on_btSrce();
    void on_btKaro();
    void on_btZvezda();



    void setIconOnLabel(Symbols symbol, int row, int column);
    std::string getIconResourcePath(Symbols symbol);

    void customSetButtonEnabled(int index, bool enabled);

    void initController();
};

#endif // SKOCKOUI_H
