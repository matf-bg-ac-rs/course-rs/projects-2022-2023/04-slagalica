#include "asocijacije.h"

void Asocijacije::guess(int associationNumber, Column m_column, QString m_resultColumn, int playerId, QTcpSocket* server)
{
    // poruka serveru -> playerId_a(attempt)_a(asocijacije)_redniBrojAsocijacije_i(nazivKolone)_answer(odgovora)

    QString msgString =  QString::number(playerId)
                       + "_a_a_"
                       + QString::number(associationNumber)
                       + "_" + columnToString(m_column)
                       + "_" + m_resultColumn;      

    qDebug() << "Sending guess...." << msgString;
    sendMessage(server, msgString);
}

void Asocijacije::init()
{

}

int Asocijacije::calculateScore()
{
    return 0;
}

void Asocijacije::sendMessage(QTcpSocket *server, QString msg)
{
    server->write(msg.toUtf8());
    server->flush();
}

QString Asocijacije::columnToString(Column m_column){

    QString attemptString = "";

        if (m_column == Column::A)
            attemptString += "A";
        else if (m_column == Column::B)
            attemptString += "B";
        else if (m_column == Column::C)
            attemptString += "C";
        else if (m_column == Column::D)
            attemptString += "D";
        else if (m_column == Column::Konacno)
            attemptString += "K";



    return attemptString;


}


