#include "spojniceui.h"
#include "qtimer.h"
#include "ui_spojniceui.h"
#include <QDebug>
#include <QMap>

SpojniceUi::SpojniceUi(QWidget *parent, QTcpSocket* server, int playerId, QString clientName1, QString clientName2, int spojniceNumber, int playerPoints1, int playerPoints2) :
    QWidget(parent),
    ui(new Ui::SpojniceUi),
    m_id(playerId),
    m_spojniceNumber(spojniceNumber),
    m_playerPoints1(playerPoints1),
    m_playerPoints2(playerPoints2),
    m_server(server)
{
    connect(m_server, SIGNAL(readyRead()), this, SLOT(readyRead()));

    m_changedTurn = false;
    m_correctCounter = 0;
    m_missedLabels = QVector<int>();
    m_missedLabelsCounter = 0;
    m_clientName1=clientName1;
    m_clientName2=clientName2;
    m_fieldColored = QVector<int>(8);

    ui->setupUi(this);
    connect(ui->pbRhs1, &QPushButton::clicked, this, &SpojniceUi::on_btRhs1);
    connect(ui->pbRhs2, &QPushButton::clicked, this, &SpojniceUi::on_btRhs2);
    connect(ui->pbRhs3, &QPushButton::clicked, this, &SpojniceUi::on_btRhs3);
    connect(ui->pbRhs4, &QPushButton::clicked, this, &SpojniceUi::on_btRhs4);
    connect(ui->pbRhs5, &QPushButton::clicked, this, &SpojniceUi::on_btRhs5);
    connect(ui->pbRhs6, &QPushButton::clicked, this, &SpojniceUi::on_btRhs6);
    connect(ui->pbRhs7, &QPushButton::clicked, this, &SpojniceUi::on_btRhs7);
    connect(ui->pbRhs8, &QPushButton::clicked, this, &SpojniceUi::on_btRhs8);

    addButtonsAndLabelsToMaps();
    ui->lcdPlayer1->display(m_playerPoints1);
    ui->lcdPlayer2->display(m_playerPoints2);

    if(m_id == spojniceNumber+1)
    {
        m_turn = true;
        getSpojnicaFields();
        enableAllRemainingButtons();
    }
    else
    {
        m_turn = false;
        disableAll();
    }

    initController();

    ui->lcdPlayer1->display(playerPoints1);
    ui->lcdPlayer2->display(playerPoints2);

    ui->lineEdit_2->setText(m_clientName1);
    ui->lineEdit_6->setText(m_clientName2);

    // timer configuration
    m_time = 10;
    m_timer = new QTimer(this);

    connect(m_timer,SIGNAL(timeout()),this,SLOT(updateTime()));
    connect(this,&SpojniceUi::timesUp,this,&SpojniceUi::on_timesUp);

    m_timer->start(1000);
    ui->leTimer->setText(QString::number(m_time));

}

SpojniceUi::~SpojniceUi()
{
    delete m_spojniceController;
    delete ui;
}

void SpojniceUi::on_timesUp()
{
    qDebug() << "Vreme je isteklo";
    if (m_turn)
        m_spojniceController->guess(m_spojniceController->getCurrentRow(), 0, m_id, m_spojniceNumber, m_server);
}

void SpojniceUi::updateTime()
{
    if (m_time >= 0){
        ui->leTimer->setText(QString::number(m_time));
    }

    if(m_time--==0){
        emit timesUp();
    }

}

void SpojniceUi::initController()
{
    m_spojniceController = new Spojnice();
}

QString SpojniceUi::getCurrentRowValue(int currentRow) const
{
    switch (currentRow){
        case (1): {
            return ui->lbLhs1->text();
        }
        case (2): {
            return ui->lbLhs2->text();
        }
        case (3): {
            return ui->lbLhs3->text();
        }
        case (4): {
            return ui->lbLhs4->text();
        }
        case (5): {
            return ui->lbLhs5->text();
        }
        case (6): {
            return ui->lbLhs6->text();
        }
        case (7): {
            return ui->lbLhs7->text();
        }
        case (8): {
            return ui->lbLhs8->text();
        }
        default: {
            qDebug() << "Greska";
            return "";
        }
    }
}

void SpojniceUi::markLabelAsFinished(int currentRow, bool markNextLabel, int playerGuessed, int rightFieldNumber)
{
    qDebug() << currentRow << rightFieldNumber;

    if (playerGuessed == 0)
        m_fieldColored[currentRow-1] = 1;
    else
        m_fieldColored[currentRow-1] = 2;

    const auto& currentLabelColor = QString("background-color: rgb(85, 122, 255);"
                                            "border: 1px solid black; "
                                           "color:black");

    const auto& finishedLabelColorBlue = QString("background-color: rgb(14, 65, 157);border: 1px solid black;color:yellow");
    const auto& finishedLabelColorRed = QString("background-color: rgb(193, 31, 53);border: 1px solid black;color:yellow");
    switch (currentRow){
        case (1): {
            if (playerGuessed == 0)
                ui->lbLhs1->setStyleSheet(finishedLabelColorBlue);
            else
                ui->lbLhs1->setStyleSheet(finishedLabelColorRed);
            if (markNextLabel)
                ui->lbLhs2->setStyleSheet(currentLabelColor);
            break;
        }
        case (2): {
            if (playerGuessed == 0)
                ui->lbLhs2->setStyleSheet(finishedLabelColorBlue);
            else
                ui->lbLhs2->setStyleSheet(finishedLabelColorRed);
            if (markNextLabel)
                ui->lbLhs3->setStyleSheet(currentLabelColor);
            break;
        }
        case (3): {
            if (playerGuessed == 0)
                ui->lbLhs3->setStyleSheet(finishedLabelColorBlue);
            else
                ui->lbLhs3->setStyleSheet(finishedLabelColorRed);
            if (markNextLabel)
                ui->lbLhs4->setStyleSheet(currentLabelColor);
            break;
        }
        case (4): {
            if (playerGuessed == 0)
                ui->lbLhs4->setStyleSheet(finishedLabelColorBlue);
            else
                ui->lbLhs4->setStyleSheet(finishedLabelColorRed);
            if (markNextLabel)
                ui->lbLhs5->setStyleSheet(currentLabelColor);
            break;
        }
        case (5): {
            if (playerGuessed == 0)
                ui->lbLhs5->setStyleSheet(finishedLabelColorBlue);
            else
                ui->lbLhs5->setStyleSheet(finishedLabelColorRed);
            if (markNextLabel)
                ui->lbLhs6->setStyleSheet(currentLabelColor);
            break;
        }
        case (6): {
            if (playerGuessed == 0)
                ui->lbLhs6->setStyleSheet(finishedLabelColorBlue);
            else
                ui->lbLhs6->setStyleSheet(finishedLabelColorRed);
            if (markNextLabel)
                ui->lbLhs7->setStyleSheet(currentLabelColor);
            break;
        }
        case (7): {
            if (playerGuessed == 0)
                ui->lbLhs7->setStyleSheet(finishedLabelColorBlue);
            else
                ui->lbLhs7->setStyleSheet(finishedLabelColorRed);
            if (markNextLabel)
                ui->lbLhs8->setStyleSheet(currentLabelColor);
            break;
        }
        case (8): {
            if (playerGuessed == 0)
                ui->lbLhs8->setStyleSheet(finishedLabelColorBlue);
            else
                ui->lbLhs8->setStyleSheet(finishedLabelColorRed);
            break;
        }
        default: {
            qDebug() << "Greska";
        }
    }

    switch (rightFieldNumber){
        case (1): {
            if (playerGuessed == 0)
                ui->pbRhs1->setStyleSheet(finishedLabelColorBlue);
            else
                ui->pbRhs1->setStyleSheet(finishedLabelColorRed);
            break;
        }
        case (2): {
            if (playerGuessed == 0)
                ui->pbRhs2->setStyleSheet(finishedLabelColorBlue);
            else
                ui->pbRhs2->setStyleSheet(finishedLabelColorRed);
            break;
        }
        case (3): {
            if (playerGuessed == 0)
                ui->pbRhs3->setStyleSheet(finishedLabelColorBlue);
            else
                ui->pbRhs3->setStyleSheet(finishedLabelColorRed);
            break;
        }
        case (4): {
            if (playerGuessed == 0)
                ui->pbRhs4->setStyleSheet(finishedLabelColorBlue);
            else
                ui->pbRhs4->setStyleSheet(finishedLabelColorRed);
            break;
        }
        case (5): {
            if (playerGuessed == 0)
                ui->pbRhs5->setStyleSheet(finishedLabelColorBlue);
            else
                ui->pbRhs5->setStyleSheet(finishedLabelColorRed);
            break;
        }
        case (6): {
            if (playerGuessed == 0)
                ui->pbRhs6->setStyleSheet(finishedLabelColorBlue);
            else
                ui->pbRhs6->setStyleSheet(finishedLabelColorRed);
            break;
        }
        case (7): {
            if (playerGuessed == 0)
                ui->pbRhs7->setStyleSheet(finishedLabelColorBlue);
            else
                ui->pbRhs7->setStyleSheet(finishedLabelColorRed);
            break;
        }
        case (8): {
            if (playerGuessed == 0)
                ui->pbRhs8->setStyleSheet(finishedLabelColorBlue);
            else
                ui->pbRhs8->setStyleSheet(finishedLabelColorRed);
            break;
        }
        default: {
            qDebug() << "Greska";
        }
    }
}

void SpojniceUi::markLabelAsMissed(int currentRow, bool markNextLabel)
{
    const auto& currentLabelColor = QString("background-color: rgb(85, 122, 255);"
                                            "border: 1px solid black; "
                                           "color:black");
    const auto& missedLabelColor = QString("background-color: rgb(111, 186, 255);"
                                           "border: 1px solid black; "
                                          "color:black");
    switch (currentRow){
        case (1): {
            ui->lbLhs1->setStyleSheet(missedLabelColor);
            if (markNextLabel)
                ui->lbLhs2->setStyleSheet(currentLabelColor);
            break;
        }
        case (2): {
            ui->lbLhs2->setStyleSheet(missedLabelColor);
            if (markNextLabel)
                ui->lbLhs3->setStyleSheet(currentLabelColor);
            break;
        }
        case (3): {
            ui->lbLhs3->setStyleSheet(missedLabelColor);
            if (markNextLabel)
                ui->lbLhs4->setStyleSheet(currentLabelColor);
            break;
        }
        case (4): {
            ui->lbLhs4->setStyleSheet(missedLabelColor);
            if (markNextLabel)
                ui->lbLhs5->setStyleSheet(currentLabelColor);
            break;
        }
        case (5): {
            ui->lbLhs5->setStyleSheet(missedLabelColor);
            if (markNextLabel)
                ui->lbLhs6->setStyleSheet(currentLabelColor);
            break;
        }
        case (6): {
            ui->lbLhs6->setStyleSheet(missedLabelColor);
            if (markNextLabel)
                ui->lbLhs7->setStyleSheet(currentLabelColor);
            break;
        }
        case (7): {
            ui->lbLhs7->setStyleSheet(missedLabelColor);
            if (markNextLabel)
                ui->lbLhs8->setStyleSheet(currentLabelColor);
            break;
        }
        case (8): {
            ui->lbLhs8->setStyleSheet(missedLabelColor);
            break;
        }
        default: {
            qDebug() << "Greska";
        }
    }
}

void SpojniceUi::markLabelAsNext(int currentRow)
{
    const auto& currentLabelColor = QString("background-color: rgb(85, 122, 255);"
                                            "border: 1px solid black; "
                                           "color:black");
    switch (currentRow){
        case (1): {
            ui->lbLhs1->setStyleSheet(currentLabelColor);
            break;
        }
        case (2): {
            ui->lbLhs2->setStyleSheet(currentLabelColor);
            break;
        }
        case (3): {
            ui->lbLhs3->setStyleSheet(currentLabelColor);
            break;
        }
        case (4): {
            ui->lbLhs4->setStyleSheet(currentLabelColor);
            break;
        }
        case (5): {
            ui->lbLhs5->setStyleSheet(currentLabelColor);
            break;
        }
        case (6): {
            ui->lbLhs6->setStyleSheet(currentLabelColor);
            break;
        }
        case (7): {
            ui->lbLhs7->setStyleSheet(currentLabelColor);
            break;
        }
        case (8): {
            ui->lbLhs8->setStyleSheet(currentLabelColor);
            break;
        }
        default: {
            qDebug() << "Greska";
        }
    }
}

void SpojniceUi::removeButtonColor(int buttonIndex)
{
    const auto& missedLabelColor = QString("background-color: rgb(111, 186, 255);"
                                           "border: 1px solid black; "
                                          "color:black");

    switch (buttonIndex){
        case (1): {
                ui->pbRhs1->setStyleSheet(missedLabelColor);
            break;
        }
        case (2): {
                ui->pbRhs2->setStyleSheet(missedLabelColor);
                break;
        }
        case (3): {
                ui->pbRhs3->setStyleSheet(missedLabelColor);
                break;
        }
        case (4): {
            ui->pbRhs4->setStyleSheet(missedLabelColor);
            break;
        }
        case (5): {
            ui->pbRhs5->setStyleSheet(missedLabelColor);
            break;
        }
        case (6): {
            ui->pbRhs6->setStyleSheet(missedLabelColor);
            break;
        }
        case (7): {
            ui->pbRhs7->setStyleSheet(missedLabelColor);
            break;
        }
        case (8): {
            ui->pbRhs8->setStyleSheet(missedLabelColor);
            break;
        }
        default: {
            qDebug() << "Greska spojniceui 434";
            break;
        }
    }
}

int SpojniceUi::time() const
{
    return m_time;
}

void SpojniceUi::setTime(int newTime)
{
    m_time = newTime;
}


void SpojniceUi::on_btRhs1()
{
    disableAll();
    m_spojniceController->guess(m_spojniceController->getCurrentRow(), 1, m_id, m_spojniceNumber, m_server);
}

void SpojniceUi::on_btRhs2()
{
    disableAll();
    m_spojniceController->guess(m_spojniceController->getCurrentRow(), 2, m_id, m_spojniceNumber, m_server);
}

void SpojniceUi::on_btRhs3()
{
    disableAll();
    m_spojniceController->guess(m_spojniceController->getCurrentRow(), 3, m_id, m_spojniceNumber, m_server);
}

void SpojniceUi::on_btRhs4()
{
    disableAll();
    m_spojniceController->guess(m_spojniceController->getCurrentRow(), 4, m_id, m_spojniceNumber, m_server);
}

void SpojniceUi::on_btRhs5()
{
    disableAll();
    m_spojniceController->guess(m_spojniceController->getCurrentRow(), 5, m_id, m_spojniceNumber, m_server);
}

void SpojniceUi::on_btRhs6()
{
    disableAll();
    m_spojniceController->guess(m_spojniceController->getCurrentRow(), 6, m_id, m_spojniceNumber, m_server);
}

void SpojniceUi::on_btRhs7()
{
    disableAll();
    m_spojniceController->guess(m_spojniceController->getCurrentRow(), 7, m_id, m_spojniceNumber, m_server);
}

void SpojniceUi::on_btRhs8()
{
    disableAll();
    m_spojniceController->guess(m_spojniceController->getCurrentRow(), 8, m_id, m_spojniceNumber, m_server);
}

int SpojniceUi::playerPoints2() const
{
    return m_playerPoints2;
}

void SpojniceUi::setPlayerPoints2(int newPlayerPoints2)
{
    m_playerPoints2 = newPlayerPoints2;
}

int SpojniceUi::playerPoints1() const
{
    return m_playerPoints1;
}

void SpojniceUi::setPlayerPoints1(int newPlayerPoints1)
{
    m_playerPoints1 = newPlayerPoints1;
}

int SpojniceUi::id() const
{
    return m_id;
}

void SpojniceUi::setId(int newId)
{
    m_id = newId;
}

bool SpojniceUi::turn() const
{
    return m_turn;
}

void SpojniceUi::setTurn(bool newTurn)
{
    m_turn = newTurn;
}

void SpojniceUi::readyRead()
{
    QString serverMsg = m_server->readAll();
    QStringList data = serverMsg.split('_');
    qDebug() << "Rec data" << data;

    if (data[0] == 'g')
        showFields(data);
    else if (data[0] == 's')
    {
        qDebug() << data;
        showResults(data);
    }
    else
        updatePoints(data);
}

void SpojniceUi::showFields(QStringList data)
{
    ui->lbLhs1->setText(data[1]);
    ui->lbLhs2->setText(data[2]);
    ui->lbLhs3->setText(data[3]);
    ui->lbLhs4->setText(data[4]);
    ui->lbLhs5->setText(data[5]);
    ui->lbLhs6->setText(data[6]);
    ui->lbLhs7->setText(data[7]);
    ui->lbLhs8->setText(data[8]);

    ui->pbRhs1->setText(data[9]);
    ui->pbRhs2->setText(data[10]);
    ui->pbRhs3->setText(data[11]);
    ui->pbRhs4->setText(data[12]);
    ui->pbRhs5->setText(data[13]);
    ui->pbRhs6->setText(data[14]);
    ui->pbRhs7->setText(data[15]);
    ui->pbRhs8->setText(data[16]);
}

void SpojniceUi::updatePoints(QStringList data)
{
    m_time = 10;

    int leftFieldNumber = data[1].toInt();
    int rightFieldNumber = data[2].toInt();

    int currentRow = m_spojniceController->getCurrentRow();
    int nextNonGuessedLabel = -1;
    if (!m_changedTurn)
        m_spojniceController->incrementCurrentRow();
    else
    {
        nextNonGuessedLabel = getNextNonGuessedLabel();
        disableLabel(leftFieldNumber);
        if (nextNonGuessedLabel == -1)
        {
            if (data[3] == "c")
            {
                int points1 = data[4].toInt();
                int points2 = data[5].toInt();

                ui->lcdPlayer1->display(ui->lcdPlayer1->intValue() + points1);
                ui->lcdPlayer2->display(ui->lcdPlayer2->intValue() + points2);

                int playerGuessed = 0;
                if (points2 == 2)
                    playerGuessed = 1;
                markLabelAsFinished(currentRow, !m_changedTurn, playerGuessed, rightFieldNumber+1);
            }
            else
                markLabelAsMissed(currentRow, !m_changedTurn);

            m_timer->stop();
            QTimer::singleShot(2000, this, &SpojniceUi::getResults);
            return;
        }
        else
            m_spojniceController->setCurrentRow(nextNonGuessedLabel);
    }

    if (data[3] == "c")
    {
        m_correctCounter++;

        int points1 = data[4].toInt();
        int points2 = data[5].toInt();

        ui->lcdPlayer1->display(ui->lcdPlayer1->intValue() + points1);
        ui->lcdPlayer2->display(ui->lcdPlayer2->intValue() + points2);

        int playerGuessed = 0;
        if (points2 == 2)
            playerGuessed = 1;
        markLabelAsFinished(currentRow, !m_changedTurn, playerGuessed, rightFieldNumber+1);
        if (m_changedTurn)
            markLabelAsNext(nextNonGuessedLabel);

        disableButton(rightFieldNumber);
    }
    else
    {
        if (!m_changedTurn)
            m_missedLabels.push_back(leftFieldNumber);
        else
            markLabelAsNext(nextNonGuessedLabel);
        markLabelAsMissed(currentRow, !m_changedTurn);
    }

    if (leftFieldNumber == 7)
    {
        if (m_correctCounter == 8)
        {
            m_timer->stop();
            QTimer::singleShot(2000, this, &SpojniceUi::getResults);
            return;
        }
        else if (!m_changedTurn)
        {
            m_turn = !m_turn;
            changeTurn();
        }
        else
        {
            m_timer->stop();
            QTimer::singleShot(2000, this, &SpojniceUi::getResults);
            return;
        }
    }
    else if (m_turn)
        enableAllRemainingButtons();
}

void SpojniceUi::disableButton(int btnIndex)
{
    QPushButton* btn = nullptr;
    switch (btnIndex) {
    case 0:
        btn = ui->pbRhs1;
        break;
    case 1:
        btn = ui->pbRhs2;
        break;
    case 2:
        btn = ui->pbRhs3;
        break;
    case 3:
        btn = ui->pbRhs4;
        break;
    case 4:
        btn = ui->pbRhs5;
        break;
    case 5:
        btn = ui->pbRhs6;
        break;
    case 6:
        btn = ui->pbRhs7;
        break;
    case 7:
        btn = ui->pbRhs8;
        break;
    default:
        break;
    }

    btn->setDisabled(true);
    m_rightButtonsDisableFlags[btn] = 0;
}

void SpojniceUi::disableLabel(int lblIndex)
{
    QLabel* lbl = nullptr;
    switch (lblIndex) {
    case 0:
        lbl = ui->lbLhs1;
        break;
    case 1:
        lbl = ui->lbLhs2;
        break;
    case 2:
        lbl = ui->lbLhs3;
        break;
    case 3:
        lbl = ui->lbLhs4;
        break;
    case 4:
        lbl = ui->lbLhs5;
        break;
    case 5:
        lbl = ui->lbLhs6;
        break;
    case 6:
        lbl = ui->lbLhs7;
        break;
    case 7:
        lbl = ui->lbLhs8;
        break;
    default:
        break;
    }

    m_leftLabelsDisableFlags[lbl] = 0;
}

void SpojniceUi::disableAll()
{
    for (QPushButton* btn: m_rightButtonsDisableFlags.keys())
        btn->setDisabled(true);
}

void SpojniceUi::nextGame()
{
    disconnect(m_server, SIGNAL(readyRead()), this, SLOT(readyRead()));
    emit endGame();
}

void SpojniceUi::addButtonsAndLabelsToMaps()
{
    m_rightButtonsDisableFlags = QMap<QPushButton*, int>();
    m_leftLabelsDisableFlags = QMap<QLabel*, int>();

    m_rightButtonsDisableFlags[ui->pbRhs1] = 1;
    m_rightButtonsDisableFlags[ui->pbRhs2] = 1;
    m_rightButtonsDisableFlags[ui->pbRhs3] = 1;
    m_rightButtonsDisableFlags[ui->pbRhs4] = 1;
    m_rightButtonsDisableFlags[ui->pbRhs5] = 1;
    m_rightButtonsDisableFlags[ui->pbRhs6] = 1;
    m_rightButtonsDisableFlags[ui->pbRhs7] = 1;
    m_rightButtonsDisableFlags[ui->pbRhs8] = 1;

    m_leftLabelsDisableFlags[ui->lbLhs1] = 1;
    m_leftLabelsDisableFlags[ui->lbLhs2] = 1;
    m_leftLabelsDisableFlags[ui->lbLhs3] = 1;
    m_leftLabelsDisableFlags[ui->lbLhs4] = 1;
    m_leftLabelsDisableFlags[ui->lbLhs5] = 1;
    m_leftLabelsDisableFlags[ui->lbLhs6] = 1;
    m_leftLabelsDisableFlags[ui->lbLhs7] = 1;
    m_leftLabelsDisableFlags[ui->lbLhs8] = 1;
}

void SpojniceUi::enableAllRemainingButtons()
{
    for (QPushButton* btn: m_rightButtonsDisableFlags.keys())
        if (m_rightButtonsDisableFlags[btn])
            btn->setEnabled(true);
}

void SpojniceUi::changeTurn()
{
    m_changedTurn = true;

    int nextNonGuessedLabel = getNextNonGuessedLabel();
    m_spojniceController->setCurrentRow(nextNonGuessedLabel);
    markLabelAsNext(nextNonGuessedLabel);

    if (!m_turn)
        disableAll();
    else
        enableAllRemainingButtons();
}

void SpojniceUi::getSpojnicaFields()
{
    QString serverMsg = QString::number(m_id)
                        + "_g_S_"
                        + QString::number(m_spojniceNumber);

    sendMessage(m_server, serverMsg);
}

void SpojniceUi::sendMessage(QTcpSocket* server, QString msg)
{
    server->write(msg.toUtf8());
    server->flush();
}

int SpojniceUi::getNextNonGuessedLabel()
{
    qDebug() << m_missedLabels << m_missedLabelsCounter;
    int n = m_missedLabels.length();
    if (m_missedLabelsCounter < n)
    {
        m_missedLabelsCounter++;
        return m_missedLabels[m_missedLabelsCounter-1]+1;
    }
    else
        return -1;
}


int SpojniceUi::spojniceNumber()
{
    return m_spojniceNumber;
}

Ui::SpojniceUi* SpojniceUi::getUi()
{
    return ui;
}

void SpojniceUi::getResults()
{
    QString serverMsg = QString::number(m_id) + "_s_S_" + QString::number(m_spojniceNumber);
    sendMessage(m_server, serverMsg);
}

void SpojniceUi::showResults(QStringList data)
{
    qDebug() << m_fieldColored;
    showFields(data);

    for (int i = 0; i < 8; i++)
    {
        int playerGuessed = m_fieldColored[i];
        if (playerGuessed)
            markLabelAsFinished(i+1, false, playerGuessed-1, i+1);
        else
            removeButtonColor(i+1);
    }

    QTimer::singleShot(5000, this, &SpojniceUi::nextGame);
}
