#include "mainwindow.h"
#include "ui_asocijacijeui.h"
#include "ui_koznaznaui.h"
#include "spojniceui.h"
#include "ui_mainwindow.h"



#include "ui_skockoui.h"
#include "ui_spojniceui.h"
#include <iostream>
#include <QMessageBox>
#include <QMovie>
#include <QUrl>
#include <QIcon>
#include <QPalette>
#include <QPixmap>
#include <QString>

#include <QDebug>




MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Log in");

    ui->stackedWidget->setCurrentIndex(0);

    ui->pb_Return->setIcon(QIcon(":/icons/resources/return.png"));
    ui->pb_OnOff->setIcon(QIcon(":/icons/resources/on.png"));
    ui->pb_OnOff->setCheckable(true);
    //Background music


    mp = new QMediaPlayer(this);
    audioOutput = new QAudioOutput(this);
    mp->setAudioOutput(audioOutput);
    mp->setSource(QUrl("qrc:/sounds/resources/music.mp3"));
    audioOutput->setVolume(100);
    mp->play();


    connect(ui->pb_ShowBestResults, &QPushButton::clicked, this, [&](){
        najboljiRezultatiUi = new NajboljiRezultati(nullptr, m_socket, m_playerId);
        ui->stackedWidget->insertWidget(3, najboljiRezultatiUi);
        ui->stackedWidget->setCurrentIndex(3);

        connect(najboljiRezultatiUi, &NajboljiRezultati::goBack, this, [&](){
            delete najboljiRezultatiUi;
            ui->stackedWidget->setCurrentIndex(1);
        });
    });

}



MainWindow::~MainWindow()
{
    mp->stop();
    delete audioOutput;
    delete mp;
    delete ui;
}


void MainWindow::on_pb_LogIn_clicked()
{
    m_username = ui->le_UserName->text().trimmed();
    if(!m_username.isEmpty()){
        if (connectToServer())
        {
            std::cout << m_username.toStdString() << std::endl ;
            ui->stackedWidget->setCurrentIndex(1);
            this->setWindowTitle("MainMenu");
        }
        else
        {
            qDebug() << "Neuspesna konekcija sa serverom. Pokusajte ponovo.";
        }
    }
    else{
        showMessageBox("Morate unetiu korisnicko ime da biste zapoceli partiju");
        return;
    }

    ui->le_UserName->setText("");


}

void MainWindow::showMessageBox(QString content) const {
  QMessageBox mb;
  mb.setText(content);
  mb.setWindowTitle("Upozorenje");
  mb.exec();
}

void MainWindow::on_pb_StartGame_clicked()
{
    QString serverMsg = QString::number(m_playerId) + "_r";
    sendMessage(m_socket, serverMsg);

    m_socket->waitForReadyRead(-1);
    QString message= m_socket->readAll();

    bool turn=false;
    QStringList data=message.split("_");
    if (data.size()==4){
        turn=true;
        m_username=data[2];
        m_username2=data[3];
    }
    else{
        m_username=data[1];
        m_username2=data[2];
    }

    //qDebug() << username << opponentUsername;

    qDebug() << "MAIN WINDOW"<<m_playerId;
    qDebug()<<"MAIN W" << m_socket;
    this->setWindowTitle("Slagalica");



    //Skocko

   skockoUi=new SkockoUi(nullptr, m_socket, m_playerId,m_username, m_username2, turn,1);
   ui->stackedWidget->insertWidget(3, skockoUi);
   ui->stackedWidget->setCurrentIndex(3);
   connect(skockoUi, &SkockoUi::endGame, this, &MainWindow::on_EndSkocko,Qt::UniqueConnection);


    //m_koznaznaUi = new Koznaznaui(nullptr, m_socket, m_playerId, 0, 0, 0);
    //ui->stackedWidget->insertWidget(3, m_koznaznaUi);
    //ui->stackedWidget->setCurrentIndex(3);
    //m_koznaznaUi->getQuestion(0);
    //this->connect(m_koznaznaUi, &Koznaznaui::endGame, this, &MainWindow::on_EndKoZnaZna);


    //spojniceUi = new SpojniceUi(this, m_socket, m_playerId, m_username, m_username2, 0, 0, 0);
    //ui->stackedWidget->insertWidget(3, spojniceUi);
    //ui->stackedWidget->setCurrentIndex(3);
    //this->connect(spojniceUi, &SpojniceUi::endGame, this, &MainWindow::on_EndSpojnice);


    //asocijacijeUi=new AsocijacijeUi(this, m_socket, m_playerId, 0, 0, 0);
    //ui->stackedWidget->insertWidget(3, asocijacijeUi);
    //ui->stackedWidget->setCurrentIndex(3);
    //this->connect(asocijacijeUi, &AsocijacijeUi::endGame, this, &MainWindow::on_EndAsocijacije);
}


void MainWindow::on_pb_Settings_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
    this->setWindowTitle("Settings");
}


void MainWindow::on_pb_Return_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
    this->setWindowTitle("MainMenu");
}



void MainWindow::on_sl_Volume_sliderMoved(int position)
{

   audioOutput->setVolume(position/100.0);

}


void MainWindow::on_pb_OnOff_toggled(bool checked)
{
    if(checked){
        ui->pb_OnOff->setIcon(QIcon(":/icons/resources/off.png"));
        mp->stop();
    }
    else{
        ui->pb_OnOff->setIcon(QIcon(":/icons/resources/on.png"));
        mp->play();
    }
}

void MainWindow::on_EndSkocko()
{
    const auto pointsPlayer1 = skockoUi->getUi()->lcdPlayer1->value();
    const auto pointsPlayer2 = skockoUi->getUi()->lcdPlayer2->value();

    skockoUi->deleteLater();
    ui->stackedWidget->removeWidget(skockoUi);
    if (skockoUi->m_skockoNum == 1){
        skockoUi=new SkockoUi(this, m_socket, m_playerId,m_username,m_username2,!skockoUi->m_turn, 2, pointsPlayer1, pointsPlayer2);
        ui->stackedWidget->insertWidget(3, skockoUi);
        ui->stackedWidget->setCurrentIndex(3);
        this->connect(skockoUi, &SkockoUi::endGame, this, &MainWindow::on_EndSkocko);
    }
    else
    {
       qDebug()<<"pokrecem ko zna zna...";
       m_koznaznaUi = new Koznaznaui(nullptr,m_socket,m_playerId,m_username, m_username2, 0,pointsPlayer1,pointsPlayer2);
       ui->stackedWidget->insertWidget(3, m_koznaznaUi);
       ui->stackedWidget->setCurrentIndex(3);
       m_koznaznaUi->getQuestion(0);
       this->connect(m_koznaznaUi, &Koznaznaui::endGame, this, &MainWindow::on_EndKoZnaZna);
    }
}

void MainWindow::on_EndKoZnaZna()
{

    const auto pointsPlayer1 = m_koznaznaUi->getUi()->lcd_score1->value();
    const auto pointsPlayer2 = m_koznaznaUi->getUi()->lcd_score2->value();

    m_koznaznaUi->deleteLater();
    ui->stackedWidget->removeWidget(m_koznaznaUi);

    qDebug() << "pokrecem spojnice....";
    spojniceUi = new SpojniceUi(this, m_socket, m_playerId, m_username, m_username2, 0, pointsPlayer1, pointsPlayer2);
    ui->stackedWidget->insertWidget(3, spojniceUi);
    ui->stackedWidget->setCurrentIndex(3);
    this->connect(spojniceUi, &SpojniceUi::endGame, this, &MainWindow::on_EndSpojnice);

}

void MainWindow::on_EndSpojnice()
{
    const auto pointsPlayer1 = spojniceUi->getUi()->lcdPlayer1->value();
    const auto pointsPlayer2 = spojniceUi->getUi()->lcdPlayer2->value();

    spojniceUi->deleteLater();
    ui->stackedWidget->removeWidget(spojniceUi);
    if (spojniceUi->spojniceNumber() == 0)
    {
        spojniceUi = new SpojniceUi(this, m_socket, m_playerId, m_username, m_username2, 1, pointsPlayer1, pointsPlayer2);
        ui->stackedWidget->insertWidget(3, spojniceUi);
        ui->stackedWidget->setCurrentIndex(3);
        this->connect(spojniceUi, &SpojniceUi::endGame, this, &MainWindow::on_EndSpojnice);
    }
    else
    {
        qDebug() << "pokrecem asocijacije ....";
        asocijacijeUi = new AsocijacijeUi(this, m_socket, m_playerId, m_username, m_username2, 0, pointsPlayer1, pointsPlayer2);
        ui->stackedWidget->insertWidget(3, asocijacijeUi);
        ui->stackedWidget->setCurrentIndex(3);
        this->connect(asocijacijeUi, &AsocijacijeUi::endGame, this, &MainWindow::on_EndAsocijacije);
    }
}

void MainWindow::on_EndAsocijacije()
{

    const auto pointsPlayer1 = asocijacijeUi->getUi()->lcdPlayer1->value();
    const auto pointsPlayer2 = asocijacijeUi->getUi()->lcdPlayer2->value();

    asocijacijeUi->deleteLater();
    ui->stackedWidget->removeWidget(asocijacijeUi);
    if (asocijacijeUi->associationNumber() == 0)
    {
        asocijacijeUi=new AsocijacijeUi(this, m_socket, m_playerId,  m_username, m_username2, 1, pointsPlayer1, pointsPlayer2);
        ui->stackedWidget->insertWidget(3, asocijacijeUi);
        ui->stackedWidget->setCurrentIndex(3);
        this->connect(asocijacijeUi, &AsocijacijeUi::endGame, this, &MainWindow::on_EndAsocijacije);
    }
    else
    {
       qDebug()<<"Kraj slagalice";
       QString serverMsg = QString::number(m_playerId) + "_e";
       sendMessage(m_socket, serverMsg);

       winnerUi = new WinnerUi(nullptr, m_socket, m_playerId, m_username, m_username2, pointsPlayer1, pointsPlayer2);
       ui->stackedWidget->insertWidget(4, winnerUi);
       ui->stackedWidget->setCurrentIndex(4);

       connect(winnerUi, &WinnerUi::goToMainMenu, this, [&]()
       {
           winnerUi->deleteLater();
           ui->stackedWidget->setCurrentIndex(1);
       });
    }
}


bool MainWindow::connectToServer()
{
    m_socket = new QTcpSocket(this);
    connect(m_socket, SIGNAL(connected()), this, SLOT(connected()));
    connect(m_socket, SIGNAL(disconnected()), this, SLOT(disconnected()));

    qDebug() << "Connecting....";
    m_socket->connectToHost(QHostAddress::LocalHost, 8000);
    if (m_socket->waitForConnected(2000))
    {
        m_socket->waitForReadyRead();
        m_playerId = m_socket->readAll().toInt();

        QString msg = QString::number(m_playerId) + "_n_" + m_username;
        sendMessage(m_socket, msg);

        return true;
    }
    {
        qDebug() << "Error connecting to server: " << m_socket->errorString();
        return false;
    }
}

void MainWindow::connected()
{
    qDebug() << "Connected to server.";
}

void MainWindow::disconnected()
{
    ui->stackedWidget->currentWidget()->deleteLater();
    ui->stackedWidget->setCurrentIndex(0);

    qDebug() << "Disconnected from server.";
}

Ui::MainWindow *MainWindow::getUi() const
{
    return ui;
}

void MainWindow::sendMessage(QTcpSocket* socket,QString msg)
{
    socket->write(msg.toUtf8());
    socket->flush();

}



