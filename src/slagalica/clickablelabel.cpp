#include "clickablelabel.h"

ClickableLabel::ClickableLabel(QWidget* parent, Qt::WindowFlags f)
    : QLabel(parent) {
    Q_UNUSED(f);
}

ClickableLabel::~ClickableLabel() {}

void ClickableLabel::disable()
{
    m_enabled = false;
}

void ClickableLabel::enable()
{
    m_enabled = true;
}

void ClickableLabel::mousePressEvent(QMouseEvent* event) {
    Q_UNUSED(event);
    if (m_enabled){
        emit clicked();
    }
}
