#ifndef SKOCKO_H
#define SKOCKO_H

#include <iostream>
#include <QString>
#include <QPair>
#include <QVector>
#include <QMap>
#include <QTcpSocket>

#include "game.h"

enum class Symbols{
  Skocko,
  Tref,
  Pik,
  Srce,
  Karo,
  Zvezda,
  Prazno
};

enum class Status{
    Crveno,
    Zuto,
    Transparentno
};


class Skocko: public Game
{

private:
    int m_currentRow=1;
public:
    Skocko();

    bool m_turnCounter;

    QVector<Symbols> m_randomCombination;
    QVector<Symbols> m_currentCombination;
    QVector<Status> m_status;
    QString m_name;


    void guess(QVector<Symbols> symbols, QTcpSocket* server, int m_clientId,int skockoNum,int attemtNum);
    virtual void init() override;
    virtual int calculateScore() override;
    QVector<Symbols> createRandomCombination();
    int findFirstEmpty();

    void resetSymbolCombination(int column);

    bool isCombinationCompleted() const;

    QString combinationToAttemptString(QVector<Symbols> combination);
    QVector<Status> attemptStringToStatus(QString attemptString);
    void sendMessage(QTcpSocket* server, QString msg);

    // Getters
    int getCurrentRow() const;
    void setCurrentRow(int currentRow);
    virtual ~Skocko();
};

#endif // SKOCKO_H
