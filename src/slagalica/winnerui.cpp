#include "winnerui.h"
#include "qtcpsocket.h"
#include "ui_winnerui.h"

WinnerUi::WinnerUi(QWidget *parent, QTcpSocket *server, int playerId, QString clientName1, QString clientName2, int playerPoints1, int playerPoints2) :
    QWidget(parent),
    ui(new Ui::WinnerUi),
    m_server(server),
    m_playerId(playerId),
    m_clientName1(clientName1),
    m_clientName2(clientName2),
    m_playerPoints1(playerPoints1),
    m_playerPoints2(playerPoints2)
{
    ui->setupUi(this);

    connect(ui->pb_ReturnToMainMenu, &QPushButton::pressed, this, &WinnerUi::on_pbMainMenuClicked);
    ui->lcdPlayer1->display(playerPoints1);
    ui->lcdPlayer2->display(playerPoints2);

    ui->lePlayer1Username->setText(m_clientName1);
    ui->lePlayer2Username->setText(m_clientName2);

    int widthOutcomeImage = ui->lbOutcomeImg->width() - 5;
    int heigthOutcomeImage = ui->lbOutcomeImg->height() - 5;

    auto outcome = checkWinner();

    if (outcome == Outcomes::Win){
        std::string path = ":/icons/resources/winner.png";
        const char* str = path.c_str();
        QPixmap icon(str);

        QString style =
            "color: #49be25;"
            "font-size: 30px;"
            "font-family: \"Lucida Console\", \"Courier New\", monospace;";
        ui->lbOutcome->setStyleSheet(style);
        ui->lbOutcome->setText("Pobedili ste!");

         ui->lbOutcomeImg->setPixmap(icon.scaled(widthOutcomeImage, heigthOutcomeImage, Qt::KeepAspectRatio));

    } else if (outcome == Outcomes::Lose){
        std::string path = ":/icons/resources/game-over.png";
        const char* str = path.c_str();
        QPixmap icon(str);

        QString style =
            "color: #F50c46;"
            "font-size: 30px;"
            "font-family: \"Lucida Console\", \"Courier New\", monospace;";
        ui->lbOutcome->setStyleSheet(style);
        ui->lbOutcome->setText("Izgubili ste");

        ui->lbOutcomeImg->setPixmap(icon.scaled(widthOutcomeImage, heigthOutcomeImage, Qt::KeepAspectRatio));

    } else if (outcome == Outcomes::Tie){
        std::string path = ":/icons/resources/handshake.png";
        const char* str = path.c_str();
        QPixmap icon(str);

        QString style =
            "color: #E68f11;"
            "font-size: 30px;"
            "font-family: \"Lucida Console\", \"Courier New\", monospace;";
        ui->lbOutcome->setStyleSheet(style);
        ui->lbOutcome->setText("Nereseno!");

        ui->lbOutcomeImg->setPixmap(icon.scaled(widthOutcomeImage, heigthOutcomeImage, Qt::KeepAspectRatio));

    }
}

WinnerUi::Outcomes WinnerUi::checkWinner()
{
    if (m_playerPoints1 == m_playerPoints2){
        return Outcomes::Tie;
    }
    int winnerId = m_playerPoints1 > m_playerPoints2 ? 1 : 2;
    QString winnerUsername = m_playerPoints1 > m_playerPoints2  ? m_clientName1 : m_clientName2;

    if (winnerId == m_playerId){
        QString msg = "j_" + winnerUsername;

        sendMessage(m_server, msg);


        return Outcomes::Win;
    }
    // else
    return Outcomes::Lose;
}

void WinnerUi::sendMessage(QTcpSocket *server, QString msg)
{
    server->write(msg.toUtf8());
    server->flush();
}

void WinnerUi::on_pbMainMenuClicked()
{
    emit goToMainMenu();
}

WinnerUi::~WinnerUi()
{
    delete ui;
}
