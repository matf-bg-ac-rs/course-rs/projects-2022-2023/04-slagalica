#ifndef SLAGALICA_H
#define SLAGALICA_H
#include "game.h"
#include "player.h"

class Slagalica
{
public:
    Slagalica()=default;

    Player m_player1;
    Player m_player2;
    Game *m_game;

    void startGame();
    void showResult();
    void updateScore(Player p ,int score);
    void endGame();
    void onIsFinished();

};

#endif // SLAGALICA_H
