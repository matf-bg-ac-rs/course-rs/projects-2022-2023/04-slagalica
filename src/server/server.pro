QT += core network
QT -= gui

CONFIG += c++17 console Wall Wextra
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        player.cpp \
        server.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    player.h \
    server.h

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../serialization/release/ -lserialization
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../serialization/debug/ -lserialization
else:unix: LIBS += -L$$OUT_PWD/../serialization/ -lserialization

INCLUDEPATH += $$PWD/../serialization
DEPENDPATH += $$PWD/../serialization

RESOURCES += \
    resources.qrc
