#include "server.h"
#include "includes/jsonserializer.h"
#include "includes/gameslistmodel.h"
#include <QDir>
#include <QByteArray>


Server::Server(QObject* parent):
    QObject(parent)
{
    srand(time(NULL));
}

void Server::startServer()
{
    m_server = new QTcpServer();
    m_server->setMaxPendingConnections(4);
    connect(m_server, SIGNAL(newConnection()), this, SLOT(newClientConnection()));
    m_jsonSerializer = new JSONSerializer();

    if (m_server->listen(QHostAddress::Any, 8000))
    {
        qDebug() << "Started Server at port 8000.";
    }
    else
    {
        qDebug() << &"Server failed to start. Error: " [ m_server->serverError()];
    }

    m_client1 = new Player();
    m_client2 = new Player();
}

void Server::newClientConnection()
{   
    QTcpSocket* client = m_server->nextPendingConnection();

    connect(client, &QTcpSocket::disconnected, this, &Server::socketDisconnected);
    connect(client, &QTcpSocket::readyRead, this, &Server::socketReadyRead);
    connect(client, &QTcpSocket::stateChanged, this, &Server::socketStateChanged);

    if (m_client1->m_socket == nullptr)
    {
        m_client1->m_socket = client;
        m_client1->m_player_id=1;

        sendMessage(m_client1->m_socket,"1");
        qDebug() << "Client 1 connected.";
    }
    else if (m_client2->m_socket == nullptr)
    {
        m_client2->m_socket = client;
        m_client2->m_player_id=2;
        sendMessage(m_client2->m_socket,"2");

        qDebug() << "Client 2 connected.";
    }
    else
    {
        client->disconnectFromHost();
        qDebug() << "Refused connection to client. 2 clients allready connected.";
    }
}

void Server::socketDisconnected()
{

    QTcpSocket* client = qobject_cast<QTcpSocket*>(QObject::sender());
    if (client == m_client1->m_socket)
    {
        if (m_client2->m_socket != nullptr)
            m_client2->m_socket->disconnectFromHost();
    }
    else
    {
        if (m_client1->m_socket != nullptr)
            m_client1->m_socket->disconnectFromHost();
    }


    m_client1 = new Player();
    m_client2 = new Player();

    qDebug() << "Client disconnected.";
    // poslati poruku klijentu o prekidu igre!
}
void Server::readKoZnaZna()
{
    QStringList pitanja;
    QString path=":/data/resources/koZnaZna.txt";
    QFile file(path);
    m_koZnaZnaAnswersMap = QMap<int, int>();
    m_questions_and_answers= QStringList();

    qDebug()<<"USAOOOOOO";
    file.open(QFile::ReadOnly);
    QTextStream in(&file);
    QStringList lines;

    while(!in.atEnd()){
        QString line = in.readLine();
        lines.append(line);
//        m_questions_and_answers.append(line);
    }
    file.close();

    QVector<int> numbers=generateKoZnaZnaCombination(lines.size());
    for (int i = 0; i < 10; i++){
        m_questions_and_answers.append(lines.at(numbers[i]));
    }

    for (int i = 0; i < 10; i++)
    {
        m_koZnaZnaAnswersMap[i] = m_questions_and_answers.at(i).split(",")[1].toInt()-1;
    }

    qDebug()<<"USAOOOOOO";

}

void Server::readSpojnice()
{
    QString path=":/data/resources/spojnice.txt";
    QFile file(path);
    file.open(QFile::ReadOnly);
    QTextStream in(&file);

    m_spojniceWords = QVector<QMap<QString, QString>>();

    QStringList lines;

    while(!in.atEnd()){
        lines.append(in.readLine());
    }
    file.close();

    QVector<int> combinations= generate_two_ints(lines.size());
    for (auto i:combinations){
        QMap<QString, QString> spojniceWords = QMap<QString, QString>();
        QString line = lines.at(i);
        qDebug()<<line;
        QStringList pojmovi = line.trimmed().split("_");
        for(auto pojam : pojmovi){
            QStringList podeljeno= pojam.trimmed().split(",");
            spojniceWords[podeljeno[0]]=podeljeno[1];
        }

        m_spojniceWords.append(spojniceWords);
    }
    qDebug()<<m_spojniceWords;
    shuffleSpojnice();
}

void Server::readAsocijacije()
{
    QString path=":/data/resources/asocijacije.txt";
    QFile file(path);
    file.open(QFile::ReadOnly);
    QTextStream in(&file);
    QStringList lines;

    m_columnsFields = QVector<QMap<QChar, QVector<QString>>>();
    m_columnsResults = QVector<QMap<QChar, QString>>();
    m_associationAsAString =QVector<QString>();

    while(!in.atEnd()){
        lines.append(in.readLine());
    }
    file.close();


    QVector<int> combinations=generate_two_ints(lines.size());
    for(auto i : combinations){
        QMap<QChar, QVector<QString>> columnsFields = QMap<QChar, QVector<QString>>();
        QString line = lines.at(i);
        qDebug()<<"linija"<<line;

        QStringList asocijacije=line.split(";");
        qDebug()<<asocijacije[0];
        for (QString kolona : asocijacije[0].split("_")){
            qDebug()<<kolona;
            QVector<QString> kol;
            QStringList podeljeno=kolona.split(":");
            QStringList polja=podeljeno[1].split(",");
            for(QString polje : polja){
                kol.append(polje);
            }
            columnsFields[podeljeno[0][0]].append(kol);

        }

        m_columnsFields.append(columnsFields);
        QMap<QChar, QString> columnsResults = QMap<QChar, QString>();

        for (QString resenje:asocijacije[1].split("_")){
            qDebug()<<resenje;
             QStringList podeljeno= resenje.split(":");
             columnsResults[podeljeno[0][0]]=podeljeno[1];
        }
        columnsResults['K']=asocijacije[2];
        m_columnsResults.append(columnsResults);
    }


}
void Server::socketReadyRead()
{
    QTcpSocket* client = qobject_cast<QTcpSocket*>(QObject::sender());
    QString message = QString(client->readAll()).trimmed();
    QStringList data=message.split("_");

    qDebug() << "Received msg: " << message;

    if (data[0] == "j"){
        QString path = QDir::currentPath() + "/games_data.json";
        qDebug() << path;
        // poruka : j_noviJson
        // TODO : add relative path
        QString username = data[1];

        GamesListModel* gamesListModel = new GamesListModel();
        m_jsonSerializer->load(*gamesListModel, path);

        auto games = &gamesListModel->getGames();

        auto it = std::find_if(games->begin(), games->end(), [username](const GameModel* game){
            return game->getUsername() == username;
        });

        if (it != games->end()){
            (*it)->incrementWins();
        } else {
            games->push_back(new GameModel(username, 1));
        }

        m_jsonSerializer->save(*gamesListModel, path);
    }

    if (data[1] == "r"){    // player ready for game -> clicked start on modal
        readyPlayer(data[0].toInt());
    }
    if (data[1] == "w"){
        QString path = QDir::currentPath() + "/games_data.json";
        qDebug() << path;
        GamesListModel* gamesList = new GamesListModel();
        m_jsonSerializer->load(*gamesList, path);

        auto games = &gamesList->getGames();

        std::sort(games->begin(), games->end(), [](const GameModel* g1, const GameModel* g2){
            if (g1->getGamesWon() != g2->getGamesWon()){
                return g1->getGamesWon() > g2->getGamesWon();
            } else {
                return g1->getUsername() < g2->getUsername();
            }
        });

        QVector<GameModel*> *selectedGames = new QVector<GameModel*>{};

        const int n = games->size() < 10 ? games->size() : 10;
        std::copy(games->begin(), games->begin() + n, std::back_inserter(*selectedGames));

        gamesList->getGames() = *selectedGames;

        qDebug() << m_jsonSerializer->saveAsString(*gamesList);

        if (data[0]=="1"){
            m_jsonSerializer->sendDataToSocket(*gamesList,m_client1->m_socket);
        }
        else{
            m_jsonSerializer->sendDataToSocket(*gamesList,m_client2->m_socket);
        }
    }

    if (data[1] == "a")
    {
        if (data[2] == "s") // provera odgovora za skocko
        {
            skockoAttempt(data);
        }
        else if (data[2] == "k") // provera odgovora za  ko zna zna
        {
            koZnaZnaAttempt(data);
        }
        else if (data[2] == "a") // provera odgovora za asocijacije
            checkAssociationAttempt(data);
        else if (data[2] == "S")
            checkSpojniceAttempt(data);
        else
            qDebug() << "Error: wrong attempt message send!";
    }


    if (data[1] == "n") // client salje username
    {
        if (data[0] == "1")
            m_client1->m_name = data[2];
        else if (data[0] == "2")
            m_client2->m_name = data[2];
        else
            qDebug() << "Wrong id." << data[0];
    }

    if (data[1] == "g") // server vraca generisano pitanje/spojnicu/asoc
    {
        if (data[2] == "k") // server vraca generisano pitanje za kzz
        {
            returnKoZnaZnaQuestion(data);
        }
        else if (data[2] == "a") // server vraca polje za asoc
            returnAssociationField(data);
        else if (data[2] == "S")
            returnSpojniceWords(data);
    }

    if (data[1] == "s") // server vraca solution za igru
    {
        if (data[2] == "S")
            sendSpojniceResult(data);

        if (data[2] == "a")
            sendAssociationResult(data);
    }

    if (data[1] == "e")
    {
        if (data[0] == "1")
            m_client1->m_isReady = false;
        else
            m_client2->m_isReady = false;
    }

    // implementirati funkcije za razlicite odgovore servera na poruke klijenata
}
void Server::skockoAttempt(QStringList data)
{
    QString attemptString = data[5];
    QVector<QChar> attempt = QVector<QChar>();
    for (QChar c: attemptString)
        attempt.push_back(c);

    QString attemptReturn;
    if (data[3] == "1")
    {
        attemptReturn = checkSkockoAttempt(m_skocko1CombinationMsg, attempt);
    }
    else if (data[3] == "2")
    {
        attemptReturn = checkSkockoAttempt(m_skocko2CombinationMsg, attempt);
    }
    else
        qDebug() << "ERROR!";

    attemptReturn += "_";
    attemptReturn += attemptString;
    attemptReturn += "_";
    QString poeni = calculateSkockoPoints(data[4],attemptReturn[0]);
    attemptReturn += poeni;

    if (data[4] == "7")
    {
        attemptReturn += "_";
        if (data[3] == "1")
            attemptReturn += m_skocko1CombinationMsg;
        else
            attemptReturn += m_skocko2CombinationMsg;

        sendMessage(m_client1->m_socket, attemptReturn);
        sendMessage(m_client2->m_socket, attemptReturn);
    }
    else
    {

        if(data[0] == "1"){
            sendMessage(m_client2->m_socket, attemptReturn);
            attemptReturn+="_";
            attemptReturn+="turn";
            sendMessage(m_client1->m_socket, attemptReturn);
        }
        else{
            sendMessage(m_client1->m_socket, attemptReturn);
            attemptReturn+="_";
            attemptReturn+="turn";
            sendMessage(m_client2->m_socket, attemptReturn);
        }
    }
}

void Server::returnKoZnaZnaQuestion(QStringList data)
{

    int questionNumber = data[3].toInt();
    QString question = m_questions_and_answers.at(questionNumber).split(",")[0];

    if (data[0] == "1")
        sendMessage(m_client1->m_socket, "g_" + question);
    else
        sendMessage(m_client2->m_socket, "g_" + question);

}
void Server::koZnaZnaAttempt(QStringList data)
{

    int questionNumber = data[3].toInt();
    int answerNumber = data[4].toInt();
    int msToAnswer = data[5].toInt();
    qDebug() << "kzz attempt: " << questionNumber << answerNumber << msToAnswer;

    m_koZnaZnaAttemptReturn[6] = QString::number(m_koZnaZnaAnswersMap[questionNumber]).at(0);
    if (data[0] == "1")
    {
        m_koZnaZnaAttemptReturn[2] = QString::number(answerNumber).at(0);
        m_koZnaZnaAnswersMs[1] = checkKoZnaZnaAttempt(questionNumber, answerNumber, msToAnswer);
    }
    else if (data[0] == "2")
    {
        m_koZnaZnaAttemptReturn[4] = QString::number(answerNumber).at(0);
        m_koZnaZnaAnswersMs[2] = checkKoZnaZnaAttempt(questionNumber, answerNumber, msToAnswer);
    }
    m_koZnaZnaAnswersMs[0]++;
    updateKoZnaZnaPoints();
}


QString Server::calculateSkockoPoints(QString attemptNum, QString numOfReds)
{
    if (numOfReds == "4"){
        if (attemptNum== "6"){
            return "10";
        }
        else if (attemptNum =="5"){
            return "15";
        }
        else return "20";
    }
    else return "0";

}
void Server::socketStateChanged(QAbstractSocket::SocketState state)
{
    QString stateMessage = "";

    if (state == QAbstractSocket::UnconnectedState)
        stateMessage = "Socket not connected.";
    if (state == QAbstractSocket::HostLookupState)
        stateMessage = "Performing host name lookup.";
    if (state == QAbstractSocket::ConnectingState)
        stateMessage = "Establishing connection.";
    if (state == QAbstractSocket::ConnectedState)
        stateMessage = "Established connection";
    if (state == QAbstractSocket::BoundState)
        stateMessage = "Socket is bound.";
    if (state == QAbstractSocket::ClosingState)
        stateMessage = "Socket is closing.";
    if (state == QAbstractSocket::ListeningState)
        stateMessage = "Socket for internal use only.";

    qDebug() << stateMessage;
}

void Server::sendMessage(QTcpSocket* socket,QString msg)
{
    qDebug() << "Sending msg: " << msg;
    socket->write(msg.toUtf8());
    socket->flush();

}
void Server::readyPlayer(int player_id)
{

    if(player_id == 1){
        m_client1->m_isReady=true;
        //sendMessage(m_client1->m_socket, "start game"); // temp, ne mora 2 klijenta da bi se pokrenula igra. Komentarisati da bi se cekala oba klijenta za pocetak igre.
    }
    else if (player_id == 2){
        m_client2->m_isReady=true;
    }
    else return;

    if (m_client1->m_isReady && m_client2->m_isReady){
        generateCombinations();
        sendMessage(m_client1->m_socket, "start game_turn_" + m_client1->m_name + "_" + m_client2->m_name);
        sendMessage(m_client2->m_socket, "start game_" + m_client1->m_name + "_" + m_client2->m_name);

    }

}

void Server::generateCombinations()
{
    m_skocko1CombinationMsg = generateSkockoCombination();
    m_skocko2CombinationMsg = generateSkockoCombination();
    readKoZnaZna();
    readSpojnice();
    generateAsocijacije();
}
QVector<QChar> Server::generateSkockoCombination()
{
    // S -> Skocko, t -> tref, p -> pik, s -> srce, k -> karo, z -> zvezda
    QVector<QChar> allSymbols = {'S', 't', 'p', 's', 'k', 'z'};
    auto rd = std::random_device {};
    auto rng = std::default_random_engine { rd() };
    std::shuffle(allSymbols.begin(), allSymbols.end(), rng);

    QVector<QChar> combination = {allSymbols[0], allSymbols[1], allSymbols[2], allSymbols[3]};
//    QVector<QChar> combination= {'t','t','t','t'};
    return combination;
}
QVector<int> Server::generate_two_ints(int limit)
{
    QVector<int> return_vector;
    return_vector.append(rand() % limit);

    int attempt;
    while(true){
        attempt=rand() % limit;
        if(attempt!=return_vector[0]){
            return_vector.append(attempt);
            break;
        }
    }
    return return_vector;
}

QVector<int> Server::generateKoZnaZnaCombination(int number_of_lines)
{  
    m_koZnaZnaAnswersMap = QMap<int, int>();
    m_koZnaZnaAnswersMs = {0, 0, 0};
    m_koZnaZnaAttemptReturn = "u_______0_0"; // client1Ans_client2Ans_correctAns_client1Points_client2Points

    QVector<int> questionNumbers;

    while(questionNumbers.size()<10){
        int attempt = rand() % number_of_lines;
        bool postoji =false;
        for(auto number : questionNumbers){
            if(number==attempt){
                postoji=true;
            }
        }
        if(!postoji){
            questionNumbers.push_back(attempt);
        }
    }
    return questionNumbers;
}



void Server::shuffleSpojnice() // import spojnice from json based on rand -> TODO!
{
    m_spojniceShuffle = QVector<QVector<int>>();

    QVector<int> spojniceShuffle = QVector<int>();
    spojniceShuffle = {0, 1, 2, 3, 4, 5, 6, 7};

    auto rd = std::random_device {};
    auto rng = std::default_random_engine { rd() };

    std::shuffle(spojniceShuffle.begin(), spojniceShuffle.end(), rng);
    m_spojniceShuffle.append(spojniceShuffle);

    std::shuffle(spojniceShuffle.begin(), spojniceShuffle.end(), rng);
    m_spojniceShuffle.append(spojniceShuffle);

    qDebug() << "Spojnice shuffle: " << m_spojniceShuffle[0];
}

QTcpSocket* Server::getPlayerSocket(int id){
    if (id == 1){
        return m_client1->m_socket;
    }
    else {
        return m_client2->m_socket;
    }
}
void Server::setPlayerSocket(int id, QTcpSocket* socket){
    if (id == 1){
        m_client1->m_socket = socket;
    }
    else {
        m_client2->m_socket = socket;
    }

}

void Server::generateAsocijacije() // import ass from json based on rand -> TODO!
{
    m_associationPoints = QVector<QMap<QChar, int>>();
    QMap<QChar, int> associationPoints = QMap<QChar,int>();
    associationPoints['A'] = 6;
    associationPoints['B'] = 6;
    associationPoints['C'] = 6;
    associationPoints['D'] = 6;
    associationPoints['K'] = 5;
    m_associationPoints.append(associationPoints);
    m_associationPoints.append(associationPoints);

    qDebug()<<"#############################################";

    readAsocijacije();
    associationToString();

}

QString Server::checkSkockoAttempt(QVector<QChar> correctCombination, QVector<QChar> attempt)
{
    //testira attempt za skocka i vraca string u obliku brojCrvenihbrojZutih, npr ako je 1 na mestu, a 3 nisu onda ce biti: "13"
    int redMarkers = 0;
    int yellowMarkers = 0;

    QVector<bool> usedCorrectCombination = {false, false, false, false};
    QVector<bool> usedAttemptCombination = {false, false, false, false};

    for (int i = 0; i < 4; i++)
    {
        if (attempt[i] == correctCombination[i])
        {
            redMarkers++;
            usedCorrectCombination[i] = true;
            usedAttemptCombination[i] = true;
        }
    }

    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            if (attempt[i] == correctCombination[j] && !usedCorrectCombination[j] && !usedAttemptCombination[i])
            {
                usedCorrectCombination[j] = true;
                usedAttemptCombination[i] = true;
                yellowMarkers++;
                break;
            }
        }
    }


    return QString::number(redMarkers) + QString::number(yellowMarkers);
}

int Server::checkKoZnaZnaAttempt(int questionNumber, int answerNumber, int msToAnswer)
{

    if (m_koZnaZnaAnswersMap[questionNumber] == answerNumber) // ako je pogodjen tacan odgovor
        return msToAnswer;
    else
    {
        if (answerNumber != 4)  // ako je pogreseno
            return -1;
        else
            return -2;  // ako nije dat odgovor
    }
}

void Server::updateKoZnaZnaPoints()
{
    if (m_koZnaZnaAnswersMs[0] != 2)
        return;

    if (m_koZnaZnaAnswersMs[1] >= 0 and m_koZnaZnaAnswersMs[2] >= 0)
    {
        if (m_koZnaZnaAnswersMs[1] < m_koZnaZnaAnswersMs[2])
            m_koZnaZnaAttemptReturn[8] = QString::number(1).at(0);
        else
            m_koZnaZnaAttemptReturn[10] = QString::number(1).at(0);
    }
    else if (m_koZnaZnaAnswersMs[1] >= 0)
        m_koZnaZnaAttemptReturn[8] = QString::number(1).at(0);
    else if (m_koZnaZnaAnswersMs[2] >= 0)
        m_koZnaZnaAttemptReturn[10] = QString::number(1).at(0);

    if (m_koZnaZnaAnswersMs[1] == -1)
        m_koZnaZnaAttemptReturn[8] = QString::number(5).at(0);
    if (m_koZnaZnaAnswersMs[2] == -1)
        m_koZnaZnaAttemptReturn[10] = QString::number(5).at(0);

    sendMessage(m_client1->m_socket, m_koZnaZnaAttemptReturn);
    sendMessage(m_client2->m_socket, m_koZnaZnaAttemptReturn);

    m_koZnaZnaAnswersMs = QVector<int> {0, 0, 0};
    m_koZnaZnaAttemptReturn = "u_______0_0";
}

void Server::returnSpojniceWords(QStringList data)
{
    QString serverMsg = "g";

    int spojniceNumber = data[3].toInt();
    QVector<QString> spojniceKeys = m_spojniceWords[spojniceNumber].keys();
    m_spojniceOrderedLeftWords = spojniceKeys;

    for (QString key: spojniceKeys)
        serverMsg += "_" + key;

    for (int i: m_spojniceShuffle[spojniceNumber])
        serverMsg += "_" + m_spojniceWords[spojniceNumber][spojniceKeys[i]];

    qDebug() << "Sending spojniec to players: " << serverMsg;
    sendMessage(m_client1->m_socket, serverMsg);
    sendMessage(m_client2->m_socket, serverMsg);
}

void Server::checkSpojniceAttempt(QStringList data)
{
    int spojniceNumber = data[3].toInt();
    int leftNumber = data[4].toInt();
    int rightNumber = data[5].toInt();

    QString serverMsg = "u_" + QString::number(leftNumber) + "_" + QString::number(rightNumber) + "_";

    if (rightNumber == -1)
        serverMsg += "i_0_0";
    else if (m_spojniceShuffle[spojniceNumber][rightNumber] == leftNumber)
    {
        if (data[0] == '1')
            serverMsg += "c_2_0";
        else
            serverMsg += "c_0_2";
    }
    else
        serverMsg += "i_0_0";

    sendMessage(m_client1->m_socket, serverMsg);
    sendMessage(m_client2->m_socket, serverMsg);
}

void Server::sendSpojniceResult(QStringList data)
{
    int spojniceNumber = data[3].toInt();

    QString msg = "s_";
    for (int i = 0; i < 8; i++)
        msg += m_spojniceOrderedLeftWords[i] + "_";
    for (int i = 0; i < 8; i++)
        msg += m_spojniceWords[spojniceNumber][m_spojniceOrderedLeftWords[i]] + "_";

    if (data[0] == "1")
        sendMessage(m_client1->m_socket, msg);
    if (data[0] == "2")
        sendMessage(m_client2->m_socket, msg);
}

void Server::returnAssociationField(QStringList data)
{
    int associationNumber = data[3].toInt();
    QChar columnLetter = data[4].at(0);
    int columnNumber = data[5].toInt();

    QString msg = "g_" + QString(columnLetter) + "_" + QString::number(columnNumber);

    QString word = m_columnsFields[associationNumber][columnLetter][columnNumber-1];
    msg += "_" + word;

    calculateAssociationPoints(associationNumber, columnLetter, false, false);

    sendMessage(m_client1->m_socket, msg);
    sendMessage(m_client2->m_socket, msg);
}



void Server::checkAssociationAttempt(QStringList data)
{
    int associationNumber = data[3].toInt();
    QChar columnLetter = data[4].at(0);
    QString attemptString = data[5].toLower();
    bool correctFlag = false;

    QString msg = "u_" + QString(columnLetter) + "_";
    if (attemptString == "")
        msg += "__";
    else
        msg += attemptString + "_";

    if (m_columnsResults[associationNumber][columnLetter].toLower() == attemptString)
        correctFlag = true;

    if (correctFlag)
        msg += "c_";
    else msg += "i_";

    int points = calculateAssociationPoints(associationNumber, columnLetter, true, correctFlag);
    if (data[0] == '1')
        msg += QString::number(points) + "_0";
    else
        msg += "0_" + QString::number(points);

    if (correctFlag)
    {
        if (columnLetter == 'K')
        {
            msg += "_" + m_associationAsAString[associationNumber];
        }
        else
        {
            for (QString word: m_columnsFields[associationNumber][columnLetter])
                msg += "_" + word;
        }
    }

    sendMessage(m_client1->m_socket, msg);
    sendMessage(m_client2->m_socket, msg);
}

int Server::calculateAssociationPoints(int associationNumber, QChar columnLetter, bool attemptFlag, bool correctFlag)
{
    int points = 0;

    if (attemptFlag)
    {
        if (correctFlag)
        {
            if (columnLetter == 'K')
            {
                for (int columnPoints: m_associationPoints[associationNumber])
                    points += columnPoints;
            }
            else
            {
                points = m_associationPoints[associationNumber][columnLetter];
                m_associationPoints[associationNumber][columnLetter] = 0;
            }
        }
        else
            points = 0;
    }
    else
    {
        m_associationPoints[associationNumber][columnLetter]--;
        points = 0;
    }

    return points;
}

void Server::associationToString()
{
    for (int i = 0; i < 2; i++)
    {
        QString associationAsAString = "";

        for (QString columnResult: m_columnsResults[i])

            associationAsAString += columnResult + "_";

        for (auto column: m_columnsFields[i])
        {
            for (QString field: column)
                associationAsAString += field + "_";
        }

        associationAsAString.chop(1);
        m_associationAsAString.append(associationAsAString);
        qDebug()<<"asocijacija string"<<m_associationAsAString;

    }
}

void Server::sendAssociationResult(QStringList data)
{
    int associationNumber = data[3].toInt();

    QString msg = "s_0_0_0_0_0_" + m_associationAsAString[associationNumber]; //sending message in this to form to be equivalent to sending fields on correct guess of final solution!

    if (data[0] == "1")
        sendMessage(m_client1->m_socket, msg);
    if (data[0] == "2")
        sendMessage(m_client2->m_socket, msg);
}


