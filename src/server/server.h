#ifndef SERVER_H
#define SERVER_H

#include <algorithm>
#include <time.h>
#include <random>

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>
#include <QVector>
#include <QMap>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>

#include "player.h"
class Serializer;
class Server: public QObject
{
    Q_OBJECT

private:

    QTcpServer* m_server;
    Player* m_client1 = new Player();
    Player* m_client2 = new Player();

    QVector<QChar> m_skocko1CombinationMsg;
    QVector<QChar> m_skocko2CombinationMsg;
    void readKoZnaZna();
    void readSpojnice();
    void readAsocijacije();

    QVector<int> m_koZnaZnaCombinationMsg;
    QStringList m_questions_and_answers;
    QVector<int> m_koZnaZnaAnswersMs;
    QString m_koZnaZnaAttemptReturn;

    int m_spojnice1CombinationMsg;
    int m_spojnice2CombinationMsg;
    QVector<QMap<QString, QString>> m_spojniceWords;
    QVector<QString> m_spojniceOrderedLeftWords;

    int m_asocijacije1CombinationMsg;
    int m_asocijacije2CombinationMsg;
    


    Serializer *m_jsonSerializer;


    void readyPlayer(int player_id);
    void sendMessage(QTcpSocket* socket,QString msg);

    void skockoAttempt(QStringList data);
    void koZnaZnaAttempt(QStringList data);

    void generateCombinations();

    void updateKoZnaZnaPoints();
    void returnKoZnaZnaQuestion(QStringList data);

    void returnSpojniceWords(QStringList data);
    void checkSpojniceAttempt(QStringList data);
    void sendSpojniceResult(QStringList data);

    void returnAssociationField(QStringList data);
    void checkAssociationAttempt(QStringList data);

    void sendAssociationResult(QStringList data);

public:
    QVector<int> generate_two_ints(int limit);
    QVector<QVector<int>> m_spojniceShuffle;


    void shuffleSpojnice();

    QTcpSocket* getPlayerSocket(int id);
    void setPlayerSocket(int id,QTcpSocket* socket);



    QMap<int, int> m_koZnaZnaAnswersMap;
    QVector<QString> m_associationAsAString;
    QVector<QMap<QChar, int>> m_associationPoints;
    QVector<QMap<QChar, QString>> m_columnsResults;
    QVector<QMap<QChar, QVector<QString>>> m_columnsFields;


    explicit Server(QObject *parent = nullptr);
    void startServer();

    QVector<QChar> generateSkockoCombination();
    QString calculateSkockoPoints(QString attemptNum, QString numOfReds);
    QString checkSkockoAttempt(QVector<QChar> correctCombination, QVector<QChar> attempt);

    QVector<int> generateKoZnaZnaCombination(int number_of_lines);
    int checkKoZnaZnaAttempt(int questionNumber, int answerNumber, int msToAnswer);

    void generateAsocijacije();
    int calculateAssociationPoints(int associationNumber, QChar column, bool attemptFlag, bool correctFlag);
    void associationToString();

public slots:

    void newClientConnection();
    void socketDisconnected();
    void socketReadyRead();
    void socketStateChanged(QAbstractSocket::SocketState state);

};

#endif // SERVER_H
