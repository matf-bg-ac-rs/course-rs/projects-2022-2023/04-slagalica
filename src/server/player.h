#ifndef PLAYER_H
#define PLAYER_H

#include <QTcpSocket>
#include <QString>

class Player
{
public:
    Player();
    int m_player_id;
    QTcpSocket* m_socket;
    QString m_name;
    int m_points;
    bool m_isReady;

};

#endif // PLAYER_H
