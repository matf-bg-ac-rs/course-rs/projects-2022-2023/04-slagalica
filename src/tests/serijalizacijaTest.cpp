#include "catch.hpp"
#include <QApplication>
#include <QMainWindow>
#include <QTcpSocket>
#include <QWidget>
#include <QColor>
#include <QPalette>
#include <iostream>
#include <string>

#include "../slagalica/najboljirezultati.h"
#include "../slagalica/winnerui.h"
#include "includes/jsonserializer.h"
#include "includes/gameslistmodel.h"
#include "includes/gamemodel.h"
#include "QDir"

TEST_CASE("WinnerUi", "[class]")
{
    SECTION("Poeni igraca 1 su veci od igraca 2 a objekat se odnosi na igraca 1 -> checkWinner Vraca Pobedu")
    {
        // arrange
        QString username1 = "Aleksa";
        QString username2 = "Ognjen";

        QMainWindow *m = new QMainWindow();
        QTcpSocket *socket = new QTcpSocket(m);

        int pointsPlayer1 = 90;
        int pointsPlayer2 = 72;

        int playerId = 1;
        auto outcomeExpected = WinnerUi::Outcomes::Win;


        // act
        auto winnerUi = WinnerUi(nullptr,
                 socket,
                 playerId,
                 username1,
                 username2,
                 pointsPlayer1,
                 pointsPlayer2);
        WinnerUi::Outcomes outcomeResult = winnerUi.checkWinner();

        // assert
        REQUIRE(outcomeExpected == outcomeResult);

    }
    SECTION("Poeni igraca 1 su veci od igraca 2 a objekat se odnosi na igraca 2 -> checkWinner Vraca Poraz")
    {
        // arrange
        QString username1 = "Aleksa";
        QString username2 = "Ognjen";

        QMainWindow *m = new QMainWindow();
        QTcpSocket *socket = new QTcpSocket(m);

        int pointsPlayer1 = 90;
        int pointsPlayer2 = 72;

        int playerId = 2;
        auto outcomeExpected = WinnerUi::Outcomes::Lose;


        // act
        auto winnerUi = WinnerUi(nullptr,
                 socket,
                 playerId,
                 username1,
                 username2,
                 pointsPlayer1,
                 pointsPlayer2);
        WinnerUi::Outcomes outcomeResult = winnerUi.checkWinner();

        // assert
        REQUIRE(outcomeExpected == outcomeResult);

    }

    SECTION("Poeni igraca 2 su veci od igraca 1 a objekat se odnosi na igraca 1 -> checkWinner Vraca Poraz")
    {
        // arrange
        QString username1 = "Aleksa";
        QString username2 = "Ognjen";

        QMainWindow *m = new QMainWindow();
        QTcpSocket *socket = new QTcpSocket(m);

        int pointsPlayer1 = 90;
        int pointsPlayer2 = 92;

        int playerId = 1;
        auto outcomeExpected = WinnerUi::Outcomes::Lose;


        // act
        auto winnerUi = WinnerUi(nullptr,
                 socket,
                 playerId,
                 username1,
                 username2,
                 pointsPlayer1,
                 pointsPlayer2);
        WinnerUi::Outcomes outcomeResult = winnerUi.checkWinner();

        // assert
        REQUIRE(outcomeExpected == outcomeResult);

    }
    SECTION("Poeni igraca 2 su veci od igraca 1 a objekat se odnosi na igraca 2 -> checkWinner Vraca Pobedu")
    {
        // arrange
        QString username1 = "Aleksa";
        QString username2 = "Ognjen";

        QMainWindow *m = new QMainWindow();
        QTcpSocket *socket = new QTcpSocket(m);

        int pointsPlayer1 = 90;
        int pointsPlayer2 = 92;

        int playerId = 2;

        auto outcomeExpected = WinnerUi::Outcomes::Win;


        // act
        auto winnerUi = WinnerUi(nullptr,
                 socket,
                 playerId,
                 username1,
                 username2,
                 pointsPlayer1,
                 pointsPlayer2);
        WinnerUi::Outcomes outcomeResult = winnerUi.checkWinner();

        // assert
        REQUIRE(outcomeExpected == outcomeResult);

    }

    SECTION("Poeni igraca 2 su jednaki sa poenima igracima 1 -> checkWinner Vraca Nereno")
    {
        // arrange
        QString username1 = "Aleksa";
        QString username2 = "Ognjen";

        QMainWindow *m = new QMainWindow();
        QTcpSocket *socket = new QTcpSocket(m);

        int pointsPlayer1 = 100;
        int pointsPlayer2 = 100;

        int playerId = 2;

        auto outcomeExpected = WinnerUi::Outcomes::Tie;


        // act
        auto winnerUi = WinnerUi(nullptr,
                 socket,
                 playerId,
                 username1,
                 username2,
                 pointsPlayer1,
                 pointsPlayer2);
        WinnerUi::Outcomes outcomeResult = winnerUi.checkWinner();

        // assert
        REQUIRE(outcomeExpected == outcomeResult);

    }
}

//TEST_CASE("NajboljiRezultati","[class]")
//{        QMainWindow *m = new QMainWindow();
//         QTcpSocket *socket = new QTcpSocket(m);
//         socket->connectToHost(QHostAddress::LocalHost, 8000);


//    SECTION("Server je pokrenut i games_data.json na serveru ima sacuvan makar jedan podatak -> broj redova u pogledu klase NajboljiRezultati > 0")
//    {
//        // arrange


//        bool connectedToServer = socket->waitForConnected(2000);
//        int playerId = -1;
//        NajboljiRezultati* najboljiRezultatiUi = nullptr;
//        int numberOfRows = -1;

//        // act
//        if (connectedToServer){
//            socket->waitForReadyRead();
//            playerId = socket->readAll().toInt();
//            najboljiRezultatiUi = new NajboljiRezultati(nullptr, socket, playerId);
//            socket->waitForReadyRead(3000);
//            numberOfRows = najboljiRezultatiUi->gamesModel()->rowCount();
//        }

//        socket->disconnectFromHost();

//        // assert
//        REQUIRE(connectedToServer);
//        REQUIRE(playerId != -1);
//        REQUIRE(najboljiRezultatiUi != nullptr);
//        REQUIRE(numberOfRows > 0);
//    }

//}

TEST_CASE("JsonSerializer", "[class]")
{
    Serializer* jsonSerializer = new JSONSerializer();
    SECTION("Ukoliko se objekat klase GamesListModel uspesno serijalizuje u neki fajl -> Iz istog fajla moze i da se deserijalizuje i da se dobije identican sadrzaj")
    {
        // arrange
        GamesListModel *gamesListModel = new GamesListModel();
        auto gamesStart = &gamesListModel->getGames();

        GameModel *game1 = new GameModel("Nikola", 1);
        GameModel *game2 = new GameModel("Aleksa", 3);

        GamesListModel *loadedGamesListModel = new GamesListModel();
        int lengthStart = -1;
        int lengthEnd = -1;
        bool condition = true;

        // act
        gamesStart->push_back(game1);
        gamesStart->push_back(game2);


        jsonSerializer->save(*gamesListModel, "./test_1.json");
        jsonSerializer->load(*loadedGamesListModel, "./test_1.json");

        lengthStart = gamesStart->size();
        lengthEnd = loadedGamesListModel->getGames().size();

        if (lengthStart != lengthEnd)
        {
            condition = false;
        }

        for (int i = 0; i < lengthStart; i++){
            auto usernameBegin = gamesListModel->getGames()[i]->getUsername();
            auto gamesWonBegin = gamesListModel->getGames()[i]->getGamesWon();

            auto usernameEnd= loadedGamesListModel->getGames()[i]->getUsername();
            auto gamesWonEnd= loadedGamesListModel->getGames()[i]->getGamesWon();

            if (usernameBegin != usernameEnd || gamesWonBegin != gamesWonEnd){
                condition = false;
                break;
            }
        }

        // assert
        REQUIRE(condition);
    }

    SECTION("Ukoliko se objekat klase GamesListModel uspesno serijalizuje u nisku -> Iz iste niske moze i da se deserijalizuje i da se dobije identican sadrzaj")
    {
        // arrange
        GamesListModel *gamesListModel = new GamesListModel();
        auto gamesStart = &gamesListModel->getGames();

        GameModel *game1 = new GameModel("Milan", 1);
        GameModel *game2 = new GameModel("Dragan", 3);
        GameModel *game3 = new GameModel("Vlado", 12);
        GameModel *game4 = new GameModel("Aleksa", 20);

        GamesListModel *loadedGamesListModel = new GamesListModel();
        int lengthStart = -1;
        int lengthEnd = -1;
        bool condition = true;

        // act
        gamesStart->push_back(game1);
        gamesStart->push_back(game2);
        gamesStart->push_back(game3);
        gamesStart->push_back(game4);


        auto serializedString = jsonSerializer->saveAsString(*gamesListModel);
        jsonSerializer->loadFromString(*loadedGamesListModel, serializedString);

        lengthStart = gamesStart->size();
        lengthEnd = loadedGamesListModel->getGames().size();

        if (lengthStart != lengthEnd)
        {
            condition = false;
        }

        for (int i = 0; i < lengthStart; i++){
            auto usernameBegin = gamesListModel->getGames()[i]->getUsername();
            auto gamesWonBegin = gamesListModel->getGames()[i]->getGamesWon();

            auto usernameEnd= loadedGamesListModel->getGames()[i]->getUsername();
            auto gamesWonEnd= loadedGamesListModel->getGames()[i]->getGamesWon();

            if (usernameBegin != usernameEnd || gamesWonBegin != gamesWonEnd){
                condition = false;
                break;
            }
        }

        // assert
        REQUIRE(condition);
    }
}







