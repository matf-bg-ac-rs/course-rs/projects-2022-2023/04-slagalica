QT       += core gui
QT       += network
QT       -= qui
QT       += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    KoznaznaTest.cpp \
    asocijacijeTest.cpp \
    main.cpp \
    serijalizacijaTest.cpp \
    serverTest.cpp \
    spojniceTest.cpp \
    test.cpp \
    ../slagalica/asocijacijeui.cpp \
    ../slagalica/asocijacije.cpp \
    ../slagalica/spojniceui.cpp \
    ../slagalica/spojnice.cpp \
    ../slagalica/koznaznaui.cpp \
    ../slagalica/koznazna.cpp \
    ../slagalica/najboljirezultati.cpp \
    ../slagalica/model.cpp \
    ../slagalica/modelitem.cpp \
    ../slagalica/winnerui.cpp \
    ../server/server.cpp \
    ../server/player.cpp \



HEADERS += \
    ../slagalica/asocijacijeui.h \
    ../slagalica/asocijacije.h \
    ../slagalica/spojnice.h \
    ../slagalica/spojniceui.h \
    ../slagalica/koznaznaui.h \
    ../slagalica/najboljirezultati.h \
    ../slagalica/model.h \
    ../slagalica/modelitem.h \
    ../slagalica/winnerui.h \
    ../server/server.h \
    ../server/player.h\




FORMS += \
    ../slagalica/asocijacijeui.ui \
    ../slagalica/spojniceui.ui \
    ../slagalica/koznaznaui.ui \
#    ../slagalica/skockoui.ui \
    ../slagalica/najboljirezultati.ui \
    ../slagalica/winnerui.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target



win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../serialization/release/ -lserialization
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../serialization/debug/ -lserialization
else:unix: LIBS += -L$$OUT_PWD/../serialization/ -lserialization

INCLUDEPATH += $$PWD/../serialization
DEPENDPATH += $$PWD/../serialization
