#include "catch.hpp"
#include <QApplication>
#include <QMainWindow>
#include <QTcpSocket>

#include "../slagalica/spojniceui.h"
#include "ui_spojniceui.h"


TEST_CASE("SpojniceUi","[class]")
{

    QMainWindow *m = new QMainWindow();
    QTcpSocket *socket = new QTcpSocket(m);
    socket->connectToHost(QHostAddress::LocalHost, 8000);
    SpojniceUi *spojnice = new SpojniceUi(m, socket, 0, "", "", 0, 0, 0);



    SECTION("Konstruktor SpojniceUi"){

           SpojniceUi *spojnice = new SpojniceUi(m, socket, 0, "", "", 0, 30, 20);


           REQUIRE(spojnice->playerPoints1()==30);

           REQUIRE(spojnice->playerPoints2()==20);

           REQUIRE(spojnice->id()==0);



    }

    SECTION("Testiranje tajmera")
    {

        spojnice->setTime(1);

        emit spojnice->updateTime();

        REQUIRE(spojnice->time()==0);
    }

    SECTION("Testiranje polja")
    {
        QString data="g_Indija_Iran_Japan_Katar_Kina_Kirgistan_Tajland_Vijetnam_Hanoj_Tokio_Doha_Nju Delhi_Biskek_Bangkok_Peking_Teheran";
        QStringList datasplit= data.split("_");
        spojnice->showFields(datasplit);

        REQUIRE(spojnice->getUi()->lbLhs1->text()=="Indija");


    }

    SECTION("Testiranje dugmica")
    {
        spojnice->addButtonsAndLabelsToMaps();
        bool disable1=spojnice->getUi()->pbRhs1->isEnabled();

        spojnice->enableAllRemainingButtons();

        REQUIRE(disable1!=spojnice->getUi()->pbRhs1->isEnabled());
    }



}











