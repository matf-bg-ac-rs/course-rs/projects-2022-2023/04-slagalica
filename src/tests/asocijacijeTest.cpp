
#include "catch.hpp"
#include <QApplication>
#include <QMainWindow>
#include <QTcpSocket>
#include <QWidget>
#include <QColor>
#include <QPalette>
#include <iostream>
#include <string>

#include "../slagalica/asocijacijeui.h"
#include "ui_asocijacijeui.h"


TEST_CASE("Asocijacijeui","[class]")
{
    QMainWindow *m = new QMainWindow();
    QTcpSocket *socket = new QTcpSocket(m);
    socket->connectToHost(QHostAddress::LocalHost, 8000);
    AsocijacijeUi *asocijacije = new AsocijacijeUi(m, socket, 0, 0, 0, 0);
    Asocijacije *asocijacije1= new Asocijacije();


    SECTION("Konstruktor asocijacijeui"){
        AsocijacijeUi *asocijacije1 = new AsocijacijeUi(m, socket, 0, "Ognjen","Nikola", 0, 0,0);

        REQUIRE(asocijacije1->id()==0);
        REQUIRE(asocijacije1->brPoteza()==0);
        REQUIRE(asocijacije1->buttonsRemaining()==16);
        REQUIRE(asocijacije1->clientName1()=="Ognjen");
        REQUIRE(asocijacije1->clientName2()=="Nikola");
        delete asocijacije1;


    }

    SECTION("Azuriranje vremena")
    {

        // Arrange
        asocijacije->setTime(1);

        // Act
        emit asocijacije->updateTime();


        // Assert
        REQUIRE(asocijacije->time()==0);

    }

    SECTION("Promena poteza")
    {
        // Arrange
        asocijacije->setTurn(true);
        asocijacije->setBrPoteza(0);
        // Act
        asocijacije->changeTurn();
        // Assert
        REQUIRE(asocijacije->time()==10);
        REQUIRE(asocijacije->brPoteza()==1);
        REQUIRE(asocijacije->turn()==false);

    }

    SECTION("Bojenje teksta widget-a u zuto"){

        // Arrange
        QWidget *w= new QWidget(m);

        // Act
        asocijacije->setColorYellow(w);

        // Assert
        REQUIRE(w->styleSheet()==":disabled{color: yellow;}");


    }


    SECTION("Prikazivanje pokusaja ako je tacan"){

        QString pokusaj = "u_B_Crvena_c_5_0_osa_osa_osa_osa";
        QStringList pokusajSplit = pokusaj.split('_');
        int time= asocijacije->time();
        asocijacije->showAttempt(pokusajSplit);
        asocijacije->getUi()->pb_B1->text();
        REQUIRE(asocijacije->getUi()->pb_B1->text()=="osa");
        REQUIRE(asocijacije->getUi()->pb_B2->text()=="osa");
        REQUIRE(asocijacije->getUi()->pb_B3->text()=="osa");
        REQUIRE(asocijacije->getUi()->pb_B4->text()=="osa");

        REQUIRE(asocijacije->time()==time+5);
        REQUIRE(asocijacije->getUi()->leTimer->text()==QString::number(asocijacije->time()));


    }

    SECTION("Azuriranje poena"){

        QString pokusaj = "u_B_Crvena_c_5_0_osa_osa_osa_osa";
        QStringList pokusajSplit = pokusaj.split('_');
        int scorePlayer1 = asocijacije->getUi()->lcdPlayer1->intValue();
        int scorePlayer2 = asocijacije->getUi()->lcdPlayer2->intValue();

        asocijacije->updatePoints(pokusajSplit);

        int scorePlayer1After = asocijacije->getUi()->lcdPlayer1->intValue();
        int scorePlayer2After = asocijacije->getUi()->lcdPlayer2->intValue();

        REQUIRE(scorePlayer1After==scorePlayer1+5);
        REQUIRE(scorePlayer2After==scorePlayer2);


    }

    SECTION("Onemogucavanje dugmeta"){

        asocijacije->disableButtonForever(asocijacije->getUi()->pb_A1);

        REQUIRE(asocijacije->enableButtonsVector()[asocijacije->getUi()->pb_A1]==0);

    }

    SECTION("Onemogucavanje line edita"){

        asocijacije->disableLineEditForever(asocijacije->getUi()->le_A);

        REQUIRE(asocijacije->enableLineEditVector()[asocijacije->getUi()->le_A]==0);

    }

    SECTION("Raspolozive kolone za polaganje"){

        bool available1 = asocijacije->availableGuesses();

        asocijacije->enableLineEditVector()[asocijacije->getUi()->le_A] = 1;


        bool available2 = asocijacije->availableGuesses();

        REQUIRE(available1 == false);
        REQUIRE(available2 == true);

    }
    SECTION("Testiranje columnToString"){

        QString str=asocijacije1->columnToString(Column::A);

        REQUIRE(str=="A");

    }


    delete asocijacije;
    delete socket;
    delete m;

}









