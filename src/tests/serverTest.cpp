#include "catch.hpp"
#include "../server/server.h"
#include <random>



TEST_CASE("Server test","[class]")
{


    SECTION("generateSkockoCombination")
    {
        QObject *o = new QObject();
        Server *server = new Server(o);

        QVector<QChar> skockoCombination = server->generateSkockoCombination();

        REQUIRE(skockoCombination.length() == 4);

        QString allSymbols = "Stpskz";
        bool allCorrectSymbols = true;
        for (int i = 0; i < 4; i++)
        {
            if (!allSymbols.contains(skockoCombination[i]))
            {
                allCorrectSymbols = false;
                break;
            }
        }
        REQUIRE(allCorrectSymbols);
    }

    SECTION("calculateSkockoPoints")
    {
        QObject *o = new QObject();
        Server *server = new Server(o);

        for (int numberOfReds = 0; numberOfReds < 5; numberOfReds++)
        {
            for (int attemptNumber = 0; attemptNumber < 8; attemptNumber++)
            {
                QString points = server->calculateSkockoPoints(QString::number(attemptNumber), QString::number(numberOfReds));

                if (numberOfReds != 4)
                    REQUIRE(points == "0");
                else
                {
                    if (QString::number(attemptNumber) == "6")
                        REQUIRE(points == "10");
                    else if (QString::number(attemptNumber) == "5")
                        REQUIRE(points == "15");
                    else
                        REQUIRE(points == "20");
                }
            }
        }
    }

    SECTION("checkSkockoAttempt")
    {
        QObject *o = new QObject();
        Server *server = new Server(o);

        QVector<QChar> randomSkockoCombination = server->generateSkockoCombination();
        //Stpskz

        QVector<QChar> currentSkockoGuess = QVector<QChar>(4);
        for (int i1 = 0; i1 < 6; i1++)
        {
            if (i1 == 0)
                currentSkockoGuess[0] = 'S';
            if (i1 == 1)
                currentSkockoGuess[0] = 't';
            if (i1 == 2)
                currentSkockoGuess[0] = 'p';
            if (i1 == 3)
                currentSkockoGuess[0] = 's';
            if (i1 == 4)
                currentSkockoGuess[0] = 'k';
            if (i1 == 5)
                currentSkockoGuess[0] = 'z';

            for (int i2 = 0; i2 < 6; i2++)
            {
                if (i2 == 0)
                    currentSkockoGuess[1] = 'S';
                if (i2 == 1)
                    currentSkockoGuess[1] = 't';
                if (i2 == 2)
                    currentSkockoGuess[1] = 'p';
                if (i2 == 3)
                    currentSkockoGuess[1] = 's';
                if (i2 == 4)
                    currentSkockoGuess[1] = 'k';
                if (i2 == 5)
                    currentSkockoGuess[1] = 'z';

                for (int i3 = 0; i3 < 6; i3++)
                {
                    if (i3 == 0)
                        currentSkockoGuess[2] = 'S';
                    if (i3 == 1)
                        currentSkockoGuess[2] = 't';
                    if (i3 == 2)
                        currentSkockoGuess[2] = 'p';
                    if (i3 == 3)
                        currentSkockoGuess[2] = 's';
                    if (i3 == 4)
                        currentSkockoGuess[2] = 'k';
                    if (i3 == 5)
                        currentSkockoGuess[2] = 'z';

                    for (int i4 = 0; i4 < 6; i4++)
                    {
                        if (i4 == 0)
                            currentSkockoGuess[3] = 'S';
                        if (i4 == 1)
                            currentSkockoGuess[3] = 't';
                        if (i4 == 2)
                            currentSkockoGuess[3] = 'p';
                        if (i4 == 3)
                            currentSkockoGuess[3] = 's';
                        if (i4 == 4)
                            currentSkockoGuess[3] = 'k';
                        if (i4 == 5)
                            currentSkockoGuess[3] = 'z';

                        QString returnString = server->checkSkockoAttempt(randomSkockoCombination, currentSkockoGuess);
                        REQUIRE(returnString.length() == 2);
                        int redSymbols = returnString[0].digitValue();
                        int yellowSymbols = returnString[1].digitValue();
                        REQUIRE((redSymbols >= 0 && redSymbols <= 4 && yellowSymbols >= 0 && yellowSymbols <= 4 && (redSymbols+yellowSymbols) >= 0 and (redSymbols+yellowSymbols) <= 4));
                        if (randomSkockoCombination == currentSkockoGuess)
                            REQUIRE((redSymbols == 4 && yellowSymbols == 0));
                        if (randomSkockoCombination != currentSkockoGuess)
                            REQUIRE((redSymbols != 4));
                    }
                }
            }
        }
    }
    SECTION("Constructor"){
        QObject *o = new QObject();
        Server *s = new Server(o);
        REQUIRE(s!=nullptr);
    }
    SECTION("generateKoZnaZna"){
        QObject *o = new QObject();
        Server *s = new Server(o);
        int random_number= rand() % 100;
        QVector<int> num=s->generateKoZnaZnaCombination(random_number);
        REQUIRE(num.size()==10);
        bool razliciti=false;
        for(int i = 0;i<num.size();i++){
            for (int j =0;j<num.size();j++){
                if (num[i]==num[j] && i!=j){
                    razliciti=true;
                }
            }
            REQUIRE(num[i]<random_number);
        }

        REQUIRE(razliciti==false);
    }
    SECTION("generateTwoRandomInt"){
        QObject *o = new QObject();
        Server*s = new Server(o);
        int random_number= rand() % 100;

        QVector<int> num=s->generate_two_ints(random_number);
        REQUIRE(num.size()==2);
        REQUIRE(num[0] != num[1]);
    }
    SECTION("socketDisconnect"){
        QObject *o = new QObject();
        Server *s = new Server(o);
        QTcpSocket *socket = new QTcpSocket();
        s->setPlayerSocket(1,socket);
        s->socketDisconnected();

        REQUIRE(s->getPlayerSocket(1)==nullptr);

    }
    SECTION("playerConstructor"){

         Player *p= new Player();
         REQUIRE(p!=nullptr);
         REQUIRE(p->m_isReady ==false);
//         REQUIRE(p->m_points == 0);
    }
    SECTION("spojniceShuffle"){
        QObject *o = new QObject();
        Server *s= new Server(o);

        s->shuffleSpojnice();
        QVector<int> comb1=s->m_spojniceShuffle.at(0);
        QVector<int> comb2=s->m_spojniceShuffle.at(1);


        REQUIRE(s->m_spojniceShuffle.size()==2);
        REQUIRE(comb1.size()==8);

        REQUIRE(comb2.size()==8);
        bool is_true1=false;
        bool is_true2=false;
        for(int i=0;i<8;i++){
           for (int j = 0;j<8;j++){
               if (comb1[i]==comb1[j] && i!=j ){
                   is_true1=true;
               }
               if (comb2[i]==comb2[j] && i!=j ){
                   is_true2=true;
               }
           }
           REQUIRE(comb1[i] < 9);
           REQUIRE(comb2[i] < 9);
        }
        REQUIRE(is_true1==false);
        REQUIRE(is_true2==false);
    }

//    SECTION("readKoZnaZna"){
//        QObject *o = new QObject();
//        Server *s = new Server(o);
//        s->readKoZnaZna();
//        REQUIRE(true);

//       }

    SECTION("checkKoZnaZnaAttempt")
    {
        QObject* o = new QObject();
        Server* server = new Server(o);

        server->generateKoZnaZnaCombination(10);
        for (int questionNumber = 0; questionNumber < 10; questionNumber++)
        {
            for (int answerNumber = 0; answerNumber < 5; answerNumber++)
            {
                int returnValue = server->checkKoZnaZnaAttempt(questionNumber, answerNumber, 0);

                if (server->m_koZnaZnaAnswersMap[questionNumber] == answerNumber)
                    REQUIRE(returnValue == 0);
                else if (answerNumber < 0 || answerNumber >= 4)
                    REQUIRE(returnValue == -2);
                else
                    REQUIRE(returnValue == -1);
            }
        }
    }

    SECTION("calculateAssociationPoints")
    {
        QObject* o = new QObject();
        Server* server = new Server(o);
        server->startServer();

        server->m_associationPoints = QVector<QMap<QChar, int>>();
        QMap<QChar, int> associationPoints = QMap<QChar,int>();
        associationPoints['A'] = 6;
        associationPoints['B'] = 6;
        associationPoints['C'] = 6;
        associationPoints['D'] = 6;
        associationPoints['K'] = 5;
        server->m_associationPoints.append(associationPoints);
        server->m_associationPoints.append(associationPoints);

        for (int associationNumber = 0; associationNumber < 2; associationNumber++)
        {
            for (int i = 0; i < 5; i++)
            {
                QChar letter = "ABCDK"[i];
                for (int attemptFlag = 0; attemptFlag < 2; attemptFlag++)
                {
                    for (int correctFlag = 0; correctFlag < 2; correctFlag++)
                    {
                        int associationPoints = server->calculateAssociationPoints(associationNumber, letter, attemptFlag, correctFlag);

                        if (!attemptFlag)
                            REQUIRE(associationPoints == 0);
                        else if (!correctFlag)
                            REQUIRE(associationPoints == 0);
                        else
                            REQUIRE((associationPoints >= 0 && associationPoints <= 30));
                    }
                }
            }
        }
    }

    SECTION("associationToString")
    {
        QObject* o = new QObject();
        Server* server = new Server(o);

        server->m_columnsFields = QVector<QMap<QChar, QVector<QString>>>();
        server->m_columnsResults = QVector<QMap<QChar, QString>>();

        QMap<QChar, QVector<QString>> columnsFields1 = QMap<QChar, QVector<QString>>();
        QMap<QChar, QVector<QString>> columnsFields2 = QMap<QChar, QVector<QString>>();

        QVector<QString> columnA = {"vujadin", "grad", "mladi", "vek"};
        QVector<QString> columnB = {"divlji", "evropa", "morava", "w"};
        QVector<QString> columnC = {"bliski" ,"e", "pitanje", "azija"};
        QVector<QString> columnD = {"muzika", "skupstina", "heroj", "seoba"};
        columnsFields1['A'].append(columnA);
        columnsFields1['B'].append(columnB);
        columnsFields1['C'].append(columnC);
        columnsFields1['D'].append(columnD);

        columnA = {"boja", "osa", "mrlja", "groznica"};
        columnB = {"dvor", "voznja", "nasta", "noc"};
        columnC = {"drug", "japan", "pomoc", "utakmica"};
        columnD = {"doktor", "praksa", "pregled", "izabrani"};
        columnsFields2['A'].append(columnA);
        columnsFields2['B'].append(columnB);
        columnsFields2['C'].append(columnC);
        columnsFields2['D'].append(columnD);

        server->m_columnsFields.append(columnsFields1);
        server->m_columnsFields.append(columnsFields2);

        QMap<QChar, QString> columnsResults1 = QMap<QChar, QString>();
        QMap<QChar, QString> columnsResults2 = QMap<QChar, QString>();

        columnsResults1['A'] = "Stari";
        columnsResults1['B'] = "Zapad";
        columnsResults1['C'] = "Istok";
        columnsResults1['D'] = "Narod";
        columnsResults1['K'] = "Sloveni";

        columnsResults2['A'] = "Zuta";
        columnsResults2['B'] = "Luda";
        columnsResults2['C'] = "Prijatelj";
        columnsResults2['D'] = "Lekar";
        columnsResults2['K'] = "Kuca";

        server->m_columnsResults.append(columnsResults1);
        server->m_columnsResults.append(columnsResults2);

        server->associationToString();

        REQUIRE(server->m_associationAsAString.length() == 2);
        for (int i = 0; i < 2; i++)
        {
            QStringList splitAssociationString = server->m_associationAsAString[i].split("_");
            REQUIRE(splitAssociationString.length() == 21);
        }
    }
}
