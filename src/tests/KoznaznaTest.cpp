#include "catch.hpp"
#include <QApplication>
#include <QMainWindow>
#include <QTcpSocket>
#include <QWidget>

#include "../slagalica/koznaznaui.h"
#include "ui_koznaznaui.h"



TEST_CASE("Koznaznaui","[class]")
{
    QMainWindow *m = new QMainWindow();
    QTcpSocket *socket = new QTcpSocket(m);
    socket->connectToHost(QHostAddress::LocalHost, 8000);
    Koznaznaui *koznazna = new Koznaznaui(m, socket, 0, 0, 0, 0);


    SECTION("Konstruktor koznaznaui"){

        Koznaznaui *koznazna1 = new Koznaznaui(m, socket, 0, "Ognjen","Nikola", 3, 0,0);

        REQUIRE(koznazna1->id()==0);

        REQUIRE(koznazna1->clientName1()=="Ognjen");
        REQUIRE(koznazna1->clientName2()=="Nikola");
        REQUIRE(koznazna1->time()==10);
        REQUIRE(koznazna1->timeMs()==0);
        REQUIRE(koznazna1->questionCounter()==3);


    }

    SECTION("Azuriranje vremena"){

        koznazna->setTime(10);

        koznazna->updateTime();

        REQUIRE(koznazna->time()==9);
    }

    SECTION("Azuriranje vremena u milisekundama"){

        koznazna->setTimeMs(100);

        koznazna->updateTimeMs();

        REQUIRE(koznazna->timeMs()==101);
    }

    SECTION("Prikazivanje pitanja"){
        QString pitanje = "Koje je boje nebo?_Crvene_Plave_Zute_Zelene";
        koznazna->displayQuestion(pitanje);
        REQUIRE(koznazna->getUi()->le_question->text()=="Koje je boje nebo?");
        REQUIRE(koznazna->getUi()->pb_A->text()=="Crvene");
        REQUIRE(koznazna->getUi()->pb_B->text()=="Plave");
        REQUIRE(koznazna->getUi()->pb_C->text()=="Zute");
        REQUIRE(koznazna->getUi()->pb_D->text()=="Zelene");
        REQUIRE(koznazna->time()==10);
        REQUIRE(koznazna->timeMs()==0);

    }

    SECTION("Testiranje dugmica")
    {
        koznazna->on_timesUp();
        bool state=false;
        if(koznazna->getUi()->pb_A->isEnabled() || koznazna->getUi()->pb_B->isEnabled() || koznazna->getUi()->pb_C->isEnabled() ||koznazna->getUi()->pb_D->isEnabled() || koznazna->getUi()->pb_Next->isEnabled())
            state=true;

        REQUIRE(state==false);



    }
    delete koznazna;
    delete socket;
    delete m;

}
