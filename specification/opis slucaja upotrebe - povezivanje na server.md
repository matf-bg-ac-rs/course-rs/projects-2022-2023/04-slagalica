# Slučaj upotrebe: Povezivanje na server
## Kratak opis:
 Pri pokretanju klijentskog dela aplikacije, salje se TCP zahtev za povezivanje na server. Posle uspesnog povezivanja prikazuje se glavni meni.
## Akteri:
  Klijenti
## Preduslovi:
 /
## Postuslovi:
 Klijentska aplikacija je povezana na server i prikazan je glavni meni aplikacije.
## Osnovni tok:
```
1. Prilikom pokretanja klijentske aplikacije klijent bira username.
2. Klijentska aplikacija salje TCP zahtev serveru radi uspostavljanja konekcije.
3. Server prihvata zahtev za uspostavljanje konekcije i klijentu su dodeljuje soba.
4. Uspostavlja se TCP konekcija izmedju servera i klijenta i otvara se glavni meni.

```
### Alternativni tokovi:

```
A1 – Nemogucnost ostvarivanja veze sa serverom. Prikazuje se poruka o neuspesnom povezivanju i dugme za ponovni pokusaj ostvarivanja konekcije.

```

### Specijalni zahtevi:
 Klijenti moraju imati pristup internetu. Server aplikacije mora biti pokrenut i u stanju izvrsavanja u trenutku zahtevanja konekcije.

### Dodatne informacije:
```
/

```
