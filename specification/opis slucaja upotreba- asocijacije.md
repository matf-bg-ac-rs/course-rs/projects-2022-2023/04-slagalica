# Slučaj upotrebe: Asocijacije

## Kratak opis:
Poslednja igra u slagalicli, nakon 'Spojnica', dolazi igra 'Asocijacije' . U igri 'Asocijacije' igraci otvaraju polja u kolonama i pokusavaju da pogode resenja kolona i konacno resenje .

## Akteri: 
Dva igraca.

## Preduslovi: 
Zavrsena je igra 'Spojnice'.

## Postuslovi: 
/

## Osnovni tok:

    1.Aplikacija ucitava poene osvojene iz prethodnih igra.
    2.Prvi igrac je na potezu, prva runda asocijacije.
    3. Sve dok igracu koji je na potezu ne istekne vreme ili dok nije pogodjeno konacno resenje ili dok se ne ispuni maksimalni broj pokusaja, ponavljaju se naredni koraci:
        3.1. Igrac koji je na potezu otvara polje koje prethodno nije otvoreno  
        3.2  Igrac pokusava da pogodi neko od resenja (konacno ili kolona) 
        3.3. Ukoliko je tacno pogodio resenje 1 kolone potrebno je uvecati poene i 	restartovati vreme poteza:
		    3.3.1. Aplikacija uvecava ukupne poene. 
		    3.3.2. Aplikacija obevestava igraca koji nije na potezu o tekucem stanju.
		    3.3.3. Aplikacija restartuje vreme potrebno za potez.
		    3.3.4. Aplikacija se vraca na korak 3.2.
	    3.4. Ukoliko nije tacno pogodio resenje potrebno je obavestiti igrace:
		    3.4.1. Aplikacija obevestava oba igraca o tekucem stanju.
		    3.4.2. Menjamo igrasa koji je na potezu. 
		    3.4.3 Aplikacija se vraca na korak 3.1.
    4. Ukoliko drugi igrac nije zapoceo asocijaciju:
        4.1 Drugi igrac je na potezu, druga runda asocijaciju.
        4.2.Vracamo se na korak 3.
    5. Prelazi se na slucaj upotrebe 'Cuvanje rezultata'.
    
### Alternativni tokovi: 

    A1 - Neocekivani izlaz iz aplikacije. Ukoliko u bilo kojem koraku korisnik isključi aplikaciju,
    sve zapamćene informacije se poništavaju i aplikacija završava rad.
    Slučaj upotrebe se završava.

    A2 - Neočekivani izlaz protivnika iz aplikacije. Ukoliko u bilo kojem koraku protivnik isključi
    aplikaciju, sve zapamćene informacije se poništavaju i aplikacija prikazuje glavni meni.
    Slučaj upotrebe se završava.


Podtokovi:
```
 /
 ```

### Specijalni zahtevi: 
/

### Dodatne informacije:

    DI1. Vreme za igranje jednog poteza je 30 sekundi.
    DI2. Broj poena koji se dobija za tacno pogodjenu kolonu zavisi broja polja otvorenih u toj koloni. 
            Svaka kolona ima 4 polja -> Za 1 polje otvoreno 5 poena. Za 2 polja otvorenih 4 poena, 3 polja 3 poena, 4 polja 2 poena.
    DI3. Ako se pogodi konacno resenje poeni se racunaju po sledecem kirterijumu - > 5 poena za konacno resenja i za kolone koje nisu pogodjene racuna se po DI2. Ako postoji kolona u kojoj nije
        otvoreno nijedno polje, za nju se dobija 6.
    DI4. Ne moze se pogadjiati resenje kolone ako nijedno polje nije otvoreno i new moze se pogadjati konacno resenje ako nije pogodjeno nijedno resenje kolone.                                                   


