# Slučaj upotrebe: Početak partije

## Kratak opis: 
Igrač započinje jednu partiju igre iz glavnog menija aplikacije.
Aplikacija započinje partiju. Nakon odigrane partije, aplikacija čuva informacije
o odigranoj partiji.

## Akteri: 
Igrači 

## Preduslovi: 
Aplikacija je pokrenuta i prikazuje glavni meni.

## Postuslovi: 
Čuvaju se rezultati partije.

## Osnovni tok:

	1.Igrač bira dugme Započni igru iz glavnog menija.
	2.Aplikacija dohvata informacije o podešavanjima partije.
	3.Aplikacija konstruiše novu partiju i započinje je.
	4.Prelazi se na slučaj upotrebe "Skočko".Po završetku prelazi se na korak 5.
	5.Prelazi se na slučaj upotrebe Ko zna zna.Po završetku prelazi se na korak 6.
	6.Prelazi se na slučaj upotrebe Spojnice.Po završetku prelazi se na korak 7.
	7.Prelazi se na slučaj upotrebe Asocijacije. Po završetku prelazi se na korak 8.
	8.Nakon odigranih svih igara aplikacija prikazuje konačan rezultat.
	9.Prelazi se na slučaj upotrebe Čuvanje rezultata. Po završetku prelazi se na korak 10.
	10.Aplikacija prikazuje galvni meni.

### Alternativni tokovi:
A1: Neocekivani izlaz iz aplikacije. Ukoliko u bilo kojem koraku korisnik isključi aplikaciju,
sve zapamćene informacije se poništavaju i aplikacija završava rad.
Slučaj upotrebe se završava.

A2: Neočekivani izlaz protivnika iz aplikacije. Ukoliko u bilo kojem koraku protivnik isključi
aplikaciju, sve zapamćene informacije se poništavaju i aplikacija prikazuje glavni meni.
Slučaj upotrebe se završava.

Podtokovi: 
```
/
```

### Specijalni zahtevi: 
Server aplikacije mora biti pokrenut u trenutku pokretanja partije.

### Dodatne informacije: 
Tokom trajanja partije, aplikacija prati informacije o osvojenim poenima igrača.




