# Slučaj upotrebe: Prikazivanje najboljih rezultata

## Kratak opis: 
Igrač iz glavnog menija bira opciju za prikazikvanje najboljih rezultata. Aplikacija prikazuje najbolje rezultate na ekranu.

## Akteri: 
Igrač - igra jednu partiju igre.

## Preduslovi: 
Aplikacija je pokrenuta i prikazuje glavni meni.

## Postuslovi: 
Informacije o najboljim rezultatima su ispisane na ekranu.

## Osnovni tok:

	1. Igrač bira dugme "Prikazivanje najboljih rezultata" iz glavnog menija.

	2. Aplikacija ispisuje rezultat u obliku liste, gde su igrači rangirani po bodovima.

	3. Igrač bira dugme za povratak u glavni meni.

	4. Aplikacija prikazuje glavni meni.

### Alternativni tokovi: 
A1: Neočekivani izlazak iz aplikacije. Ukoliko u bilo kojem koraku korisnik isključi aplikaciju, sve informacije o rezultatima ostaju upamćene. Aplikacija završava rad.
Slučaj upotrebe se završava.

### Podtokovi: 
/

### Specijalni uslovi: 
/

### Dodatne informacije: 
/
