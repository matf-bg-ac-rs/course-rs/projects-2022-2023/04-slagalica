﻿# Slučaj upotrebe: Podešavanje jedne partije igre

## Kratak opis: 
Oba igrača podešavaju svoje ime i zvuk. 

## Akteri: 
Dva igrača.

## Preduslovi: 
Aplikacija je pokrenuta i prikazuje se glavni meni.

## Osnovni tok: 

		1.Igrač klikom na dugme “Zvuk” može da ugasi pozadinsku muziku. 
		
### Alternativni tokovi:  
A1. Izlazak iz aplikacije. Ukoliko igrač izađe iz aplikacije njegova podešavanja se gube. 

Podtokovi: 
```
/
```


### Specijalni zahtevi: 
Server aplikacije mora biti pokrenut u trenutku podešavanja partije.

### Dodatne informacije: 
/

