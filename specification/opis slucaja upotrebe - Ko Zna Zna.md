# Slučaj upotrebe: Ko Zna Zna
## Kratak opis:
 Aplikacija zapocinje igru Ko zna zna. U okviru igre Ko zna zna se postavlja 10 pitanja igracima, na koja igraci 	odgovaraju i tacnim odgovorima skupljalju poene.
## Akteri:
 Igraci
## Preduslovi:
 Partija Slagalice je u toku i zavrsen je prethodni slucaj upotrebe Skocko.
## Postuslovi:
 Rezultati igre su sacuvani i prelazi se na slucaj upotrebe Spojnice.
## Osnovni tok:
```
1. Sve dok se ne postavi 10 pitanja, ponavljaju se sledeci koraci:
    1.1. Aplikacija postavlja pitanje igracima
    1.2. Aplikacija postavlja tajmer za pitanje na 10 sekundi
    1.3. Igraci imaju priliku do kraja isteka vremena na tajmeru da odgovore na pitanje
    1.4. Aplikacija dodeljuje poene u odnosu na odgovore igraca
        1.4.1. Ukoliko su oba igraca odgovorila tacno na pitanje, poeni se dodeljuju brzem
        1.4.2. Ukoliko je samo jedan igrac odgovorio tacno  na pitanje, dodeljuju mu se poeni
        1.4.3. Ukoliko su neki ili oba igraca dali pogresan odgovor, onda im se oduzimaju poeni
    1.5. Aplikacija prikazuje tacan odgovor
    1.6. Aplikacija azurira trenutne poene
2. Prelazi se na slucaj upotrebe Spojnice

```
### Alternativni tokovi:

```
A1 - Neocekivani izlaz iz aplikacije. Ukoliko u bilo kojem koraku korisnik isključi aplikaciju,
sve zapamćene informacije se poništavaju i aplikacija završava rad.
Slučaj upotrebe se završava.

A2 - Neočekivani izlaz protivnika iz aplikacije. Ukoliko u bilo kojem koraku protivnik isključi
aplikaciju, sve zapamćene informacije se poništavaju i aplikacija prikazuje glavni meni.
Slučaj upotrebe se završava.

```

### Specijalni zahtevi:
 Klijenti moraju imati pristup internetu. Server aplikacije mora biti pokrenut i u stanju izvrsavanja tokom trajanja partije.

### Dodatne informacije:
```
 DI1 – Broj poena koji se dodeljuje igracima pri tacnom odgororu je 10 poena.
 
 DI2 – Broj poena koji se oduzima od igraca pri netacnom odgovoru je 5 poena.

```
