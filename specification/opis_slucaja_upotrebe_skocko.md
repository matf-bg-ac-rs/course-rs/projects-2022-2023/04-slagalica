﻿# Slučaj upotrebe: Skočko
## Kratak opis
Igrač iz skupa podataka od šest simbola treba da pogodi
nasumično generisanu sekvencu dužine 5 iz 6 pokušaja, gde posle svakog pokušaja
dobija informaciju o broju tačno pogođenih na mestu i broju tačno pogođenih
koji nisu na mestu u traženoj sekvenci.

## Akteri:
- Igrač 1 - Igra jednu rundu skočka
- Igrač 2 -Prati poteze drugog igrača dok je on na potezu

## Preduslovi
Partija je pokrenuta i otvoren je prozor za igru skočko

## Postuslovi
Nakon što oba igrača završe igru skočko, prelazi se na igru Ko zna zna
igru i podaci o zarađenim poenima se ažuriraju.

## Osnovni tok:
```
1. Aplikacija daje prvom igraču pravo poteza
2. Aplikacija postavlja broj pokušaja igraču koji je na potezu na 6
3. Aplikacija kreira nasumučnu sekvencu koja igrač treba da pogodi
4. Aplikacija konstruiše novi tajmer i započinje odbrojavanje
5. Sve dok vreme ne istekne
	5.1. Sve dok igrač ima broj poteza veći od nula
		5.1.1. Aplikacija zaključava polje za submitovanje pokušaja
		5.1.2. Server prosleđuje aplikaciji preostalo vreme
		5.1.3. Aplikacija provera da li je igrač kliknuo na neki od simbola u listi simbola, ili na neki od simbola u sekvenci
			5.1.3.1. Ukoliko je igrač kliknuo na neki od simbola u listi simbola, taj simbol
				se dodaje na prvo slobodno mesto u igračevoj sekvenci
			5.1.3.2 Ukoliko je igrač kliknuo na neki simbol iz svoje sekvence, taj simbol se uklanja  iz te sekvence
		5.1.4. Aplikacija proverava da li je igrač popunio svih pet polja
			5.1.4.1. Ukoliko je popunio, dugme za submitovanje pokušaja se otključava
				5.1.4.1.1. Ukoliko je korisnik kliknuo na dugme za submitovanje
					5.1.4.1.1.1. Aplikacija prikazuje broj tačnih simbola koji jesu na svom mestu kao i broj tačnih simbola koji nisu na svom mestu
					5.1.4.1.1.2. Aplikacija obaveštava i drugog igrača o rezultatu pokušaja
					5.1.4.1.1.3. Aplikacija proverava da li je sekvenca koju je igrač uneo ujedno i tražena sekvenca
						5.1.4.1.1.3.1. Ukoliko jeste
							5.1.4.1.1.3.1.1. Prelazi se na korak 4.
						5.1.4.1.1.3.2. Ukoliko nije
							5.1.4.1.1.3.2.1. Aplikacija smanjuje broj pokušaja igrača za jedan
							5.1.4.1.1.3.2.2. Prelazi se na korak 3.1.1.
				5.1.4.1.2. Ukoliko korisnik nije kliknuo na dugme za submitovanje
					5.1.4.1.2.1. Prelazi se na korak 3.1.2.
			5.1.4.2. Ukoliko nije popunio, prelazi se na korak 3.1.2.
6. Aplikacija proverava da li igrač koji je na potezu ima broj pokušaja nula ili da li je vreme isteklo
	6.1. Ukoliko je makar jedan od dva uslova tačan
		6.1.1. Aplikacija proverava da li je igrač dva imao pravo poteza
			6.1.1.1. Ukoliko nije
				6.1.1.1.1. Aplikacija igraču dva daje privremeno pravo poteza
				6.1.1.1.2. Aplikacija igraču dva postavlja broj pokušaja na jedan
				6.1.1.1.3. Prelazi se na korak 3.
			6.1.1.2. Ukoliko jeste
				6.1.1.2.1. Prelazi se na korak 5.
	6.2. Ukoliko nijedan uslov nije tačan
		6.2.1. Aplikacija daje poene igraču koji je na potezu u zavisnosti od preostalog broja pokušaja
		6.2.2. Prelazi se na korak 5.
7. Aplikacija proverava da li su oba igrača odigrali igru
	7.1. Ukoliko jesu
		7.1.1. Prelazi se na slučaj upotebe Ko zna zna.
	7.2. Ukoliko nisu
		7.2.1. Aplikacija daje drugom igraču pravo poteza
		7.2.2. Prelazi se na korak 2.
```

### Alternativni tokovi:
```
A1 - Neocekivani izlaz iz aplikacije. Ukoliko u bilo kojem koraku korisnik isključi aplikaciju,
sve zapamćene informacije se poništavaju i aplikacija završava rad.
Slučaj upotrebe se završava.

A2 - Neočekivani izlaz protivnika iz aplikacije. Ukoliko u bilo kojem koraku protivnik isključi
aplikaciju, sve zapamćene informacije se poništavaju i aplikacija prikazuje glavni meni.
Slučaj upotrebe se završava.
```
Podtokovi:
```
 /
 ```

### Specijalni zahtevi
Aplikacija i server su povezani međusobno.

### Dodatne informacija: /
		

