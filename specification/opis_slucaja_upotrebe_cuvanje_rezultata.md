# Slučaj upotrebe: Čuvanje rezultata
## Kratak opis 
 Aplikacija nakon završene partije treba da sačuva informacije o odigranoj igri.
## Akteri:
/
## Preduslovi
 Igranje jedne partije igre je završeno.

## Osnovni tok
```
1. Aplikacija šalje podatke serveru o broju postignutih poena i o igračima
2. Aplikacija šalje podatke serveru o identifikatoru igre
3. Server dohvata podatke
4. Server dodaje u fajl sistem podatke o odigranoj igri
5. Aplikacija otvara glavni meni igračima
```
### Alternativni tokovi:
```
A1: Neocekivani izlaz iz aplikacije. Ukoliko u bilo kojem koraku korisnik isključi aplikaciju,
sve zapamćene informacije se poništavaju i aplikacija završava rad.
Slučaj upotrebe se završava.

```

### Specijalni zahtevi:
Aplikacija i server su povezani međusobno.

