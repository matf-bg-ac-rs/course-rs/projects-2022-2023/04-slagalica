﻿# Slučaj upotrebe: Spojnice

## Kratak opis:
Nakon igre “Ko zna zna“ na red dolazi igra “Spojnice“ . U igri “Spojnice“ igrači se takmiče tako što spajaju dva pojma.

## Akteri: 
Dva igrača.

## Preduslovi: 
Završena je igra “Ko zna zna“.

## Postuslovi: 
Poćinje igra “Asocijacije“.

## Osnovni tok:

    1. Aplikacija učitava poene osvojene u prethodne dve igre.

    2. Prvi igrač je na potezu, prva runda spojnica.

    3. Sve dok igraču koji je na potezu ne istekne vreme ili dok nisu prošli svi pojmovi, ponavljaju se naredni koraci:

        3.1. Igrač koji je na potezu spaja pojam sa nekim od pojmova sa desne strane.
        3.2. Ukoliko je tačno spojio potrebno je uvećati poene:
	    	3.2.1. Aplikacija uvećava ukupne poene. 
	    	3.2.2. Aplikacija obeveštava igrača koji nije na potezu o tekućem stanju.
	    	3.2.3. Aplikacija prelazi na sledeći pojam.
	    	3.2.4. Aplikacija se vraća na korak 3.
    	3.3. Ukoliko nije tačno spojio potrebno je obavestiti igrace:
	    	3.3.1. Aplikacija obeveštava oba igrača o tekućem stanju.
	    	3.3.2. Menjamo igrača koji je na potezu. 
	    	3.3.3. Ukoliko nisu oba igrača pokušala da spoje taj isti pojam:
                3.3.3.1.Aplikacija ostaje na istom pojmu i igrač koji je na potezu ima mogućnost da spoji isti pojam sa nekim pojmom sa desne strane.    
	    	3.3.4 Aplikacija se vraća na korak 3.
    4. Ukoliko drugi igrač nije započeo spojnicu :

        4.1 Drugi igrač je na potezu, druga runda spojnica. Vraćamo se na korak 3.
    
    5. Prelazi se na slučaj upotrebe “Asocijacije“.

### Alternativni tokovi: 

    A1 - Neocekivani izlaz iz aplikacije. Ukoliko u bilo kojem koraku korisnik isključi aplikaciju,
    sve zapamćene informacije se poništavaju i aplikacija završava rad.
    Slučaj upotrebe se završava.

    A2 - Neočekivani izlaz protivnika iz aplikacije. Ukoliko u bilo kojem koraku protivnik isključi
    aplikaciju, sve zapamćene informacije se poništavaju i aplikacija prikazuje glavni meni.
    Slučaj upotrebe se završava.

Podtokovi:
```
 /
 ```

### Specijalni zahtevi: 
/

### Dodatne informacije:

    DI1. Vreme za spajanje jednog pojma je 10 sekundi.

    DI2. Broj poena koji se dobija za tačno spojena dva pojma je 2.

    DI3. Imamo 2 runde spojnica, a u svakoj po 8 pojmova sa leve strane i 8 sa desne strane, koje treba upariti.



