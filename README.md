# Slagalica

## :memo: Opis igre
-Dva igraca takmice se u kvizu znanja. Pobednik je onaj koji osvoji vise poena. Kviz Slagalica se sastoji od sledecih igara: skocko, ko zna zna, spojnice i asocijacije. <br><br>

## :movie_camera: Demo snimak projekta 
- link: [Slagalic++a](https://youtu.be/s2mL_vEtAh8) <br><br>

## Okruzenje
- [![qtCreator](https://img.shields.io/badge/IDE-Qt_Creator-olivia)](https://www.qt.io/download) <br><br>


## Programski jezik
- [![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-red)](https://www.cplusplus.com/)  *C++17*  <br>
- [![qt6](https://img.shields.io/badge/Framework-Qt6-green)](https://doc.qt.io/qt-6/) *ili >* <br><br>


## :hammer: Instalacija :
- Preuzeti i instalirati [*Qt* i *Qt Creator*](https://www.qt.io/download).
- Ako je to potrebno,  nadograditi verziju C++ na C++17 <br><br>


## :wrench: Preuzimanje i pokretanje :
- 1. U terminalu se pozicionirati u zeljeni direktorijum
- 2. Klonirati repozitorijum komandom: `$ git clone git@gitlab.com:matf-bg-ac-rs/course-rs/projects-2022-2023/04-slagalica.git`
- 3. Otvoriti okruzenje *Qt Creator* i u njemu otvoriti game.pro fajl
- 4. Pritisnuti dugme *Build* u donjem levom uglu ekrana <br>
- 5. Prilikom otvaranja game.pro fajla otvaraju se server.pro i slagalica.pro fajlovi
- 6. Pokrenuti server desnim klikom na server i klikom na dugme *Run*
- 7. Pokrenuti dva klijenta na isti nacin   <br><br>



<ul>
    <li><a href="https://gitlab.com/MilosevicNikola99">Nikola Milošević 197/2018</a></li>
    <li><a href="https://gitlab.com/kouzel">Aleksa Kozoderović 176/2018</a></li>
    <li><a href="https://gitlab.com/aleksat21">Aleksa Toroman 84/2018</a></li>
    <li><a href="https://gitlab.com/FoRPoP">Ognjen Popović 214/2018</a></li>
    <li><a href="https://gitlab.com/MilaJakovljevic">Mila Jakovljević 117/2018</a></li>
</ul>
